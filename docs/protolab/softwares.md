# Softwares Available

In the protolab, we have two desktops and one laptop available! Here you can find which softwares are available on which:


## *(Left)* Desktop:



## *(Right)* **Predator** Desktop:

### **A**
- 3D viewer
- Agisoft
    - Agisoft Lens
    - Agisoft Network Monitor
    - Agisoft Photoscan Professional
    - Agisoft Viewer
- Arduino
- Autodesk
    - Autodesk Desktop App
    - Autodesk Fusion
    - Autodesk Meshmixer
    - Autodesk Recap
    - Autodesk ReCap Photo

### **B**
- Bambu Studio
- Blender

### **C**
- Cortana (not working)
- Chitubox 64
- ClickMILL

### **D**
- Disney +

### **F**
- FreeCAD

### **G**
- Github Atom

### **I**
- iModela Creator
- Inkscape

### **K**
- KiCad
    - Bitmap2component
    - Eeschema
    - Gerbview
    - KiCad
    - Pcbnew
- Krita

### **L**
- LibreOffice
    - LibreOffice
    - LibreOffice Base
    - LibreOffice Calc
    - LibreOffice Draw
    - LibreOffice Impress
    - LibreOffice Math
    - LibreOffice Writer

### **M**
- Mail
- Makehuman
- Maps
- MecSoftFloatingLicense Manager
- Media Player
- Microsoft
    - Microsoft Edge
    - Microsoft Store
- Mixed Reality Portal
- Movies & TV

### **N**
- Notepad ++
- NVIDIA Control Panel

### **O**
- OneDrive
- OneNote for Windows 10
- Outlook

### **P**
- Paint 3D
- PC Health Check
- Phone Link
- Photos

### **R**
- Revo Studio
- RevoScan
- Rhino 6
- Rhino 6 in Safe Mode
- RhinoCAM
    - 2016 for 5.0
    - 2017 for 5.0
    - 2018 for 6.0
- Roland
    - CutStudio
    - Roland MODELA player 4
    - Roland SS+FEdit2
    - Roland Virtual MODELA

### **S**
- Settings
- Silhouette Studio
- Skanect 1.11 (Win64)
- Skype
- Slicer for Fusion360
- Snip & Sketch
- Solitaire & Casual Games
- Spotify
- Sticky Notes

### **T**
- Tips System

### **U**
- Ultimaker Cura

### **V**
- VideoLAN
- Voice Recorder
- VPanel for SRM-20

### **W**
- Weather
- Windows
    - Accessories
    - Administrative Tools
    - Backup
    - Ease of Access
    - Powershell
    - Security
    - System
- WinRAR

### **X**
- Xbox Console Companion

### **Z**
- Zoom



## Mountain Laptop:

