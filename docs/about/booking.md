# Booking system

![](../assets/booking.png)

## Intro

**To use any machine in the Lab, users must reserve them first via our [Booking System](https://iaac.simplybook.it/v2/).**

**We will add students to the data base to become users; users will receive their log in data via email, please log in and change your password.**




## BEFORE BOOKING
To book any machine you must have all the below things done:

1. Sign these terms and conditions

2. Have training in the use of EVERY MACHINE

3. Pass the online TEST

4. Your name must be added to the trained user list

5. Preparing files and production plan

6. Then…booking!

## BOOKING SYSTEM INSTRUCTIONS
To make your reservation, go to [Booking system](https://iaac.simplybook.it/v2/)

1. Log in with your Iaac email.

2. Select the machine or tool you want to use.

3. Select the duration you intend to use the machine or tool (30 min, 1, 2, 3, 4
hrs).

4. Select the appropriate day and time from the available time slots.

5. To finish, click "BOOK NOW".


## MACHINE TEST

!!! warning "For anyone wanting to use the lab"
    [FABLAB PROTOCOL](https://rules.fablabbcn.org/)
