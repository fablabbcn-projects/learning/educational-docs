## Inventory

Make sure you update the inventory whenever you take components from the lab: https://inventory.fablabbcn.org

![](../assets/inventory.png)

!!! info "Ask for the login"
    Ask your instructors for the login into the inventory

### What to update:

- Electronics inventory
- Molding and casting
- Millbits
- ...

### Why?

1. It saves time for instructors and lab management for yearly purchases
2. You can see how other projects in the house (Smart Citizen, Precious Plastics, etc) do things, or which components they use
3. It helps the great project [inventree](https://github.com/inventree/InvenTree) is
