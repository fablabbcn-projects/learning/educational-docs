# Microchallenge III

## Intro presentation

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQUHVfww4cSsDwcKkuvDUuDnzC0EQete5sXSbet6UOOywHvQ5nYXpLKkJ-BfA0puS4owvOxIuQopNei/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>



## Goal

**Compose a meaningful interaction that uses data collected from an "INPUT" (body) and transforms it into another digital signal (OUPUT).**

- Define the interaction protocols
- Define your digital system

*Develop a prototype that reflects on personal or collective identity*


### Digital Interface

A digital interface refers to the point of interaction between users and a digital device or system. **It is the medium through which users can communicate, control, and exchange information with a digital system or device.** The digital interface can be comprised of various components, such as software, hardware, and visual elements, that enable users to interact with and manipulate digital content.

The goal is to create an interface that enables users to easily understand and interact with the **digital system**, effectively completing their tasks or achieving their objectives.


## Weekly support

![](assets/supportc3.png)

## Documents

![](assets/c_1-f7978b2d.png)

- [Miro](https://miro.com/app/board/uXjVKTwGfVQ=/?share_link_id=916008817316)

![](assets/c3feedback.png)

- [Link to Spreadsheet](https://docs.google.com/spreadsheets/d/1aa4715tpwxirpDHnoAvP7I7jaG1zI_RWzGTz-JxsSg8/edit?usp=sharing)