# Scripting away your fears

Welcome to the code-club!

![](https://upload.wikimedia.org/wikipedia/commons/c/cc/Digital_rain_animation_medium_letters_shine.gif)

## Our goal

Our goal for the code-club is to stop thinking that you need to download a program/application from the internet in order for your computer to do something. 

### Where programs live

You can easily find out many programs/scripts in your computer.

![](https://imgs.xkcd.com/comics/universal_install_script.png)

Open up a terminal and type:

```
where python
```

or

```
whereis python
```

### Two things to remember

![](assets/script-life.png)
    
1. Everything is a file - installing something means moving a file and doing a couple of changes
2. Every file you make into a script needs to know who is executing them. That brings us to programming languages

**Working with scripts**

Mainly, one can execute a script in two ways:

* Telling the command line who should run the script

```
python app.py
```

```
bash app.sh
```

* Making the file executable. For this we need to tell it which interpreter to use

To check where an application (in UNIX) or program (in Windows) resides in your computer, you can check it in the CL by:

```
where python
/Users/macuser/anaconda2/bin/python
/usr/local/bin/python
/usr/bin/python
```

!!! Note "bash, windows and linux users"
    Instead of where, type in `whereis`

Then, the very first line of the file should be: `#!path/to/python`, i.e.: `#!/Users/macuser/anaconda2/bin/python`. Then, we can make the file executable by (this is called a [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)))

```
chmod +x app.py
```

And run it by:

```
./app.py
```

### Programming languages

[![](https://i.imgur.com/Y5Di3h8.png)](https://visual.ly/community/interactive-graphic/technology/network-graph-programming-languages-influence?view=true)

According to [Codelani's list](http://codelani.com/lists/languages.html), there are about **3,156 computer languages** (including dialects of BASIC, [esoteric programming languages](https://en.wikipedia.org/wiki/Esoteric_programming_language), and [markup languages](https://en.wikipedia.org/wiki/Markup_language)).

In Wikipedia you can find a more [filtered list](https://en.wikipedia.org/wiki/List_of_programming_languages_by_type) by type of language.

![](https://miro.medium.com/max/1200/1*8j2PmhExz4q87OoddaH7ag.png)

!!! Note "**For the geeks!**"
    Instructions for the Apollo Guidance Computer https://en.wikipedia.org/wiki/Apollo_Guidance_Computer#Instruction_set

### Our languange for today - python

**Python** is an open source and high-level programming language, which means, it is easily understandable and most crucially readable for humans, simultaneously being independent of the platform it is running.

![](https://i.imgur.com/7NUW9W7.png)

It was designed by this guy (Guido Van Rossum - Monty python fan)

![](https://cms.qz.com/wp-content/uploads/2019/05/Guido-van-Rossum-e1558635088256.jpg)

List of Python applications:

[![](https://i.imgur.com/12Wl6Pt.png)](https://www.python.org/about/apps/)

- Readable and Maintainable Code

```
age = 16
if age >= 18:
    print (“Eligible to Vote”)
else:
    print (“Not Eligible to Vote”)
```

- Compatible with Major Platforms and Systems

Python is supported in *everywhere*. You can even use Python interpreters to run the code on specific platforms and tools. Also, since Python is an *interpreted programming language*, it allows you to run the same code on multiple platforms without recompilation.

- Top 3 popular programming languages

According to the latest [TIOBE Programming Community Index](https://www.tiobe.com/tiobe-index/), Python is one of the top 3 popular programming languages of 2019.

![](https://i.imgur.com/Fd6weEV.png)

- Robust Standard Library

Its large and robust standard library makes Python score over other programming languages. The standard library allows you to choose from a wide range of modules according to your precise needs. Each module further enables you to add functionality to the Python application without writing additional code.

- Many Open Source Frameworks and Tools

You can even use several open source Python frameworks, libraries and development tools to curtail development time without increasing development cost.

#### Getting to know eachother

- Python 2.0 was first released in 2000. Its latest version, 2.7, was released in 2010.
- Python 3.0 was released in 2008. Its newest version, 3.7, was released in 2016, and version 3.7 is currently in development.
- Although Python 2.7 is still widely used **it shoudn't**.
- On January 1, 2020, Python 2.7 was _set to be no longer maintained_

#### Installing python

Follow [this guide](https://docs.python-guide.org/starting/installation/) **for python3!!**

#### Python Environments

A python environment is a tool that helps to keep dependencies required by different projects separate by creating isolated python virtual environments for them. This is one of the most important tools that most of the Python developers use.

![](https://d33wubrfki0l68.cloudfront.net/02962eb19c0069740d16e67b5ba7c613238c8b9a/30ed2/_images/34435689480_2e6f358510_k_d.jpg)

!!! Note "The _official_ way"
    [Installing packages](https://packaging.python.org/tutorials/installing-packages/#installing-packages)

A **Virtual Environment** is a tool to keep the dependencies required by different projects in separate places, by creating virtual Python environments for them. It solves the “Project X depends on version 1.x but, Project Y needs 4.x” dilemma, and keeps your global site-packages directory clean and manageable.

The most popular tools for setting up environments are:

- **PIP** (a Python package manager; funnily enough, it stands for “Pip Installs Packages”) with `virtualenv` (a tool for creating isolated environments) - the traditional approach

```
# Create environment with virtualenv
python -m venv environment
# Activate the environment
source ./environment/bin/activate
# List packages
pip list
# Install packages
pip install <package-name>
```

- **Conda** (a package and environment manager)
- **Virtualenvwrapper** (environment manager wrapped around virtualenv)
- **Pipenv** (probably the best) - find how to set it up [here](https://pipenv.kennethreitz.org/en/latest/install/#make-sure-you-ve-got-python-pip)

!!! Note "What's the best?"
    **It depends** on what you do and how you do it. _Conda_ manages environments globally, although it can be slow due to _conda forge_ size. Virtualenvwrapper is very good, and does things globally too. Pip manages globally, but without environment, and pipenv or venv does not install things globally... So it depends!

**Virtualenvwrapper (easy peasy)**

[The docs](https://virtualenvwrapper.readthedocs.io/en/latest/).

> virtualenvwrapper is a set of extensions to Ian Bicking’s virtualenv tool. The extensions include wrappers for creating and deleting virtual environments and otherwise managing your development workflow, making it easier to work on more than one project at a time without introducing conflicts in their dependencies.

To install virtualenvwrapper:

```
pip install virtualenvwrapper
```

To create an environment:

```
mkvirtualenv py3 -p python3
```

To activate it:

```
workon py3
```

To install something in it:

```
pip install something
```

**Anaconda/Miniconda (heavy but with GUI)**

*Anaconda* is a Python distribution intended for data processing, predictive analysis and scientific programming that simplifies the management of packages.
There are other ways to install python and the necessary packages for OpenCv but they are more complex and Anaconda makes it easier for us to work.

!!! warning "Windows users"
	In [this link](https://medium.com/@GalarnykMichael/install-python-on-windows-anaconda-c63c7c3d1444) you can find a nice tutorial to set up your anaconda

!!! tip "Step-by-step"

    * Step 1 - Visit official [Anaconda website](https://www.anaconda.com/distribution/) and download it

    ![](https://i.imgur.com/xjqKoBc.png)

    There are different versions for different Windows, Mac and Linux operating systems. Download and follow the instructions to install the one that suits you best.

    ![](https://i.imgur.com/dbucNMf.png)

    You can check that you have installed it by opening a command line and typing the following:

    ```
    python -V
    ```
    ![](https://i.imgur.com/GT3ExCM.png)

    ![](https://i.imgur.com/NgcBEb3.png)

    * Step 2 - Creating an environment from an environment.yml file

    Use the terminal or an Anaconda Prompt for the following steps.

    **Create the environment for Python 3.7**

    ```
    conda create -n yourenvname python=x.x anaconda
    ```
    ![](https://i.imgur.com/dyxh1Ub.png)

    And press Yes [y]

    ![](https://i.imgur.com/V3wBvKp.png)

    ![](https://i.imgur.com/kIXA8G3.png)

    To activate your environment:

    ```
    conda activate nameenv
    ```

    ![](https://i.imgur.com/VsHxukx.png)

    To deactivate an active environment:
    ```
    conda deactivate
    ```

!!! Note "Manage your environments like a pro"
	More information about [managing environments](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file).

!!! Warning "Windows users"
    Follow [this for setting up your PATH](https://stackoverflow.com/questions/44597662/conda-command-is-not-recognized-on-windows-10)

#### Installing packages

![](https://i.imgur.com/jwgAORw.png)

With pip:

```
pip install <name_package>
```

With Conda:

```
conda install -c conda-forge <name_package>
```

#### Coding control flow structures

**While Loop**

_C++_
```
while (condition) {

    // Do stuff
}
```

_Python_
```
a = 0
while a < 10:
    a = a + 1  #alternatively a += 1
    print(a)
```

**Conditionals**

_C++_
```
if (condition) {

    // Do stuff
} else {

    // Do other stuff
} 
```

_Python_

```
a = 1
if a > 5:
    print("This shouldn't happen.")
else:
    print("This should happen.")
```

**For loop**

_C++_
```
for (# iterations) {
    //Do stuff
}
```

_Python_
```
for i in range(6):
	print ('Hello')
```

**Boolean Expressions**

![](https://i.imgur.com/JvWNnMx.png)

## Hands-on!

!!! Note
    Code for this session in: https://gitlab.fabcloud.org/barcelonaworkshops/code-club/tree/2020/01_python_basics

### Hello world

Super basic example:

```
print ('Hello')
```

```
python Example_00.py
Hello
```

A bit more complex, with [__main__ structure](https://docs.python.org/3/library/__main__.html):

```
def main():
	print ('Hello')

if __name__ == '__main__':
	main()
```

```
python Example_01.py
Hello
```

### Hello world with arguments

!!! Warning "Challenge"
    Make a python script that you can run either with ```python script.py``` or by `./script.py` and that prints: `Hello <YOUR NAME> <YOUR SURNAME>`
    

        python script.py -h
        usage: Example_02.py [-h] [--name NAME] [--surname SURNAME]

        optional arguments:
          -h, --help            show this help message and exit
          --name NAME, -n NAME  your name
          --surname SURNAME, -s SURNAME  your surname
        python script.py -n 'Oscar' -s 'Gonzalez'
        Hello Oscar Gonzalez

!!! Note "Where to start?"
    More info about argparsing [here](https://docs.python.org/3/library/argparse.html?highlight=argparse#module-argparse)

```
import argparse

### 
--- Your function here ---
### 

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--name", "-n", default = "Antonio", help="your name")
    ###
    # --- Your argument here ---
    ####
    parser.set_defaults(keep=False)

    args = parser.parse_args()
    main(args.name, args.surname)
```

## References

- [Understanding the Features and Usage of 10 Popular Python GUI Frameworks and Toolkits](http://www.allaboutweb.biz/python-gui-frameworks-usage/)
- [Pipenv](https://pipenv.kennethreitz.org/en/latest/)
- [REPL.it](https://repl.it/languages)
- [Managing packages](https://packaging.python.org/tutorials/installing-packages/#installing-packages)