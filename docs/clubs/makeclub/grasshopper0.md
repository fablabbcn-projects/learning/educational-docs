# Welcome to Grasshopper!

![](img/wave.jpg)

## W1-Class Intro **

- [Class File - Day 19-10-2020](assets/19-10-20ClassFile.gh)

!!! Class Keypoints
      - Interfaces
      - 12:55
      - Interfaces Nohow
      - Parametric design concept - Scripting logic's
      - Parameter definition
      - Rhino-Grasshopper bridge

#### *Guide scripts*
This are the files that you can use after the class. They host the same concepts explained during the lecture, but noted and clarified in different parts, so they are easier to understand in a standalone workflow.

- [Grasshopper Basics - IntroRhinoFile](assets/day1/01.3dm)
- [Grasshopper Basics - IntroGrasshopperFile](assets/day1/01-Basic.gh)

## W2-Class Intro **

- [Class File - Day 02-11-2020](assets/02-11-20ClassFile.gh)

!!! Class Keypoints
      - List concept
      - List Operation/Manipulations
      - Geometry as a compound of lists
      - Geometry transformations


### *Guide scripts*

- [Grasshopper Basics -Grow-Scale-Rotate](assets/day1/02-growscaledandrotatedgeometry.gh)
- [Grasshopper Basics - Lists](assets/day1/03-Listas.gh)
- [Grasshopper Basics - Geometry-Lists](assets/day1/04-Geometria-Lists.gh)
- [Grasshopper Basics - Grow-Scale-Rotate-Surface](assets/day1/05-Grow-Scale-RotateSurface.gh)
- [Grasshopper Basics - Displace-Geometry-Conect](assets/day1/06-DisplaceGeometryConect.gh)
- [Grasshopper Basics - Extrusion Distance](assets/day1/07-ExtrusionDistance.gh)



## Installers Plugins

W.I.P.
