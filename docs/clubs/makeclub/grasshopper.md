# Welcome to Grasshopper!

![](img/wave.jpg)

!!! Note "First time user -Read this"
    **Grasshopper comprehensive**: intro[guide](http://grasshopperprimer.com/en/index.html)

    !!! Note "Online Grasshopper"
        **Grasshopper on the browser**: access the [link](https://nodepen.io/)
        For now, a limited selection of components are available, but more are being added all the time

**[ONLINE SHARING FILES PLATFORM](https://hydrashare.github.io/hydra/)**

**Grasshopper codes** [CO-DE-IT link](https://www.co-de-it.com/code/grasshopper-code)
A collection of codes & grammars for Grasshopper, a generative modeling tool included with Rhinoceros.



## Sample Files we will use in class:

[Download link for zip folder with all the scripts](https://drive.google.com/file/d/1gxtSOYQ4cBJ5GV7-8A0C0bGBP2NnWqzI/view?usp=sharing)
[Download link for zip folder with all the scripts-Class 27/05/2020](assets/Grass2.rar)

## Animation**

- [Animation - Horster](assets/Animation-Horster.gh)
- [Animation - ScreenshotCapturing](assets/Animation-ScreenshotCapturing.gh)
- [Animation - RotationalRecording](assets/Animation-RotationalRecording.gh)

## Physics Engine**

- [Physics - Kangaroo Intro](assets/Physics-Kangaroo.gh)
- [Physics - Kangaroo 02](assets/Physics-Kangaroo2.gh)
- [Physics - Kangaroo BendingRods](assets/Physics-KangarooBendingRods.gh)
- [Physics - Kangaroo Dome](assets/Physics-KangarooDome.gh)
- [Physics - Kangaroo Minimum surface](assets/Physics-KangarooMinimun.gh)
- [Physics - Kangaroo Particles](assets/Physics-KangarooParticles.gh)
- [Physics - Kangaroo Planarize](assets/Physics-KangarooPlanarize.gh)

## Photogrammetry**

- [Valldaura point Cloud Example file](https://drive.google.com/file/d/1n3Q7WMjXcI-TJDFG7Tf8cmg2tdv0uuO0/view?usp=sharing)
- [Photogrammetry - PointCloudVegetationAll](assets/Photogrammetry-PointCloudVegetationAll.gh)
- [Photogrammetry - Height](assets/Photogrammetry-Height.gh)

## FormFinding**

- [FormFinding - SurfaceTopologyAttractor](assets/FormFinding-SurfaceTopologyAttractor.gh)
- [FormFinding - Truss](assets/FormFindingTruss.gh)
- [FormFinding - Voronoi-Cocoon](assets/FormFinding-Voronoi-Cocoon.gh)
- [FormFinding - DendroMeshFixer](assets/FormFinding-DendroMeshFixer.gh)
- [FormFinding - Cocoon-Topology-Biomorpher](assets/FormFinding-Cocoon-Topology-Biomorpher.gh)
- [Fabrication - 3Dprinting-Curvature analysis](assets/Fabrication-3Dprinting-Curvatureanalysis.gh)

## Webcam/ Open Vision

**Grasshopper interactive tools** [tools link](https://github.com/dantaeyoung/GrasshopperArsenal)
- PoseNet: Use PoseOSC to send realtime pose estimation data from your camera to Grasshopper!
- FaceOSC: Use FaceOSC to send facial tracking data to Grasshopper!
- reacTIVision: Use reacTIVision to detect fiducial tags and their movement!
- PostUploader: Use C# to do a POST request to a REST api (namely, to upload a file to imagebin.ca).
- ScreenshotTaker: Python-activated Rhino command to do ViewCaptureToFile.
- LispHopper: Lisp (Hy), Python, and Grasshopper. What's not to like?

W.I.P.
