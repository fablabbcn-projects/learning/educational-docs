# Weekly Agenda and Class links

#### Class Types*

- **Local Classes**: This classes are given every Thursday from 9.30 am to 13.30 pm (CET time)(mandatory) and additionally, depending on the week, on Thursday from 15 to 18:30 or on Friday from 9:30 to 13:30.

- **Global   Class**: This classes are given by Neil Gershenfeld every Wednesday at (15.00pm CET)(mandatory).

- **Recitations**: Every two weeks, open talks given by some experts of the network on the weekly topic of the assignment.

- **Project Review**: Once a month, every student will give a short presentation on the status of their final project.

![FabAcademy Schedule](/learning/educational-docs/assets/fabacademy.jpg)

### Monday -  Recitations

**Every Monday  Recitation from 15h to 16.30h hours**
meeting ID: 894 1627 8965, password: 1234
can connect via browser: [http://fabacademany.org/2025/video.html](http://fabacademany.org/2025/video.html)
or:  [https://zoom.us/j/89416278965](https://zoom.us/j/89416278965)


### Wednesday - Global Classes day

**Every Wednesday  GLOBAL CLASS & Review from 15h to 18h hours**
- Zoom Link : [https://zoom.us/j/89416278965](https://zoom.us/j/89416278965)
- Meeting ID: 894 1627 8965
- password: 1234
- or via browser: [http://fabacademany.org/2025/video.html](http://fabacademany.org/2025/video.html)

### Thursday / Friday - Local Classes days

Details can be found up to date in [the Google Calendar](https://calendar.google.com/calendar/embed?src=c_2e5de0c5b76028b9610c8d112c6018a2a64a9917bc0d87828b8c5641f05bbb4a%40group.calendar.google.com 
).
