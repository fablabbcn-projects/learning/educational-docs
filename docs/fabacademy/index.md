# Fabacademy

Welcome to Fabacademy 2025!

Here you are going to find the local documentation that we are going to use during the course.

This is a live site! It gets updated constantly. If you want to contribute or find a broken link, please let us know through [the gitlab repo](https://gitlab.com/fablabbcn-projects/learning/educational-docs).
