# Wildcard Week

{{ get_snippet_rel('docs/material/week16.md')}}

# Robots

**Robot Programming with Robots plugin WIKI steps by step guide [Link](https://wiki.fablabbcn.org/ROBOTS)**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0BbDR5Q6XtwUwwY_Waz7m8NkvOilB-uvHqjXN-tI1qJkHj5iMTj5AzTMXOxdY5hcZaCC2m4NDI2QS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

[DOWNLOAD LINK FOR ROBOTS PLUGIN FOR GRASSHOPPER & ROBOTS LIBRARY](https://drive.google.com/drive/folders/1wmBZL8tYLowkqEA6wXxM4Izn2tAjXYeh?usp=sharing)

[MORE INFO ON ROBOTIC ARM CLASS](/learning/educational-docs/clubs/makeclub/roboticarm)

** Here is the guide to the installation from [GitHub](https://github.com/visose/Robots/releases) for Rhino 7: **

[Installing the "Robots" plugin](https://drive.google.com/drive/u/1/folders/1VvIel_PcPPMXOrZHDe8-W_vHYybsm3Ac)


(Though we encourage to upgrade to Rhino 7, For the older version of Rhino:

- Download [Robots plugin](https://drive.google.com/file/d/1R0oL4cLXKDOvoAg4I-GyA7qP2VTEGZdE/view?usp=sharing) 

- Copy the .gha and .dll into the special folder. Make sure it is ‘Unblocked’ (Right Click on the .gha file and click on unblock button in the attributes section) Place BOTH files in your ‘Components Folder’. (Components folder can be found via grasshopper: File -> Special Folders -> Components Folders)

- Download the folder named Robots inside the [Robot libraries.rar](https://drive.google.com/file/d/1aefrHTtzpGMgti7UPSIY4AlewqUpUBt2/view?usp=sharing) (contains the ABBs robots at IAAC in a.3dm file) and place it inside the OS's Documents folder:

In Windows the path will look like C:\Users\userName\Documents\Robots\RobotLibrary.xml.
In Mac the path will look like HD/Users/userName/Robots/RobotLibrary.xml.

- Open Rhino > Open Grasshopper > drag and drop the 'robot system' component from the plugin to verify the installation.

You can find [here](https://drive.google.com/drive/folders/1jJT9AYlFCN_T9zl8cCbLyfj1t43DfkL0?usp=sharing) the files required for the installation

For instructions on installing Grasshopper Add-Ons, please see: [](https://www.food4rhino.com/en/faq#users-install-grasshopper-plugin)

[Grasshopper Class Script ROBOTS](/learning/educational-docs/material/extras/week16/assets/SCRIPT-RobotsAdvanced-LightPainting.gh)

[Grasshopper Basic Script ROBOTS](/learning/educational-docs/material/extras/week16/assets/SCRIPT-RobotsBasics.gh)

[Grasshopper Advanced Script ROBOTS](/learning/educational-docs/material/extras/week16/assets/SCRIPT-RobotsAdvanced.gh)

[Grasshopper Full Script ROBOTS](/learning/educational-docs/material/extras/week16/assets/SCRIPT-RobotsFull.gh)

