# Embedded Programming

## How To Make A Computer

**An intoduction to digital electronics**

<script async class="speakerdeck-embed" data-id="85a73ebd554c45a2808a6768fb1c2914" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

[Interactive explanation on how computers work](http://www.simplecpu.com/)


## Getting inspired by the past

{{ get_snippet_rel('docs/content/electronics/information_theory.md', '## Getting inspired by the past')}}

## Representing information

{{ get_snippet_rel('docs/content/electronics/information_theory.md', '## Representing information')}}

## Putting things together: circuits

{{ get_snippet_rel('docs/content/electronics/information_theory.md', '## Putting things together: circuits')}}

<br><iframe width="560" height="315" src="https://https://www.youtube.com/watch?v=lNuPy-r1GuQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



## Going integrated

{{ get_snippet_rel('docs/content/electronics/information_theory.md', '## Going integrated')}}


## The Arduino project

{{ get_snippet_rel('docs/content/electronics/information_theory.md', '## The Arduino project')}}

## Coding languages

{{ get_snippet_rel('docs/content/electronics/from_ideas_to_code.md', '## Coding languages')}}

## Algorithms and flow charts

{{ get_snippet_rel('docs/content/electronics/from_ideas_to_code.md', '## Algorithms and flow charts')}}

## Practical coding

Let's get coding! 

- [Embedded programming Practical](https://hackmd.io/@danimateos/rknAq7cFJe)

{{ get_snippet_rel('docs/content/electronics/from_ideas_to_code.md', '## Practical coding')}}


## Barduino 4.0

This week we are going to be using the Barduino 4.0. Please check the documentation [here](https://fablabbcn-projects.gitlab.io/electronics/barduino-docs/)

![barduino4](https://fablabbcn.github.io/adventronics/images/Barduino4board.svg)

And you can find the here also the [Adventronics Calendar](https://fablabbcn.github.io/adventronics/barduino/)

## Arduino Programming

### Installation

You will need to install the [Arduino IDE](https://www.arduino.cc/en/Main/Software). After that, we need to define the board that we are going to use.

!!! Note "Guide"
    To get started with the Arduino IDE, and install the boards needed for the ESP 32 and the SAMD11, please:

    Open your Arduino. Go to: `Preferences > Additional Board Manager URLs`

    There, add the boards sources. **One line per URL**:

    Paste this in the field there:

    ```
    https://dl.espressif.com/dl/package_esp32_index.json

    https://raw.githubusercontent.com/qbolsee/ArduinoCore-fab-sam/master/json/package_Fab_SAM_index.json
    ```

## Local Class: Arduino programming on TinkerCAD and Barduino

https://hackmd.io/XQyBsrUxR9OIIfzMW3ELBg?view


