# Electronics Design

## Global Class

- [Electronics Design](http://academy.cba.mit.edu/classes/electronics_design/index.html)

!!! Note "Assignment and assessment"

    [Assessment Guide](https://fabacademy.org/2025/nueval/electronics_design.html)


## Volts, Watts and waterfalls

{{ get_snippet_rel('docs/content/electronics/volts_waterfalls.md', '# Volts, Watts and waterfalls')}}

## Local Class: electronics design

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT57ZzInJanXhKj2wXPRF4Sm-psd4iwDuO4_lEl__x_bZZptALMSPi6HrRIvS21kT6GTn-s26sQWOtR/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### EDA

This week will be talking about EDAs. From [Wikipedia](https://en.wikipedia.org/wiki/Electronic_design_automation): _Electronic design automation (EDA), also referred to as electronic computer-aided design (ECAD),[1] is a category of software tools for designing electronic systems such as integrated circuits and printed circuit boards_

In Fab Academy, we use the free and open source EDA KiCad:

- [KiCad class](/learning/educational-docs/material/extras/week06/kicad)

Autodesk EAGLE is a commercial product. If you are interested, find a sample class on it [here](/learning/educational-docs/material/extras/week06/eagle).

!!! Danger "Making classes easier"
    Please, come with the following software downloaded and installed in your machine:

    - [Kicad](http://kicad-pcb.org/download/)
    - A vector manipulation program (preferrably [Inkscape](https://inkscape.org/))
    - **Optional**: An image manipulation program (GIMP, Photoshop)

!!! Note "Fab libraries for Kicad"

    We use a curated list of components that will simplify things a lot.

    [It is hosted in gitlab.fabcloud.org and we call it the Fab libraries.](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad)

## Resources

!!! Note "Curated references"
    - [All the libraries of Attiny,Atsam etc compilation link](https://github.com/arduino/Arduino/wiki/Unofficial-list-of-3rd-party-boards-support-urls)
    - [Circuit diagram](https://www.circuit-diagram.org/)
    - [How to self-learn electronics](https://news.ycombinator.com/item?id=16775744)
    - [Circuit simulation](https://hackaday.com/2019/11/30/circuit-simulation-in-python/?mc_cid=5bce5b5fcb&mc_eid=348b67951f)
    - [Ultimate Electronics tutorial](https://ultimateelectronicsbook.com/)
    - [Concise electronics for geeks](http://lcamtuf.coredump.cx/electronics/)
    - [Hand Drawn Traces](http://fab.cba.mit.edu/classes/863.15/section.CBA/people/Schaad/week6-electronics-design.html)
    - [Free Simulation Tools ( Mac, Windows, Linux, Android, iOS )](https://en.wikipedia.org/wiki/List_of_free_electronics_circuit_simulators)
    - [Reddit: best free circuit simulation tool?](https://www.reddit.com/r/embedded/comments/166wpii/best_free_circuit_simulation_tool/)

### Videos

- [Soldering Master Class](https://www.youtube.com/watch?v=vIT4ra6Mo0s)
- [Big Clive](https://www.youtube.com/user/bigclivedotcom)
- [Julian Illet](https://www.youtube.com/user/julius256)
- [Mike Electric Stuff](https://www.youtube.com/user/mikeselectricstuff)

### Books

- [Make: Electronics Learning by Discovery Charles Platt (e-book)](https://www.amazon.es/Make-Electronics-Learning-Through-Discovery/dp/0596153740)
- [Make: Encyclopedia of Electronic Components Volume 1: Resistors, Capacitors, Inductors, Switches, Encoders, Relays, Transistors (pdf)](https://www.amazon.es/Make-Encyclopedia-Electronic-Components-Transistors/dp/1449333893)

### Extra stuff

**Components**

- [Pull up](https://learn.sparkfun.com/tutorials/pull-up-resistors)
- [Resistor SMD code](https://www.hobby-hour.com/electronics/smdcalc.php/)

**How current flows in a circuit**

- [What is electric current](https://youtu.be/kYwNj9uauJ4)
- [Voltage, Current, Resistance, and Ohm's Law](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)
- [Conventional Versus Electron Flow](https://www.allaboutcircuits.com/textbook/direct-current/chpt-1/conventional-versus-electron-flow/)
- [Electric current](https://en.wikipedia.org/wiki/Electric_current)

**Simulation:**

- [Falstad](http://www.falstad.com/circuit/)

**Fab Academy students documentation:**

- [Jundy Wang](http://archive.fabacademy.org/archives/2016/fablabshangai/students/80/week6.html)
- [Steven Chew](http://archive.fabacademy.org/archives/2016/fablabsingapore/students/98/exercise06.html)
- [Gérard Bandini](http://archive.fabacademy.org/archives/2016/fablabajaccio/students/277/week06.html)
- [Vincent Dupuis](http://archive.fabacademy.org/archives/2016/fablabdigiscope/students/456/W6_electronics_design.html)
- [Adrián Torres](https://fabacademy.org/2020/labs/leon/students/adrian-torres/week06.html)
- [Quentin Bolsee](https://fabacademy.org/2020/labs/ulb/students/quentin-bolsee/assignments/week07/)
