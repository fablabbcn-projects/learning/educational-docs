# Project Management, Principles & Practices

## Fabacademy Global Class

{{ get_snippet_rel('docs/content/code/HTML.md', '## Fabacademy Global Class')}}


## Fabacademy Presentation

{{ get_snippet_rel('docs/content/code/HTML.md', '## Fabacademy Presentation')}}


## Introduction to the terminal

{{ get_snippet_rel('docs/content/code/Terminal.md', '## Introduction to the terminal')}}


## Intro to GIT

{{ get_snippet_rel('docs/content/code/Git.md', '## Git Basics - Version Control')}}


## Setting up git

{{ get_snippet_rel('docs/content/code/Git.md', '## Setting up git')}}


### git +

{{ get_snippet_rel('docs/content/code/Git.md', '### git+')}}


## How to make your web page in 10 steps

{{ get_snippet_rel('docs/content/code/HTML.md', '## Presentation How to make your web page in 10 steps')}}

## HTML exercise

{{ get_snippet_rel('docs/content/code/HTML.md', '## HTML exercise')}}

## Static Site Generators

[Here is a guide for using SSG](/learning/educational-docs/material/extras/week01/ssgs)



## Git Resources

{{ get_snippet_rel('docs/content/code/Git.md', '## Resources')}}

## HTML Resources

{{ get_snippet_rel('docs/content/code/HTML.md', '## HTML Resources')}}


## HTML Tools

{{ get_snippet_rel('docs/content/code/HTML.md', '## HTML Tools')}}