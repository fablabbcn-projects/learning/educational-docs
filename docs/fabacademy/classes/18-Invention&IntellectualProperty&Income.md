# 18. Invention, intellectual property, and income

![](/learning/educational-docs/material/assets/ip.jpg)

### Class Week 18

- [invention, intellectual property, and income](http://academy.cba.mit.edu/classes/invention_IP_business/index.html)


## FABLAB BCN classes

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vS7cnMwh5poA2JTs1bg9wBxZQrHQ0veSkKNM2nB57JAjUIRwb7vWK57PYsDFAmZug/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>




### From All Rights Retained to All Rights Relinquished

- [IP discussion notes](/learning/educational-docs/material/extras/week18/ipnotes.md)

This is a copy of the FLU class notes. You can find the latest version [here](https://hackmd.io/s/HJZ9KGRoE)


### Governance and Licensing in Open Source

- [Debate](https://hackmd.io/Gv6AAZOCTASNP9h04w-uMA?both)
