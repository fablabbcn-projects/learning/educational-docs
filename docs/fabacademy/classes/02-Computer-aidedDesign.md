# Computer-aided Design

## Global Class

{{ get_snippet_rel('docs/content/design/cad.md', '## Global Class')}}

## Presentation 2D MODEL

{{ get_snippet_rel('docs/content/design/cad.md', '## Presentation 2D MODEL 2025')}}

## Presentation 3D MODEL

{{ get_snippet_rel('docs/content/design/cad.md', '## Presentation 3D MODEL 2025')}}

   
## Computer Aided Modelling

{{ get_snippet_rel('docs/content/design/cad.md', '## COMPUTER AIDED 
MODELLING ')}}

### Lego Piece

![](../../assets/lego_piece.jpg)

### Rhino Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Rhino Lego Exercise')}}

### Blender Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Blender Intro')}}

### Openscad Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Openscad Intro')}}

### Grasshopper Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Grasshopper Intro')}}

### Fusion360 Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Fusion360 Intro')}}



## CAD - Resources

{{ get_snippet_rel('docs/content/design/cad.md', '## CAD - Resources')}}
