# Electronics Production

## Classes

### Global Class

- [Electronics production](http://academy.cba.mit.edu/classes/electronics_production/index.html)

### Local Class

#### Electronic production Overview

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTev9_NuHFuMPZfi600Ir06N7tfvATxlJWRbpkWVqylh2uZnjm8zAIzN9XSB2Ha1lemkSSj1pYVhEmb/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#### Links to production files

[MODS](https://modsproject.org/)

[Fabacademy programmers](https://pub.fabcloud.io/programmers/summary/)

[Quentorres](https://gitlab.fabcloud.org/pub/programmers/quentorres)

![Traces](../../assets/QTtraces.png)

![Holes](../../assets/QTholes.png)

![Exterior](../../assets/QToutline.png)

[SVG file](../../assets/quentorres_v2-brd.svg)

## Resources

!!! Note "Curated references"
    - [Beautiful images of common chips](https://siliconpr0n.org/map/)
    -[Recommended fabrication](http://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#programming)
    - [What is make](https://www.gnu.org/software/make/manual/html_node/Introduction.html#Introduction)
    - [How to self-learn electronics](https://news.ycombinator.com/item?id=16775744)
    - [Circuit simulation](https://hackaday.com/2019/11/30/circuit-simulation-in-python/?mc_cid=5bce5b5fcb&mc_eid=348b67951f)
    - [Ultimate Electronics tutorial](https://ultimateelectronicsbook.com/#dependent-sources)
    - [Guerrilla electronics](http://lcamtuf.coredump.cx/electronics/)
    - [List of videos and books as an introduction to electronics and computers (doc)](https://docs.google.com/document/d/1TS8hGFBqgIwLksClnXev__a6kn2-_dAEW7L0ZNUII5A/edit?usp=sharing)

### Fab Modules/MODS

* [FabModules App](http://fabmodules.org/)
* [How to use FabModules](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
* [Mods App](http://mods.cba.mit.edu/) *The newer fabmodules*
* [how to use MODS step by step tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mods.html)

### Precision milling CNC/VINYL tutorials

- [How to use SRM-20 Roland](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
- [How to use MDX-20 Roland](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mdx20.html)
- [How to use Vinyl cutter for PCB´S](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/flexible_pcb_windows_mac.html)

### Soldering

- [Basics Soldering](https://youtu.be/vIT4ra6Mo0s)
- [Soldering Tutorial](https://www.youtube.com/watch?v=b9FC9fAlfQE)
- [Instructables: How to Solder](http://www.instructables.com/id/How-to-solder/)
- [Student experience guide on how to solder](http://archive.fabacademy.org/archives/2016/fablabseoul/students/62/a04.html)
