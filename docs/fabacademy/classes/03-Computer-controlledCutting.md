# Computer-controlled Cutting

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRlYeSJufxq-Y80ZMn0pTaAwDry_zKqXoUbnCZ5scqhtVfFXVOYBTt6wScz_F4CD0pCyDKbaNhQAMiK/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Local Class

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Local Class')}}

### Presentation 3D MODEL Inspirational trends

{{ get_snippet_rel('docs/content/design/cad.md', '## Presentation 3D MODEL Inspirational trends')}}

### Grasshopper

[Grasshopper introductory class and exercises](https://hackmd.io/@danimateos/Hkxd_PzKye)

[Grasshopper introductory class 2025 definitions](../../assets/week_03_parametric_grasshoper.zip) (zip file)

## Computer-control Laser/Vinyl Equipment Usage Videos

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Computer-control Laser/Vinyl Equipment Usage Videos')}}

## Materials

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Materials')}}

## Trotec Speedy 400

{{ get_snippet_rel('docs/content/digifab/laser.md', '### Trotec Speedy 400')}}

## Vinyl Cutter

{{ get_snippet_rel('docs/content/digifab/laser.md', '### Vinyl Cutter')}}

## Softwares:

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Softwares:')}}

## Class Parametric scripts

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Class Parametric scripts')}}

## Inspirational Ideas

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Inspirational Ideas')}}

## Test files

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Test files')}}
