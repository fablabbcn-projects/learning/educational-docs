# What are the clubs

The Clubs are learning environment designed to develop the skills and competences related to Code, Make and Grow topics. Based on hands-on practical activities to promote peer to peer learning and community building. The clubs could be seen as **additional sessions** where we expand the contents seen during the core lectures, but that are aiming to give you those skills that we never have time to see in classes. Some of them will help to understand better and deeper the content of some weeks, but others could even be a fully different course on their own.

- **Make Club**: here we want to explore topics related to design and fabrication, as well as additional techniques related to the assignments and more. We will understand the basic format for fabrication: the G-CODE, and mmanipulate it to use the machines in more advance ways.

- **Code Club**: here we want to give you a _crash course_ in computers, scripting tools, creative coding, **PYTHON**... Starting from a very basic `Hello World` in different flavours, we will explore how we can interact with the internet, open a camera, read some data from a PCB or use machine learning to detect objects in images.

- **Grow club**: Coming soon!

!!! Note "Code club repository"
    Find it here (a branch per year): https://gitlab.com/fablabbcn-projects/learning/code-club