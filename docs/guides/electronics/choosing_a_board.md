# Selecting a board :pager:

![](https://i.imgur.com/OarV4G7.png)

## Basics :computer:

**What is a microcontroller?**

<a href="https://commons.wikimedia.org/wiki/File:Intel_C4004.jpg#/media/File:Intel_C4004.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Intel_C4004.jpg/1200px-Intel_C4004.jpg" alt="Intel C4004.jpg"></a>

> A microcontroller (MCU for microcontroller unit) is a small computer on a single integrated circuit. A microcontroller contains one or more CPUs (processor cores) along with memory and programmable input/output peripherals.

!!! info
    More details [here](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/material/extras/week04/whatisamicrocontroller/#now-actually-what-is-a-microcontroller)

**Microcontroller boards**

![](https://cdn.shopify.com/s/files/1/0438/4735/2471/collections/Types_Boards_2430x810.jpg?v=1628066011)

> A microcontroller board is considered a small computer built on a metal oxide semiconductor circuit chip.

!!! warning "Naming convention?"
    We sometimes refer to as an Arduino to any board with a microcontroller - it may be a board made by arduino, sparkfun, or someone else. 

**Shields**

![](https://programarfacil.com/wp-content/uploads/2019/05/arduino-shield-02.jpg)

> Shields are boards that can be plugged on top of the (Arduino) PCB extending its capabilities. The different shields follow the same philosophy as the original toolkit: they are easy to mount, and cheap to produce. 

!!! warning
    A shield is not always passive! It can have a microcontroller in it too, and sometimes it can be "stand alone" board if needed.

**SBCs**

![](https://www.raspberrypi.org/pagekit-assets/media/images/0c9cd61678cd088a7d22.jpg)

> A computer with all of its essential components such as RAM, CPU, and GPU built-in. Usually, SBCs are small, about the size of a credit card, or a bit larger but still smaller than the average desktop PC.

!!! warning
    Raspberry Pi is the SBC manufacturer we use the most. Not all SBCs are Raspberry Pis.

**HATs**

![](https://www.raspberrypi.com/app/uploads/2014/07/HAT-longpins.jpg)

> (Hardware Attached on Top). A HAT is an add-on board for (Raspberry Pi B+) that conforms to a specific set of rules that will make life easier for users.

!!! info
    In this class, we will talk about different brands/boards/types of boards. We will focus mostly on microcontroller boards.


## Important aspects to keep in mind :eyes:

!!! info "Remember this"
    A feature only makes a board better if you actually use it!

### 1. Power

<div style="display: grid; grid-gap: 10px;
    grid-template-columns: 1fr 1fr;
    grid-template-rows:  1fr 1fr;">
    <img src="https://i.imgur.com/46XcdPo.png" width=200px> 
    <img src="https://i.imgur.com/5aU676Z.png" width=200px> 
    <img src="https://i.imgur.com/10PrTrM.png" width=200px>
    <img src="https://i.imgur.com/qubiFE4.png" width=200px>
</div>

> **Power** - does your project run on batteries or wall power/mains? If it only uses batteries, consider a design that conserves power to run longer between battery changes. Or consider if solar or other power sources might be desirable.

**Questions**

- Does it need portability or is it stationary?
- Related to the above question, is it powered with AC or DC? What are the input voltage ranges?
    - AC: 230V; or 110V? Depends on where you are in the world!
    - DC common voltage levels: 5V, 3V3, 1V8 (not so common, yet)
- How can we power it? 
    - Do we use USB? --> Good for stationary, sometimes usable with power packs. Remember USB is always 5V.
    - Does it have a battery connector? JST-2Pin. **Be careful with the polarity!!**
    - Can we power the board using the pins?
- What are voltage ranges we need for the peripherals we want to connect?
    - If we have a peripheral that uses a digital protocol to communicate -> level shifters
    - If we have a peripheral such as an analog sensor with higher range than our maximum voltage -> Voltage divider (one directional)
- Current output (type of circuit)
- PoE
    
### 2. I/O

<div style="display: grid; grid-gap: 10px;
    grid-template-columns: 1fr 1fr;">
    <img src="https://i.imgur.com/qeqKHdz.png" width=300px>
    <img src="https://i.imgur.com/JfI7z7u.png" width=300px>
</div>

> **Peripherals** are the 'extras' beyond plain number crunching - like digital i/o pins, capacitive touch, ADC, SPI, I2C, UART, I2S, etc...

- Pin amounts

![](https://paradisetronic.com/media/images/org/a000067_iso_.jpg)

- Pin types (ADC, PWM) - clock speed ref. (**Not all the pins in a board are the same - always check the pinout!**)

![](https://www.redmossmedia.com/wp-content/uploads/2021/01/Arduino-Mega-Pinout.jpg)
    
Some things to look at:
+ For analog sensors: ADC (analog to digital converter) - check resolution for the ADC
+ For making _"fake"_ analog voltages we use PWM (pulse width modulation). For instance, servos are also driven with PWM signals

![](https://www.exploreembedded.com/wiki/images/5/54/0_LPC1768_PWM.gif)

- Buses: what type of communication protocols does the board integrate?
    - I2S: for digital audio transmision
    - I2C: normally used for networs of peripherals, such as sensors 
    - SPI: high speed, like SD-cards
    - UART: normally used for communication between microcontrollers

### 3. Network

<div style="display: grid; grid-gap: 10px;
    grid-template-columns: 1fr 1fr 1fr;">
    <img src="https://i.imgur.com/wFfh6qY.png" width=200px>
    <img src="https://i.imgur.com/kLBqI3L.png" width=200px>
    <img src="https://i.imgur.com/vnJ1FqC.png" width=200px>
</div>

> **Communications** - does your project need to communicate information to you or other devices? Consider how to do that. Radio-enabled projects are great but they will require a transmitter and receiver so the amount of hardware may be double what you thought. You can communicate via USB to a computer often easily. Or maybe infrared signals?

!!! success
    **Network vs. protocol**

    **A network** is a group of computers connected with eachother with communication lines.

    **The protocols** are the norms/agreements that format the messages and processes between the computers and programs in thos computers to understand eachother

**Questions**

- How far does it need to go?: 
    + long distance (lower frequency)
        * _RF_: depending on the application, different frequencies
    + short distance:
        * WiFi: high bandwidth
        * BLE: low consumption
        * Zigbee...
- How much data?
    + Not too much: RF
    + A lot: WiFi or BLE
- Cabled comunication: Although not very common in the microntroller boards we do - ethernet? It can also power the board with PoE!

### 4. Data/Processing capabilities

<div style="display: grid; grid-gap: 10px;
    grid-template-columns: 1fr 1fr;
    grid-template-rows:  1fr 1fr;">
    <img src="https://i.imgur.com/5YtAuE6.png" width=200px>
    <img src="https://i.imgur.com/e1M5FTN.png" width=200px>
    <img src="https://i.imgur.com/uIXK9SF.png" width=200px>
    <img src="https://i.imgur.com/jCD9gkD.png" width=200px>
</div>

> **Speed** - does your  project crunch a lot of numbers or gather huge amounts of data? Then speed may be good. Otherwise if the project is relatively simple, a smaller, slower board may be perfectly fine (and less expensive).

- RAM: ram size will determine how much you can do in your code: how many variables can be used at the same time, and because of that how many libraries, and other things your code can integrate. 2-4kb is kind of small, 32kb is good for many projects. This memory is **volatile** (i.e. it gets erased when the board is unplugged)
- Flash size / External flash: it will determine how big your code can be in general (the totality of the compiled code you _flash_ into the board). 4kb is small, 256kb is very good for many projects. The ESP8266 has 4Mb (or even 16Mb)
- EEPROM: you might need this if you want to store small things *permanently* (i.e. when the board is unplugged it doesn't get erased)
- Data streams: do yo need to handle video/audio at high speed/resolution? **Better use a SMC**.
- Data storage: do yo need to store data on the board? In other words, do you need memory? Maybe use one board that has **SD-card** compatibility!


### 5. Usability

- Size: some boards are larger only because they are used by non-experienced people, learning how to use them. Same functionality can be found in similar boards many times. Feather boards by adafruit are good options
- Power requirements: remember, cables, batteries... **Sometimes high power requires heat dissipation** - so be careful!
- User interface: do you need buttons, or screens?
- Documentation/Tutorial availability: very important - no documentation means more frustration!

### 6. Price

> **Price** - what is your budget? Keep this in mind when shopping but don't consider buying anything that will not fulfill the design of your project.


### 7. Programming Interface

<div style="display: grid; grid-gap: 10px;
    grid-template-columns: 1fr 1fr 1fr;">
    <img src="https://i.imgur.com/q1DzSwE.png">
    <img src="https://i.imgur.com/jsayur0.png">
    <img src="https://i.imgur.com/wvgfUz4.png">
</div>

> **Programming** - less of a consideration but **very** important. Your time is valuable, so something easier to program may save you time/money/frustration even if it's a little more expensive

- External interface needed? (i.e. FTDI): if everytime you need to develop something for your board you need a lot of cables, and a lot of steps... it may not be the best option
- Arduino IDE compatible: for beginners, this IDE is the best
- Platformio compatible: for more advanced users, but much faster/confortable
- Other software/hardware tools needed?
 
!!! info
    Quoted info taken from the excellent Adafruit guide [How to Choose a Microcontroller](https://learn.adafruit.com/how-to-choose-a-microcontroller)

## Places to look in

1. [Adafruit](https://learn.adafruit.com)
2. [Arduino](https://docs.arduino.cc)
3. [Sparkfun](https://learn.sparkfun.com)
4. [Github](https://github.com)/[lab](https://gitlab.com) [hackaday](https://hackaday.com) for inspiration/references/documentation - **if there is nothing, we don't recommend to buy it unless you have a lot of experience!**

!!! info "More references"
    - Adafruit guide: [Selecting an Arduino](https://learn.adafruit.com/adafruit-arduino-selection-guide)
    - Adafruit guide: [How to Choose a Microcontroller](https://learn.adafruit.com/how-to-choose-a-microcontroller)
    - Sparkfun guide: [Choosing an Arduino for Your Project](https://learn.sparkfun.com/tutorials/choosing-an-arduino-for-your-project/all)
    - [Learn how to choose the right Arduino board for your project.](https://maker.pro/arduino/tutorial/how-to-choose-the-right-arduino-board-for-your-project)
    - Sparkfun [ARDUINO COMPARISON GUIDE](https://www.sparkfun.com/standard_arduino_comparison_guide)
    - Seeedstudio: [Arduino Boards Selection Guide](https://www.seeedstudio.com/blog/2020/02/20/arduino-boards-selection-guide/)
