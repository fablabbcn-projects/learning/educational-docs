# Intro to HTML

In order to go through the basics of a webpage, let's go thought some examples:
!!! Note
    - [Here you can download all the files needed for this guide](assets/HTML_Class_Examples/HTML_Class_Examples.rar)

First of all, let's try to understand the basic structure of a website:

   - [First example](assets/HTML_Class_Examples/01-Basic Structure.html)


The second example is about some of the different items that we can use in HTML.

   - [Second example](assets/HTML_Class_Examples/02-Basic Items.html)

Let's give it some style!

   - [Third example](assets/HTML_Class_Examples/03-Style.html)

What is CSS?

   - [Fourth example](assets/HTML_Class_Examples/04-StyleInCSS.html)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSEWikJkQBGpCkUwxrVq9s84cYB_VK_V-BC1PKFPaQ5JdrhyxDYehdiyaQXkRXIt4SxEdLHORAz6DbP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Resources

- Repo to learn how the web works: [https://github.com/vasanthk/how-web-works](https://github.com/vasanthk/how-web-works)
