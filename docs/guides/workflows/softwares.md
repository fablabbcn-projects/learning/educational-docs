# FABACADEMY SOFTWARE

This is a document compiling most of the common software used during fabacademy by most of the students.

As always, this is not mandatory but a recommendation to help you choose the program of your liking and find what best suits you.

## Text editor

- [Sublime Text](https://www.sublimetext.com/) ("Free" and NOT Opensource)
- [Atom](https://atom.io/)
- [Brackets](http://brackets.io/)
- [VIM](https://www.vim.org/)(Free and Opensource)
- [VSCodium](https://vscodium.com)

## Design 2D

### Raster

- [Gimp](https://www.gimp.org/) (Free + Opensource)
- [Krita](https://krita.org/es/) (Free)
- [Photoshop](https://www.adobe.com/es/products/photoshop.html)

### Vector

- [Inkscape](https://inkscape.org/en/) (Free + Opensource)
- [Illustrator](https://www.adobe.com/es/products/illustrator.html)

## Design 3D

### Parametric

- [Freecad](http://www.freecadweb.org/?lang=eng_EN) (Free + Opensource)
- [Fusion 360](https://www.autodesk.com/products/fusion-360/overview) (Free 3-year education license, not free as in freedom, not Opensource)
- [Grasshopper-rhino](https://www.grasshopper3d.com/)

### Nurbs

- [Rhinoceros 6](http://www.rhino3d.com/en/) (Commercial 3D Cad software)

### Meshes

- [Blender](https://www.blender.org/) (Free + Opensource 3D software)
- [Meshmixer](https://meshmixer.com/) (Free)
- [ZBRUSH](https://zbrushcore.com/mini/)

### Coding

- [OpenScad](http://www.openscad.org/)(Free and Opensource)

### 3Dscan

- [Metashape](https://www.agisoft.com/)
- [kinect-Skannet](https://skanect.occipital.com/)
- [Meshroom](https://alicevision.org/)

### Milling

- [Rhinocam]()
- [Meshcam](http://www.grzsoftware.com/)

### Slicer

- [Prusa Slic3r](https://www.prusa3d.com/prusaslicer/)(Free and Opensource)
- [Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura)(Free and Opensource GUI? - TBC)
- [Slicer3d](https://slic3r.org/)(Free and Opensource)
- [Chitubox](https://www.chitubox.com/en)

## Physical Computing

### Electronics and PCB

- [Kicad](http://kicad-pcb.org/download/)(Free and Opensource)
- [Arduino IDE](https://www.arduino.cc/en/Guide/HomePage)(Free and Opensource)
- [Visual Studio](https://visualstudio.microsoft.com/es/)
- [Platformio](https://platformio.org/)(Free and Opensource)

### Interface and Application Programming

- [Processing](https://processing.org/)(Free and Opensource)
- [MIT APP INVENTOR](https://appinventor.mit.edu/)
