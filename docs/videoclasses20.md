This section is a compilation of the video classes given by the Fab Lab Barcelona team, for the Fabacademy / MDEF courses


## 2019-20

#### [Machine Design] - SPML Machine

<iframe width="640" height="350" src="https://www.youtube.com/embed/KGTLqm-fLL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200528-075124_FABACADEMY.txt)


#### [Wildcard week] - How to make a remote controlled CNC draw in the sand (The CNC by the sea)

<iframe width="640" height="350" src="https://www.youtube.com/embed/OykLwclgaWA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200522-084031_FABACADEMY.txt)

#### [Thursday’s hyperconnected] - The CNC by the sea

 <iframe width="640" height="350" src="https://www.youtube.com/embed/TaoDNlwIKss" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200521-075802_FABACADEMY.txt)


#### [Molding and casting] - Design & Machining, Materials & Processes

<iframe width="640" height="350" src="https://www.youtube.com/embed/07gnve3sMdk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200514-075147_FABACADEMY.txt)

#### [Networking and Communications] - What we learnt from onions

<iframe width="640" height="350" src="https://www.youtube.com/embed/cLQ8mpU6Ndo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200512-080012_FABACADEMY.txt)


#### [Networking and communications] - DIY network

<iframe width="640" height="350" src="https://www.youtube.com/embed/9WhlkxgKZEg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



#### [Networking and communications] - Wired and OTA Networking

<iframe width="640" height="350" src="https://www.youtube.com/embed/bvYz8JmwVcE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200507-075641_FABACADEMY.txt)


#### [CODE CLUB] - Computer vision II

<iframe width="640" height="350" src="https://www.youtube.com/embed/Uxmo4FAeypU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200504-144404_FABACADEMY.txt)


#### [Interface and application programming] - Blender as Interface

<iframe width="640" height="350" src="https://www.youtube.com/embed/vB9frgHgrQw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200504-075456_-FABACADEM.txt)

#### [Invention, Intellectual Property & Income]- From All Rights Retained to All Rights

<iframe width="640" height="350" src="https://www.youtube.com/embed/MVXPZZxUbk4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200430-075450_FABACADEMY.txt)


#### [MAKER CLUB] - Grasshopper III

<iframe width="640" height="350" src="https://www.youtube.com/embed/PU_gbBR8GlA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200427-141817_FABACADEMY.txt)


#### [Interface and application programming] - Unity

<iframe width="640" height="350" src="https://www.youtube.com/embed/mUABdt7CA_w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200427-075923_-FABACADEM.txt)


#### [Interface and application programming] - Aframe

<iframe width="640" height="350" src="https://www.youtube.com/embed/nQ1QHPwhdlo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200424-074906_FABACADEMY.txt)


#### [Interface and application programming] - Web application (Html)

<iframe width="640" height="350" src="https://www.youtube.com/embed/HPgQt-zPzp0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200424-074906_FABACADEMY.txt)


#### [Interface and application programming] MIT App Inventor

<iframe width="640" height="350" src="https://www.youtube.com/embed/d1yDCmgmESc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200423-074954_FABACADEMY.txt)


#### [Interface and application programming] Processing, P5

<iframe width="640" height="350" src="https://www.youtube.com/embed/Z9Snd1YDLGs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200423-074954_FABACADEMY.txt)


#### [Output Devices] - Debugging Electronics

<iframe width="640" height="350" src="https://www.youtube.com/embed/pwGW0POQuWs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200421-074706_FABACADEMY_1920x1080.txt)


#### [CODE CLUB] - Computer vision

<iframe width="640" height="350" src="https://www.youtube.com/embed/xyGJxL-mx94" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200420-140506_FABACADEMY.txt)

#### [Computer Controlled Machining] - RHINOCAM Debugging class

<iframe width="640" height="350" src="https://www.youtube.com/embed/IUmUPOFcs2A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200420-090401_FABACADEMY.txt)

#### [Output devices] - DIY Actuators

<iframe width="640" height="350" src="https://www.youtube.com/embed/oCYF5sEssEc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200417-073222_FABACADEMY.txt)

#### [Output devices] - Output Device & Debugging

<iframe width="640" height="350" src="https://www.youtube.com/embed/nkNC_ZoeqTg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200416-074733_FABACADEMY.txt)

#### [MAKE CLUB] - Grasshopper, Scripting visualization II

<iframe width="640" height="350" src="https://www.youtube.com/embed/5GtFz6iF-J0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200406-140807_FABACADEMY.txt)

#### [MAKE CLUB] - Grasshopper, Scripting visualization

<iframe width="640" height="350" src="https://www.youtube.com/embed/s-BmAi9N8J8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200402-075403_FABACADEMY.txt)

#### [CODE CLUB] - Interfacing with hardware (sensors)

<iframe width="640" height="350" src="https://www.youtube.com/embed/SSJrf14P4Tw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200330-134906_FABACADEMY.txt)


#### [Input devices] - From signals to events (DSP)

<iframe width="640" height="350" src="https://www.youtube.com/embed/f2is1o5Kiyw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200326-085410_FABACADEMY.txt)

#### [Input devices] - DIY Sensors

<iframe width="640" height="350" src="https://www.youtube.com/embed/FUl5C6Tl95c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200326-085410_FABACADEMY.txt)

#### [Input devices] - Sensors/Inputs class

<iframe width="640" height="350" src="https://www.youtube.com/embed/S6-0iyKRZ0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200326-085410_FABACADEMY.txt)


#### [MAKER CLUB] - Grasshopper, Firefly

<iframe width="640" height="350" src="https://www.youtube.com/embed/yjE65bGlhno" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200323-150058_FABACADEMY.txt)


#### [Embedded programming] - Platformio

<iframe width="640" height="350" src="https://www.youtube.com/embed/iqDlszxzEzI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200320-085837_FABACADEMY.txt)


#### [Embedded programming] - How computer works

<iframe width="640" height="350" src="https://www.youtube.com/embed/8iHOhcFMza8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Chat text](material/extras/videoclasses/GMT20200319-090255_FABACADEMY.txt)


#### [MDEF] - Weekly review

<iframe width="640" height="350" src="https://www.youtube.com/embed/hQLnhg_aLgQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### [Computer Controlled Machining] - Aspire and Rhinocam

<iframe width="640" height="350" src="https://www.youtube.com/embed/_5R5G5IB18A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### [CODE CLUB] - Interfacing with the web II

<iframe width="640" height="350" src="https://www.youtube.com/embed/KEo06hagYb0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### [CODE CLUB] - Interfacing with the web I

<iframe width="640" height="350" src="https://www.youtube.com/embed/biMaY2PgO5w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
