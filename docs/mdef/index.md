# MDEF

## Welcome to Digital Prototyping for Design

[SILLABUS INFORMATION](https://mdef.fablabbcn.org/2023-24/year-1/t2/digital-prototyping-for-design/)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQkoCk_kSXpPa--qUJTYmzBf0ZU9POwMoZEmNnbg0I5Jy9dPtPJGEVnYOhoSIKM129_8WMZxs_eq2xD/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

---
## Modules

<div style="display:flex; width: 100%; align-items: flex-start; align-content: flex-start; gap: 20px; flex-wrap:wrap;">
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/pasteprinting/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%">
    <img src="icons/paste.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/3dPrint/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%">
    <img src="icons/3dprinting.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%;
     object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/biomaterials/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%" >
    <img src="icons/bio.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/cnc/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%" >
    <img src="icons/cnc.png"></img>
    </div>
    </a>
        <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
        object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/interactionmaking/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%">
    <img src="icons/sensing.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/3dscanning/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%">
    <img src="icons/scan.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/parametric/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%" >
    <img src="icons/parametric.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/moulding/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%" >
    <img src="icons/moulding.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/growmaterials/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%" >
    <img src="icons/grow.png"></img>
    </div>
    </a>
    <a style="box-shadow: 5px 5px 0px 0px #181040; display: flex; flex-direction: column; align-items: flex-start; width: 48%; height: 100%; 
    object-fit: cover; background-color: #CABFFF" href="https://fablabbcn-projects.gitlab.io/learning/educational-docs/mdef/classes/2024/visuals/">
    <div style="display:flex; align-content: center; justify-content:center; width: 100%" >
    <img src="icons/extend.png"></img>
    </div>
    </a>
</div>


---

<!-- Button to activate the script -->
<button onclick="openRandomPage()">Open Random Web Page</button>

<script>
// List of web pages
var webPages = [
    "https://niente010.github.io/MDEF_website/#welcome",
    "https://niente010.github.io/MDEF_website/#welcome",
    "https://floraroseberkowitz.github.io/",
    'https://vania-bisbal.github.io/repo-website/',
    'https://everardocastro.github.io/mdef1/',
    'https://jdlm92.github.io/MDEFsite/',
    'https://33dudu.github.io/magicreator/',
    'https://anthuanetf.github.io/MDEF/',
    'https://annafedele.github.io/mdef/',
    'https://panchipunchi.github.io/mdef1/',
    'https://oliver-lloyd-mdef.github.io/Oliver-MDEF-Portfolio/',
    'https://annnalozano.github.io/PaginaWeb/',
    'https://minnie-at-iaac.github.io/',
    'https://sophma.github.io/myMDEFportfolio/',
    'https://jmuozan.github.io/mdef-website/',
    'https://grayson-iaac.github.io/MDEF/',
    'https://dhrishyaramadass.github.io/mdefwebsite/',
    'https://carmenrobres.github.io/portfolio/',
    'https://marius-schairer.github.io/MDEF_Documentation/',
    'https://nuriavalsells.github.io/MDEF/',
    'https://avilabon.github.io/MDEF_Albert/',
    'https://caglaralkan.github.io/MDEF/',
    
    // Add more URLs as needed
];

// Function to open a random web page in a new tab
function openRandomPage() {
    // Get a random index within the length of the webPages array
    var randomIndex = Math.floor(Math.random() * webPages.length);
    // Get the URL at the random index
    var randomPage = webPages[randomIndex];
    // Open the random page in a new tab
    window.open(randomPage, '_blank');
}
</script>

----------------

## Fab Test

- [Fab Test P1 - MDEF 2023-24](https://docs.google.com/forms/d/e/1FAIpQLSd3XqvLmRvKPZYoZMlK0eIt6U-tQ2NKkSltm537AeANg7VUmQ/viewform)

- [Fab Test P2 - MDEF 2023-24](https://docs.google.com/forms/d/e/1FAIpQLScZX5zMPr116Zz17dw2-cS_iMHtZohZkUl-bfXIajtSbfriIQ/closedform)

- [Fab Test P3 - MDEF 2023-24](https://docs.google.com/forms/d/e/1FAIpQLSeyH6D9hNUiBenxoA9N2A1EDWgqU4NI36P6CvVDbWYynoFXoA/viewform)
