# 3D Fabrication & Scanning

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Presentation 3D Printing Inspirational')}}

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Presentation 3D Scanning Detailed')}}

## MDEF Task

[ACCESS TO THE MIRO BOARD](https://miro.com/app/board/uXjVPrnS_rQ=/?share_link_id=220271332338)

Fabricate a small prototype with the 3dprinter & scanner, choose one of this options:
  - Scan your something and print it!
    ![](https://gomeasure3d.com/wp-content/uploads/2021/02/featured-image-how-to-3d-scan-to-print.jpg)
  - Print something and scan it!
  ![](https://3dprintrva.com/wp-content/uploads/2021/07/LegoSkele-sm.png)

##  Scanning Softwares

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Scanning Softwares')}}

##  3D Equipment Usage Videos

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## 3D Printing Equipment Usage Videos')}}

## G-CODE GENERATORS FOR 3D PRINTERS

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## G-Code generators for 3D Printers')}}

#### 3D Printable Things

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## 3D Printable Things')}}
