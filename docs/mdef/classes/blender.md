# Blender

!!! warring
    The content of this section is a copy of a working document that is constantly being updated.
    To see the latest updates visit here: https://hackmd.io/@vico/S1bQ_yL_x


![](https://www.blender.org/wp-content/uploads/2015/03/blender_logo_socket-1-1280x391.png)

## Why I use Blender

There is always a lot of discussion on wich 3d modeling tool is the best to learn and use, most of the time it comes down to personal preference and becomes an almost religious fight.

In my opinion we should talk and think about some other aspects on why we choose one tool or the other, here are some thoughs:

* The tools that we use, shape ourselves and the things we do.
* The design of the tool, limits our capacity to create solutions, and influences how we face problems.
* Our creative freedom has to struggle against the rules imposed by the tool itself.
* The reasons that motivate tool designer decisions are not always aligned with our objectives.
* In commercial tools, profit is prioritized in design decisions.
* A tool that is created by a comunity has more potential to be diverse in the paradigms it imposes.
* Modifying the tool itself should be a normal part of the creative process to allow a wider range of outputs.
* An open source tool by definition allows (and even promotes) us to change it to fit our needs.


## Introduction to the Blender project

Blender is the free and open source 3D creation suite. It supports the entirety of the 3D pipeline modeling, rigging, animation, simulation, rendering, compositing and motion tracking, even video editing and game creation. Advanced users employ Blender’s API for Python scripting to customize the application and write specialized tools; often these are included in Blender’s future releases. Blender is well suited to individuals and small studios who benefit from its unified pipeline and responsive development process. Examples from many Blender-based projects are available in the showcase.

Blender is cross-platform and runs equally well on Linux, Windows, and Macintosh computers. Its interface uses OpenGL to provide a consistent experience. To confirm specific compatibility, the list of supported platforms indicates those regularly tested by the development team.

As a community-driven project under the GNU General Public License (GPL), the public is empowered to make small and large changes to the code base, which leads to new features, responsive bug fixes, and better usability. Blender has no price tag, but you can invest, participate, and help to advance a powerful collaborative tool: Blender is your own 3D software.

_from [blender.org](https://www.blender.org/about/)_

## [History](https://www.blender.org/foundation/history/)
... Roosendaal wrote the first source files titled “Blender” on the 2nd of January, 1994, still considered Blender’s official birthday...

... In May of 2002, he started a non-profit, the Blender Foundation, with the intention of making Blender open-source. His hope was to create a public monument to Blender, and give everyone who had worked on the Blender project the chance to use it for their portfolios. In July of the same year, he launched the first-ever crowdfunding campaign: Free Blender. Thanks to Blender’s community of 250,000 users, the Blender Foundation was able to raise one hundred and ten thousand euros in just seven weeks — sufficient to regain Blender from its investors. 

On Sunday, October 13th, 2002, Blender was released under the terms of the GNU General Public Licence, the strictest possible open-source contract. Not only would Blender be free, but its source code would remain free, forever, to be used for any purpose whatsoever...

[![](https://i.imgur.com/D4jmmcs.jpg =200x)](https://i.imgur.com/D4jmmcs.jpg)

_Blender history in [splash screens](https://commons.wikimedia.org/wiki/Category:Blender_splash_screens)_

## Free (libre) and Open Source

![](https://i.imgur.com/VHfyQCR.png)

Blender license [information](https://www.blender.org/about/license/)

**In a few sentences, what is the GNU GPL?**

0. You are free to use Blender, for any purpose
1. You are free to distribute Blender
2. You can study how Blender works and change it
3. You can distribute changed versions of Blender

_In the last case you have the obligation to also publish the changed source code as GPL._

**How does the GPL and Blender benefit me?**
1. The GPL allows for developers to work on Blender without worry that their work could be copied into a closed application. They can keep their own copyright on the code even, reserving the rights to sell or license it as they wish to.
2. The GPL makes it so that all contributers must make their code open, this means that if someone distributes a version of Blender with a cool feature, everyone can have  access to it.
3. The GPL ensures that all future Blender versions will always be available as Free Software, providing the core freedom users and developers expect from it.

One of the main benefits of Blender is that it’s truly “your own software”. You or your studio can treat it as in-house software equally to the big powerhouses in the film industry.


## Comunity
The importance of the [community](https://www.blender.org/community/) in this software development model.

[![](https://i.imgur.com/THDPniD.png =300x)](https://video.blender.org/w/f7nWckAawPoYxwcsSkiQrr)

## Financing

[![](https://i.imgur.com/hepaSiw.png)](https://fund.blender.org/)

![](https://www.blender.org/wp-content/uploads/2021/06/blender_funding_v2.png)

Blender [Foundation](https://www.blender.org/about/)

:::info
Blender [donations page](https://www.blender.org/about/donations/)
:::

## Development
Blender has used the idea of [Open Movies](https://video.blender.org/video-channels/blender_open_movies/videos) as a way to develop and fund free (libre) software.

_As a way to stress-test Blender’s increasing power, the Blender Foundation challenged its community’s most talented artists to make an animated 3D short film. The only criterion was that they had to use open source tools, with Blender prime among them._

_Under the codename “Project Orange,” this project began in 2005, resulting in Elephants Dream, a surreal adventure through a gigantic machine. The film and all its assets were made freely available under a Creative Commons licence._

_After the success of Elephants Dream, the Blender Institute was established in the summer of 2007. As well as helping to define the Blender Foundation’s goals, the Blender Institute comprised a permanent office and studio, with the express intention of generating Open Projects related to 3D movies, games or visual effects. As part of its output, the Blender Institute has created a series of Open Movies in collaboration with leading artists. They include the comedy Big Buck Bunny (2008), science fiction thriller Tears of Steel (2012), a poetic fantasy Spring (2019), and horror-comedy Sprite Fright (2021)._

[![](https://i.imgur.com/CA8C0IN.jpg =x250)](https://studio.blender.org/films/elephants-dream/) [![](https://i.imgur.com/WoVzIrJ.jpg =x250)](https://studio.blender.org/films/big-buck-bunny/) [![](https://i.imgur.com/rMTcAOj.png =x250)](https://studio.blender.org/films/spring/) [![](https://i.imgur.com/rJR9bzu.png =x250)](https://video.blender.org/w/a69d68a5-a0e0-4a80-9d66-49f093c97aaf)

:::info
Blender [developers blog](https://code.blender.org/)
Blender Developer [Wiki](https://wiki.blender.org/index.php/Main_Page)
:::

## [Features](https://www.blender.org/features)

## User Interface
![](https://code.blender.org/wp-content/uploads/2021/10/blender_3_0_beta-1024x550.png)

* Window system
* Screen types
* Personalization - Split and join
* Editors
    * 3D view
        * Tool shelf
        * Properties shelf
    * Properties editor
    * Outliner
    * Info Editor (top bar)
    * Animation
        * Graph
        * Dopesheet
        * NLA editor
        * Timeline
    * Node Editor
        * Geometry
        * Composition
        * Materials
        * Textures
        * Animation Nodes Addon [Github,](https://github.com/JacquesLucke/animation_nodes/releases/tag/v2.0) [docs](https://animation-nodes-manual.readthedocs.io/en/latest/)
        * [Sverchok](https://github.com/nortikin/sverchok/) Addon
    * UV Image editor
    * Video Sequence editor
    * Text editor
* Keyboard Shortcuts and mouse movement

[![](https://giudansky.com/images/articoli/2021/03/blender-infographic-lowres-preview.jpg =200x)](https://www.giudansky.com/downloads/learnwithaposter/blender-poster-infographic/blender-infographic-SM-2500.png)

## Viewport navigation
* Changing views
* Perspective
* Camera

## Manipulating objects
* Grab, Rotate and Scale (keyboard, mouse, numeric)
* Duplicate
* Duplicate linked
* Snap
* 3d cursor

## Modeling

### Mesh modeling
![](https://www.blender.org/wp-content/uploads/2019/07/modeling02-1280x720.jpg?x79440)
[Hard surface modelig timelapse](https://www.youtube.com/watch?v=E7lqyJh-pVc&list=PL5zJo_b1ZvzGQ-aK83aVbr8EcOAWRTCCr)
* Object manipulation (shortcuts and manipulator) 
* Object mode
* Edit mode
* Primitives (extra meshes addons, gears, bolts, etc)
* Extruding
* Snaps
* 3D cursor
* Pivot center
* Using Collections

### Modifiers
* Array
* Mirror
* Subsurface
* Decimate
* Solidify
* Bevel
* Boolean

### Geometry nodes

![](https://docs.blender.org/manual/en/latest/_images/modeling_geometry-nodes_introduction_properties.png)

> The geometry node tree connected to a modifier is a Node Group. The geometry from the state before the modifier (the original geometry or the result of the previous modifier) will be passed to the Group Input node. Then the node group can operate on the geometry and pass an output to the Group Output node, where it will be passed to the next modifier.

from blender [documentation](https://docs.blender.org/manual/en/latest/modeling/geometry_nodes/index.html)

### Others
* Curve modeling
* Geometry nodes
* Surface modeling
* Metaball
* [Sverchok](https://github.com/nortikin/sverchok/)


## Sculpting
![](https://i.imgur.com/QMZ1G8r.png)

* Brushes
* Remesh

Sculp timelapse example [video](https://www.youtube.com/watch?v=TLb_CCoWn5o)

## Animation
![](https://i.imgur.com/XMNM48r.png)
* Basic keyframing
* Dopesheet editing
* Curve edition 
    * Interpolation
* Animation nodes [demo](https://www.youtube.com/watch?v=slQphRSC6W0)

## Materials and textures
![](https://i.imgur.com/DT6ewFq.png)
* Asign materials
* UV mapping

!!! info
    [How to Use Blender's new ULTIMATE Shader: Principled](https://www.blenderguru.com/tutorials/2017/6/21/how-to-use-blenders-new-ultimate-shader-principled-bsdf) on blender guru.


## Rendering
![](https://www.blender.org/wp-content/uploads/2019/07/blender_render-1280x720.jpg?x79440)
* Cycles renderer
* Eevee [example](https://www.youtube.com/watch?v=nxrwx7nmS5A)

!!! info
    - Rendering on [blender manual](https://docs.blender.org/manual/en/latest/render/index.html)
    - [Rendering with cycles](https://hackmd.io/0hq9wSKvS5y7orq9CZqHrg) (in spanish)
    - [Enviroment lighting](https://hackmd.io/vF_cwfZKQiSN8dvX3m1k8A?both) (in spanish)


### Working display
* Viewport shading
* Light, shadows, cavity, etc
* Matcap, colors, etc

## Grease pencil
![](https://www.blender.org/wp-content/uploads/2019/07/hero_p31_screenshot-1280x720.jpg?x79440)
* [Intro video](https://www.youtube.com/watch?v=pywbPQD9vYU)
* Anotation tool
* Grease pencil object
[To 3d](https://www.youtube.com/watch?v=ONV-Wy8Z7-w)
[Another](https://www.youtube.com/watch?v=qne29KKef58)
[Hero demo](https://vimeo.com/266060937)

## FX
![](https://i.imgur.com/FTvlzJe.jpg)

* Cloth
* Particles
* Field forces (Test with particles and smoke)
* Fire and smoke
* Ocean modifier
* Hair
* Fluids

Example [video](https://www.youtube.com/watch?v=nU6PF8vuEBk)

## Compositing
![](https://www.blender.org/wp-content/uploads/2019/07/blender_vfx-1280x720.jpg?x79440)

* Basic node usage
* Camera tracking [Example video](https://www.blenderguru.com/tutorials/introduction-to-camera-tracking)
* Tracking and Fluids video [example](https://www.youtube.com/watch?v=SizZLCdAhR8)

!!! info
    [Blender Compositor notes](https://hackmd.io/pt_peqytQiGzSTewGCNyrA) (in spanish)
    [Motion Traking](https://hackmd.io/_MZexh9fREqiZqRZuCVMlA) with Blender (in spanish)
    [Compositing 3d models](https://hackmd.io/_ua-ggoJQuiZP9RxGpEwew?view) (in spanish)


## Video editing
![](https://www.blender.org/wp-content/uploads/2019/07/vse_custom_grade.jpg)

## Python scripting
Fabacademy Blender IO class [documentation](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/material/extras/week12/blender/#intro)
Blender Python API [documentation](https://docs.blender.org/api/current/)

## Addons
[Blender market addons](https://blendermarket.com/categories/scripts-and-addons)
[BlenderCAM](https://github.com/vilemduha/blendercam)

!!! note
    **Resources**
    * Blender [FAQ](https://www.blender.org/support/faq/)
    * Blender fundamentals [videos](https://video.blender.org/video-channels/blender_fundamentals/videos)
    * Blender video [channels](https://video.blender.org/accounts/blender/video-channels)
    * Blender [manual](https://docs.blender.org/manual/)
    * Blender IO Fablab class [documentation](https://hackmd.io/vIgyQbZYRsKZuxc89xQ5KA)
    * Blender CAM Fablab [documentation](https://hackmd.io/0E9ruq_UQjSUUmh2qU8Jww)

## MDEF Task

!!! warning "Task"
    Experiment with the tool, and reflect on how it could be used in your daily practice.
