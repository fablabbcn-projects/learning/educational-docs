# How to set up your documentation

In this guide you are going to find the basic instructions on how to create your documentation webpage that you will use to document your MDEF journy. To do it this guide will cover different points:

- [The Terminal](#the-terminal): First you need to know basic command line commands to navigate througth files in your computer.
- [Git](#git): What is it? How to use version control.
- [Markdown](#markdown): Let's make web easy.
- [Mkdocs](#mkdocs): How to configure your page.
- [How to start from the template](#how-to-start-from-the-template): it's time to create you page!

## The Terminal

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/DEC_VT100_terminal.jpg/1200px-DEC_VT100_terminal.jpg)

The terminal is a scary thing at the beggining, but you are going to get use to it and, if you do not want, you are not going to us it much. But to get started with your page it's important that you learn how to navigate around your computer using the terminal. Here there is a list of the more basic commands that you are going to use. 

```
pwd: Print Work Directory
```

The command pwd will print the directory where you are working in that terminal, it can be understand as the folder you are in. 

```
ls: List files
```

Using the ls command you will see whats inside your actual directory, all files and folder will be listed. Is very usefull to know where to navigate.

```
cd: Change Diretory
```

To navigate to a different folder you will use the cd command. By default it needs a relative path for the new folder inside the actual one, to go back outside of the actual folder you need to use cd .. to go back one level. If you the full path of the folder you want to navigate to you can use cd /folder_path to enter it. 

```
mkdir: Make new Directory
```

This will create a new folder with the specified name like mkdir newFolderName.

```
touch: Create a new file
```

With the command touch you can create a new file, again specifiny the name as touch newFileName. 

```
clear: Clear the terminal
```

Clear or Ctrl + L you can clear the output of the terminal to clean your screen. 

!!! Note
	Using the terminal there are some shortcut that come handy when working. The first one is to use the tab key to autocomplete the names of the files, and the second one is to use the keyboard Up and Down arrows to navigate to the history of commands. 


## Git

![](https://git-scm.com/images/logos/downloads/Git-Logo-Black.png)

Git is a free and open source distributed version control system. It will allow you to keep track of the files and history of your repository that will contain your webpage. Together with Git, you are going to be using GitHub to host your repository online and serve your page to the internet. 

### Git Local/Remote Structure

As a reference, this is a basic structure of the local/remote structure using git.

![](https://image.slidesharecdn.com/gitbranchstregagycasestudywoogenius-140314152231-phpapp01/95/git-branch-stregagy-case-study-2-638.jpg?cb=1413975847)

!!! warning "Time to follow a guide"
    Follow [this guide](git-setup.md) to set it all up

### Interactions

Here there are some basic interactions you will normally do with your git repositories.

#### Cloning

Cloning takes a remote repository and literally _clones it_ into a local one:

```
git clone git@gitlab.com;flu/supermegasuperguay.git
```

#### Status

Shows what is the status of your local/remote repository - if there are local or remote changes, or things to be pushed:

```
git status
```

#### Log

Shows the history of the local and remote repositories as a list of commits with more info:

```
git log
```

#### Basic changes workflow

You will most likely be using this workflow for git

##### Pull-add-commit-push

- Download the last copy from the online repository and merge it with your local working directory:

    ```
    git pull
    ```

- Add the new files you added to git
    
    ```
    git add index.html
    ```

    or to review each `sloc`:

    ```
    git add -p
    ```

- Now name your update, so you know what you changed with this push.
    
    ```
    git commit -m "COMMIT MESSAGE"
    ```

!!! danger "About the commit message"
    This is a general point of failure for many many students (and instructors) that do not make a relevant commit message. 

    Write a **meaningful commit message**. This should answer the question: 
    > "If I apply this commit, I will... <commit message>". 
    
    For example: 
    > "uploading final project idea"
    
    This is not OK at all and will not help anyone to trace problems (and they will happen):

    ![](assets/bad-repo.png)

- Upload to the Repository

    ``` 
    git push
    ```

!!! warning "This is 1% of what git does"
    Git is much more than this. Do not hesitate to visit [the docs](https://git-scm.com/doc) to get more info or to follow a tutorial on more advanced features.


## Markdown

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/1200px-Markdown-mark.svg.png)

When it comes to web development there are tons of tools to do it. The basic way to create a webpage is by using HTML code and normally it's styled by CSS and animated by JavaScript. 

Markdown is a lightweight markup language for creating formatted text using a plain-text editor. It makes web-development more human readable, and also easier to start with. It's not a replacement for HTML, in fact it is converted to html in the deployment of the page. 

Here there is a nice [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/) with the basic syntax. 

Another good tool to start with quickly and to create notes is [Hackmd.io](https://hackmd.io/).

## Mkdocs

Mkdocs is a static site generator that's geared towards building project documentation. Documentation source files are written in Markdown, and configured with a single YAML configuration file, making it customizable and easy to start with. There are many themes available, but we will be using Material theme for this page. 

Check [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/) page for more documentation on how to customize and use this theme. 

### YAML file

In the YAML file is where the magic happens. This file will define how your page looks like, where you can change the font, the colours, the logos and navigation of your page, and then all the content will be stored into the docs folder. 

### Installing Mkdocs localy

You can install mkdocs localy to serve your page localy, without pushing it to a server. To do so, follow their [documentation](https://www.mkdocs.org/getting-started/) to install it, you will need Python and Pip to do it. 

## How to start from the template

There are several ways to have a repository:

- In the online git platform, create a repository and then clone it
- In your terminal initialising it from scratch
- Cloning an existing one (most likely)

[Link to template](https://github.com/fablabbcn/mdef-template)

### Online Git platform

If you are using and online web service for git ([Github](https://github.com/), [Gitlab](https://gitlab.com/), [bitbucket](https://bitbucket.org/)...), you can create a new project or repo by simply going to one of these below and then follow the instructions to _clone it_:

- [New github repo](https://github.com/new)
- [New gitlab repo](https://gitlab.com/projects/new)
- and so on...

### Terminal from scratch

!!! example "Step-by-step"
    - Navigate to the folder where you want to put or create your repo

    - In the terminal, type:

    ```
    git init
    ```

    - If you have a remote already, you can just do:

    ```
    git remote add origin git@github.com:gitusername/repository.git
    ```

    - And then pull the remote:

    ```
    git pull
    ```

### Cloning an existing one

If you have a template or an online repository you want to reuse, you can navigate with your terminal to the desired destination folder and do:

!!! example "MDEF Students"

    - Navigate to the folder where you want to put or create your repo

        ```
        cd folder-for-your-project
        ```

    - Clone your student repository (ssh)

        ```
        git clone git@github.com:fablabbcn/mdef-template.git
        ```

    - Create your own project in Github - [Direct access](https://github.com/new) - Make sure that is **public!!**

    - Tell your local repository to push to the new project in Github:

    ```
    git remote rename origin old-origin
    ```

    ```
    git remote add origin git@github.com:username/your-repo-name.git
    ```

    - Do some edits and then add-commit-push (like this for the first commit):

    ```
    git add FILENAME
    ```

    ```
    git commit -m "My first commit"
    ```

    ```
    git push -u origin --all
    ```

    - For further changes, your workflow should be:

    ```
    git add FILENAME
    ```

    ```
    git commit -m "My other commit"
    ```

    ```    
    git push
    ```

!!! danger "About the commit message"
    This is a general point of failure for many many students (and instructors) that do not make a relevant commit message.

    Write a **meaningful commit message**. This should answer the question:
    > "If I apply this commit, I will... <commit message>".

    For example:
    > "uploading final project idea"

    This is not OK at all and will not help anyone to trace problems (and they will happen):

    ![](../../material/extras/week01/assets/bad-repo.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y5cm-__K3sM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>








