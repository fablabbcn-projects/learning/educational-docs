# Sensing the body for meaningful interactions
### DAY 01
![alt text](../assets/pid.png)

[Presentation link](https://docs.google.com/presentation/d/1jjq6EkRKNVDmwJVOUJrNUESB2CE7KrXsnw2HNhGCIU4/edit?usp=sharing)

### DAY 02
![alt text](../assets/pid2.png)

[Presentation link](../assets/pid2.pdf)

[Repo Link](https://github.com/TURBULENTE/prototypingInteractions)

## Resources

- [wekinator](http://www.wekinator.org/)
- [Atlas of emotions](https://atlasofemotions.org/)
- [Face Tracker](https://github.com/kylemcdonald/ofxFaceTracker/releases)
- [Processing](https://processing.org/)
- [DIY WEARABLE TECHNOLOGY](https://www.kobakant.at/DIY/)
- [Hight Low Tech](https://highlowtech.org/)