# 3D Scanning

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Presentation 3D Scanning Detailed')}}

## Overview
{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## 3D Scanning overview')}}

## Softwares

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Scanning Softwares')}}

## Resources

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Scanning resources')}}



