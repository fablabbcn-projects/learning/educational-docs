# 3D Fabrication

## 3D Printing

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQz2yI4gFvbgaM4HMXoI52xg3Zo4pzXJfCnrVnbxghQlLE2x79AeEL9PJYUbK2aAJMjGkQQRKkIT_5Y/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>



###  3D Equipment Usage Videos

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## 3D Printing Equipment Usage Videos')}}

### G-CODE GENERATORS FOR 3D PRINTERS

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## G-Code generators for 3D Printers')}}

### 3D Printable Things

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## 3D Printable Things')}}