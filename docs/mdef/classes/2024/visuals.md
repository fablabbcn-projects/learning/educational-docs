# Extended bodies with expressive Data
### DAY 01
![alt text](../assets/visual1.png)

[Presentation link](https://www.canva.com/design/DAGAicpdwME/QuQozT6Pes9UI6HHiA4hdw/view?utm_content=DAGAicpdwME&utm_campaign=designshare&utm_medium=link&utm_source=viewer)

[Audio Link](../assets/audio.zip)

### DAY 2

![alt text](../assets/visual3.png)

[Presentation link](../assets/visual2.pdf)

[Patches Link](../assets/max-patches.zip)

## Resources

- [Max 8](https://cycling74.com/downloads)