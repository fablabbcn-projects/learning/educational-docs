# Moulding & Casting

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRzfMhPrFH1cocHB752OT8l6kNfQtXIrulGYHCJdSyQbRHffUFIoQfk1L73xKtNGjL1GgLm-k5RGboP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


## Procees

![](/learning/educational-docs/material/extras/week10/assets/week10-ced79873.jpg)
![](/learning/educational-docs/material/extras/week10/assets/week10-38dc8d86.jpg)


## References and resources

- [Mold making with FUSION 360](https://fabacademy.org/2019/labs/sorbonne/students/hanneuse-luc/assignments/week10/)
- [Introduction to Molding and Casting](https://www.youtube.com/watch?v=5FQC7IepTTk) - A lecture about Molding and Casting from Opendot/WeMake in Milan
- [Molding and Casting Guide and Tips](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week12_molding_and_casting/molding_fabmodules.html)
- [Guerrilla Guide](http://lcamtuf.coredump.cx/gcnc/)
- [Mastering CAD and CAM](http://lcamtuf.coredump.cx/gcnc/ch3/)
- [Resin Casting](http://lcamtuf.coredump.cx/gcnc/ch4/)

### Food
- [http://formx.es/products/siliconas/smooth-sil-series/index.php](http://formx.es/products/siliconas/smooth-sil-series/index.php)
- [DANGEROUS POPSICLES](https://vimeo.com/108829150)

<iframe src="https://player.vimeo.com/video/108829150" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/108829150">Dangerous Popsicles</a> from <a href="https://vimeo.com/charlienordstrom">Charlie Nordstrom</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

- [NOISY JELLY](https://vimeo.com/38796545)

 <iframe src="https://player.vimeo.com/video/38796545" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/38796545">NOISY JELLY</a> from <a href="https://vimeo.com/user3131794">Rapha&euml;l Pluvinage</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

- [Chocolates with food safe silicon](http://archive.fabacademy.org/fabacademy2017/fablabseoul/students/217/wk12.html)


### Bio-Recipies

#### Biosilicone:

- Gelatine powder - 48 gr
    **Functions as the polymeer (so it becomes a solid)**
- Glycerine - 48 gr
    **Functions as plasticizer that bonds with the gelatine (makes it flexible).**
- Water - 240 ml/gr
    **To dissolve and mix the polymeer and plasticizer**


- [Loes Bogers - Fabricademy](https://class.textile-academy.org/2020/loes.bogers/files/recipes/biosilicon/)


#### Biosilicone:

- Gelatine powder - 48 gr
    **Functions as the polymeer (so it becomes a solid)**
- Glycerine - 48 gr
    **Functions as plasticizer that bonds with the gelatine (makes it flexible).**
- Water - 240 ml/gr
    **To dissolve and mix the polymeer and plasticizer**


- [Loes Bogers - Fabricademy](https://class.textile-academy.org/2020/loes.bogers/files/recipes/biosilicon/)


#### Pine Resin cast:

- 90g Pine resin
- 10g Carnauba wax
- 40ml 96 Proof alcohol
- 40g Coffe

<iframe width="560" height="315" src="https://www.youtube.com/embed/eSz_IKwz9EU?si=CQLWdWrJd7fVOA4u" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>