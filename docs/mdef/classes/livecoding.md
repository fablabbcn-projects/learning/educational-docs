# Livecoding as Human Interface

![](https://i.imgur.com/ibYZJQM.jpg)

Citlali Hernández aka [Turbulente](https://turbulente.net)

Xavi Domínguez aka [er Lunni](https://xavidominguez.com/)


!!! warring
    The content of this section is a copy of a working document that is constantly being updated.
    To see the latest updates visit here: https://hackmd.io/@fablabbcn/HJvhtz0oV


## Main Goals for today

1. Understand basic concepts to prototype an interface
2. Navigate the immensity of possibilities, tools, technologies, protocols and languages to design and implement interfaces.
3. To learn about the creative process of a transdisciplinary artist.
4. Discovering, understanding and practicing livecoding.
5. Tinkering Human Interfaces

# Interface

## Designing an Interface Application

### Key questions


* **Your interface application in 1 sentence** 
Ex: A user interface that shows the value of a sensor in a website
* **What is my budget ?**
* **How much time do I have?**
* **Is it a proof of concept ?  Is it a prototype ? Is it the final application ?**
* **What technical knowledge do I already have?** **What other profiles do I need on my team?**

### Key Ingredients

![](https://i.imgur.com/kXe0ZEQ.png)



### Basic Concepts

#### Languages

[The Hello World Collection](http://helloworldcollection.de/)

#### Protocols

A protocol is a set of rules that governs how data is transmitted and received over a network.
Internet protocols are the rules and standards that enable communication between devices over the internet.

##### Websocket

![](https://i.imgur.com/ZI3OCs2.png)

WebSocket is a communication protocol that enables two-way communication between client and server over a single, long-lived connection.
It allows for real-time data transfer between the two endpoints without the need for frequent HTTP requests.
WebSocket is commonly used in applications that require real-time updates, such as online gaming, chat applications, and stock market tickers.

##### OSC - Open Sound Control
![](https://i.imgur.com/DCOJDn2.png)

Open Sound Control (OSC) is a protocol for networking sound synthesizers, computers, and other multimedia devices for purposes such as musical performance or show control

## Tools, technologies, protocols and languages to design and implement interfaces

[Miro ](https://miro.com/app/board/uXjVMPLKNA0=/)

<iframe src="https://miro.com/app/live-embed/uXjVMPLKNA0=/?moveToViewport=-3732,-1152,6979,1887&embedId=276240957906" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen width="768" height="432" frameborder="0"></iframe>

## Projects

### Coding Robots - Families - CosmoCaixa
![](https://i.imgur.com/pNzy8T5.png)

[Documentation](https://hackmd.io/@santifu/r1rLGU4y4?type=view)
[Repository](https://gitlab.com/flu/codingrobots)

### Ironskulls ( Azul Petroleo)
https://ironskulls.es/

[Repository](https://oscgonfer.gitlab.io/portfolio//2022/Azul-Petroleo-Mask/)
<iframe width="730" height="415" src="https://www.youtube.com/embed/P9CsZFBkOrY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

![](https://i.imgur.com/eTEIeh9.png)

## Smart Bike Project

Reference: http://fabacademy.xavidominguez.com/final.html
Language:**Arduino, App Inventor,Javascript, html5, php**
Data Interfaces: **MySQL**, **TinyDB**
Networking Communication: **Bluetooth JBtek-HC-05 Wireless**

![](https://i.imgur.com/4Nt2Bza.jpg)

## K3 - Parlant amb la Júlia

[Try](https://teclaegipte.streamlit.app/)

[Repository](https://github.com/xavido/teclaEgipte)

![](https://i.imgur.com/8TlfQJx.png)

## Examples

### SC + Processing ( Serial )

[Repository](https://github.com/fablabbcn/smartcitizen-docs/tree/master/docs/assets/pde)

![](https://i.imgur.com/JZUUp1F.png)


### Processing + Arduino ( Serial Communication )

Input: Arduino with Mois Sensor
Output: Processing + Arduino LED
Communication: Serial Wired

![](https://i.imgur.com/0lKHVtN.png)

![](https://i.imgur.com/sJ5aLnu.png)


#### Processing Code

```
import processing.serial.*;

Serial myPort;  // The serial port

PFont f;                           // STEP 1 Declare PFont variable

float r = 30;
float turn = 0;
float goldenR = (1 - sqrt(5))/2;
float h = 100;
int value = 1024;
boolean noWater = false;
String plantState = "";
void setup() {
  //fullScreen();
  size(700, 1000);
  background(0);
   
   f = createFont("Arial",16,true); // STEP 2 Create Font
   
   //printArray(Serial.list());
   
  // Open the port you are using at the rate you want:
  myPort = new Serial(this, Serial.list()[0], 9600);
}

void draw() {
  
  // Read String from serial
  if (myPort.available() > 0) {
    String inBuffer = myPort.readStringUntil(13);   
    if (inBuffer != null) {
     
      inBuffer = inBuffer.replace('\n', ' ').trim();
      value = int(inBuffer);
    }
      fill(0);
     rect(80, 80, 500, 150);
     
    textFont(f,16);                  // STEP 3 Specify font to be used
    fill(0,100,0);                         // STEP 4 Specify font color 
    
   if(value>900){
     plantState = "Plant NEEDS WATERING";
     noWater=true;
   }else{
     noWater = false;
     if((600<value)&&(value<900)){
      plantState = "Plant is DRY";
     }else{
      plantState = "Plant is HUMID";
    
    }
   }
    textAlign(CENTER);
    text(plantState,180,100);   // STEP 5 Display Text
  }
  
  turn += goldenR;
  float alphaBlatt = random(100);
  float alphaRand = random(100, 150);

  translate(width/2, height/2-h);

  noFill();
   if(noWater){
     stroke(120,120, 120, 120);
   }else{
     stroke(0, random(255), random(150), random(100));
   }
  
  bezier(0, 0+(height/2)+h, 0, random(0, 300), 0, random(0, 300), random(-300, 300), random(0, 350));

  stroke(0, alphaRand);
   rotate(turn);
  if(noWater){
    fill(120, 120, 120, 0);
  }else{
    
    fill(random(200, 255), 0, random(150, 255), alphaBlatt);
  
  }
 
  ellipse(0, 0, 50, random(100, 300));
  fill(0);
  ellipse(0, 0, 2*r, 2*r);

}

void keyPressed() {
  if (key == 's') {
    myPort.clear();
    myPort.write("1");
  }
}
```

#### Arduino Code

```
int val = 0;
void setup()

{

  Serial.begin(9600);

  pinMode(A0, INPUT);
  pinMode(9, OUTPUT); 

}

void loop()

{

  val = Serial.read();
  Serial.println(val);
  Serial.flush();
  
  int mois = analogRead(A0);
  Serial.println(mois);

  if(val>=1){
    digitalWrite(9, HIGH);  
  }else{
    digitalWrite(9, LOW);  
  }

  delay(2000);

}
```
### A-Frame + NodemCU + WebSockets
[Repository](https://trello.com/b/8zCp8xda/a-frame)

![](https://i.imgur.com/biAQfJh.png)


### APP Inventor + Bluetooth HC08 + Sensor

### APP Inventor

![](https://i.imgur.com/hPvKcsi.png)

![](https://i.imgur.com/wxQhdZY.png)


#### Arduino Code

```
#include <SoftwareSerial.h>
SoftwareSerial BT1(4,2);      // RX, TX recorder que se cruzan

void setup()
   { Serial.begin(9600);
     Serial.println("Enter AT commands:");
     BT1.begin(9600);
     pinMode(A0, INPUT);
    pinMode(9, OUTPUT); 
    
   }

void loop()
   {  int mois = analogRead(A0);
    if (BT1.available())
          Serial.write(BT1.read());
          
           
      if (Serial.available())
         { char c = Serial.read() ;
           Serial.print(c);
           BT1.write(c);
           
         }
         BT1.print(mois);
         delay(1000);
   }
```


### Grasshopper firefly
![](https://static.food4rhino.com/s3fs-public/users-files/andy-payne/app/fireflylogo256x256.jpg)
[Acces to the repo with the files](https://drive.google.com/drive/folders/1EGnp5ZSwtpVfBPNCenQL33bAsdZrzpC0?usp=sharing)


* Firefly experiments [link](http://www.fireflyexperiments.com/)
* Firefly plugin for grasshopper[link](https://www.food4rhino.com/app/firefly)

#### Simple serial writte

<iframe src="https://player.vimeo.com/video/6552563" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/6552563">Using Grasshopper to Control a Pan / Tilt Servo</a> from <a href="https://vimeo.com/user2076220">Andy Payne</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

#### Simple serial read

![](https://thumbs.gfycat.com/GloomyThoroughGoshawk-mobile.jpg)


#### Image processing
![](https://api.ning.com/files/qzeWKgaSDIFX*pja2aTHl*kHzbfoxLtVBS7HUP3P-cWDSEpTSBHcfKiysamfNpj1VWg6jVmsxEyIYSbnukDwDEJgecZQCwQc/6_FireflyComponents_lr.jpg?profile=RESIZE_930x&width=737)



#### Kinect processing

<iframe src="https://player.vimeo.com/video/98232430" width="640" height="356" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/98232430">Grasshopper 3d + Firefly hand tracking</a> from <a href="https://vimeo.com/user5827770">nachetz</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


### Generative Design

Reference: http://www.generative-gestaltung.de/2/
Languange: **p5.js**

![](https://i.imgur.com/IGK6Qzj.png)


### IoT Devices with IFTTT

Reference: http://iot.futurelearningunit.com
Language: **Arduino**
Device Interfaces: **IFTTT**
Data Interfaces: **Google Sheets**
Networking Communication:**WiFI ESP8266**

### Python

Example file: [here](http://fab.academany.org/2019/labs/barcelona/local/assets/python/app.py)
Language: **Python, web application**
Device Interfaces: **Serial**
Data Interfaces: **Pandas, CSV**

![](https://i.imgur.com/dAOHSXa.png)




## Node-Red

User Interfaces: Node-Red
Device Interaces: MQTT

## The super freak option

User interfaces: [blessed-contrib](https://github.com/yaronn/blessed-contrib)
Language: **ascii/ansi art and javascript**

# Livecoding

![](https://i.imgur.com/h4F1TLR.png)


- [TopLap](https://github.com/toplap/awesome-livecoding)

- [Hydra](https://hydra.ojack.xyz/?sketch_id=example_16)
- [Hydra API](https://hydra.ojack.xyz/api/)
- [Hydra Book](https://hydra-book.glitch.me/#/)
- [Hydra Docs](https://hydra.ojack.xyz/docs/#/)

![](https://i.imgur.com/AikIGDN.png)

- [Sonic Pi](https://sonic-pi.net/)



## MDEF Task

!!! warning "Task"
    Experiment with the tools shown in class. And choose the ones that best suit your research project.