# REMIXING MATERIALS CLASS!

Here you are going to find the presentation that was used during the course.


<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQNsK7LEDkkyYwSL3Ri0E_UPHMMzbXR5MRDjQPuG__TfRzc16QZ2OE9qnFp0zN8JjPxZv4GZ-WHlGoq/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQV4xO6P4hacXXh-8k1CrGJGUp4xheJl03P4oUna6ri33NirrxwPcGdCYA2g_J_uLQxD7GBdBIZwtAZ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


## HOW TO USE THE MACHINE

<iframe width="960" height="420" src="https://www.youtube.com/embed/jNF1NYnVeio" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>



### DOWNLOADABLE FILES / CONFIGURATIONS

- [Grasshopper Slicer](https://github.com/fablabbcn/aei-3d-mamba-slicer)
- [BIOMATERIAL-PASTE EXTRUSION KIT](https://github.com/fablabbcn/aei-kit-impresion-pasta)
- [CURA profiles for the 3d printer](https://github.com/fablabbcn/aei-3dslicer-curaprofiles)

#### How to install the CURA PROFILES

In the *CURA profiles for the 3d printer* repository linked above you will find two different files in the *"PastePrinter_machine_extruder_Definitions"* folder.

- fablabbcn_0.def.json  ( this file is the extruder configuration file)
- fablabbcn_pasteprinter.def.json ( this is the machine configuration file)


Once both profiles have been downloaded, place the machine definition profile in the resources/definitions folder, or in definitions, in the user configuration folder where Cura is installed.()

**Windows install**

     - Route example
     - *C:\Program Files\Ultimaker Cura 5.2.1\share\cura\resources*

For extruder definition files, you should place them in resources/extruders, or in extruders, in the user configuration folder where Cura is installed.

**Mac install**

 In cura got to-- HELP-- CONFIGURATION FOLDER

    - paste each file in the respective folder



### References

- [Advanced 3D Printing with Grasshopper](https://www.amazon.com/Advanced-3D-Printing-Grasshopper%C2%AE-Clay/dp/B086Y7CLLC)
- 