# 3D Scanning

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Presentation 3D Scanning Detailed')}}

## 3D Scanning overview

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## 3D Scanning overview')}}

## Scanning resources

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Scanning resources')}}

## Scanning Softwares

{{ get_snippet_rel('docs/content/digifab/3dprintscan.md', '## Scanning Softwares')}}