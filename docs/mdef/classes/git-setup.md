# How to set up your documentation

In this guide you are going to find the basic instructions on how to create your documentation webpage that you will use to document your MDEF journy. To do it this guide will cover different points:

- [The Terminal](#the-terminal): First you need to know basic command line commands to navigate througth files in your computer.
- [Git](#git): What is it? How to use version control.
- [Markdown](#markdown): Let's make web easy.
- [Mkdocs](#mkdocs): How to configure your page.
- [How to start from the template](#how-to-start-from-the-template): it's time to create you page!

## The Terminal

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/DEC_VT100_terminal.jpg/1200px-DEC_VT100_terminal.jpg)

The terminal is a scary thing at the beggining, but you are going to get use to it and, if you do not want, you are not going to us it much. But to get started with your page it's important that you learn how to navigate around your computer using the terminal. Here there is a list of the more basic commands that you are going to use. 

```
pwd: Print Work Directory
```

The command pwd will print the directory where you are working in that terminal, it can be understand as the folder you are in. 

```
ls: List files
```

Using the ls command you will see whats inside your actual directory, all files and folder will be listed. Is very usefull to know where to navigate.

```
cd: Change Diretory
```

To navigate to a different folder you will use the cd command. By default it needs a relative path for the new folder inside the actual one, to go back outside of the actual folder you need to use cd .. to go back one level. If you the full path of the folder you want to navigate to you can use cd /folder_path to enter it. 

```
mkdir: Make new Directory
```

This will create a new folder with the specified name like mkdir newFolderName.

```
touch: Create a new file
```

With the command touch you can create a new file, again specifiny the name as touch newFileName. 

```
clear: Clear the terminal
```

Clear or Ctrl + L you can clear the output of the terminal to clean your screen. 

!!! Note
    Using the terminal there are some shortcut that come handy when working. The first one is to use the tab key to autocomplete the names of the files, and the second one is to use the keyboard Up and Down arrows to navigate to the history of commands. 


## Git

![](https://git-scm.com/images/logos/downloads/Git-Logo-Black.png)

Git is a free and open source distributed version control system. It will allow you to keep track of the files and history of your repository that will contain your webpage. Together with Git, you are going to be using GitHub to host your repository online and serve your page to the internet. 

### Git Local/Remote Structure

As a reference, this is a basic structure of the local/remote structure using git.

![](https://image.slidesharecdn.com/gitbranchstregagycasestudywoogenius-140314152231-phpapp01/95/git-branch-stregagy-case-study-2-638.jpg?cb=1413975847)

### Interactions

Here there are some basic interactions you will normally do with your git repositories.

#### Cloning

Cloning takes a remote repository and literally _clones it_ into a local one:

```
git clone git@gitlab.com;flu/supermegasuperguay.git
```

#### Status

Shows what is the status of your local/remote repository - if there are local or remote changes, or things to be pushed:

```
git status
```

#### Log

Shows the history of the local and remote repositories as a list of commits with more info:

```
git log
```

#### Basic changes workflow

You will most likely be using this workflow for git

##### Pull-add-commit-push

- Download the last copy from the online repository and merge it with your local working directory:

    ```
    git pull
    ```

- Add the new files you added to git
    
    ```
    git add index.html
    ```

    or to review each `sloc`:

    ```
    git add -p
    ```

- Now name your update, so you know what you changed with this push.
    
    ```
    git commit -m "COMMIT MESSAGE"
    ```

!!! danger "About the commit message"
    This is a general point of failure for many many students (and instructors) that do not make a relevant commit message. 

    Write a **meaningful commit message**. This should answer the question: 
    > "If I apply this commit, I will... <commit message>". 
    
    For example: 
    > "uploading final project idea"
    
    This is not OK at all and will not help anyone to trace problems (and they will happen):

    ![](assets/bad-repo.png)

- Upload to the Repository

    ``` 
    git push
    ```

!!! warning "This is 1% of what git does"
    Git is much more than this. Do not hesitate to visit [the docs](https://git-scm.com/doc) to get more info or to follow a tutorial on more advanced features.

## Setting up git

![](https://git-scm.com/images/logos/downloads/Git-Logo-Black.png)

!!! Note
    All information provided in this page comes from [here](https://git-scm.com/)

- **Windows**: [Gitbash](https://gitforwindows.org/) for windows users. This will install git and git bash, a terminal for using the command line in Windows other than the Power Shell.

- **Mac**: [command line](https://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line) and install from [here](https://git-scm.com/download/mac). You can use [`homebrew`](https://brew.sh/) as a package manager. We recommend to install Xcode first (from the app store, for example) and then [`homebrew`](https://brew.sh/). 

- **Linux**: Use the command `sudo apt-get git` in the Linux Terminal.

### Make an account

- Make an account in [Github.com](https://github.com/) using the email you used in the program registration form

!!! danger "Format for the username"

    Make a username as such: `name_surname`

    For example if my name is...

    Andrés López Lee Peters: `andres_lopez`

    Or...

    Wongsathon Choonhavan: `wongsathon_choonhavan`

    Do not use dots in your username as you will have problems afterwords.

### Go local

- In your terminal, add your Git username and set your email

```
git config --global user.name "your_username"
```

- Configure you email address for uploading

```
git config --global user.email "your_email@mail.com"
```

### Generate SSH Keys

- Check if you have an SSH KEY already (If you see a long string starting with ssh-rsa, you can skip the ssh-keygen step):

```
cat ~/.ssh/id_rsa.pub
```

- Generate your SSH key:

```
ssh-keygen -t rsa -C "your_email@mail.com"
```

- Now let´s see your keygen

```
cat ~/.ssh/id_rsa.pub
```

- Copy your key:

**Windows**

```
clip < ~/.ssh/id_rsa.pub
```

**macOS**

```
pbcopy < ~/.ssh/id_rsa.pub
```

**linux**

```
xclip -sel clip < ~/.ssh/id_rsa.pub
```

### Go global

Add the copied key to [github.com](github.com) by following this [guide](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)

### How to start from the template

There are several ways to have a repository:

- In the online git platform, create a repository and then clone it
- In your terminal initialising it from scratch
- Cloning an existing one (most likely)

[Link to template](https://github.com/fablabbcn/mdef-html-template)

#### Online Git platform

If you are using and online web service for git ([Github](https://github.com/), [Gitlab](https://gitlab.com/), [bitbucket](https://bitbucket.org/)...), you can create a new project or repo by simply going to one of these below and then follow the instructions to _clone it_:

- [New github repo](https://github.com/new)
- [New gitlab repo](https://gitlab.com/projects/new)
- and so on...

#### Terminal from scratch

!!! example "Step-by-step"
    - Navigate to the folder where you want to put or create your repo

    - In the terminal, type:

    ```
    git init
    ```

    - If you have a remote already, you can just do:

    ```
    git remote add origin git@github.com:gitusername/repository.git
    ```

    - And then pull the remote:

    ```
    git pull
    ```

#### Cloning an existing one

If you have a template or an online repository you want to reuse, you can navigate with your terminal to the desired destination folder and do:

!!! example "MDEF Students"

    - Navigate to the folder where you want to put or create your repo

        ```
        cd folder-for-your-project
        ```

    - Clone your student repository (ssh)

        ```
        git clone git@github.com:fablabbcn/mdef-html-template.git
        ```

    - Create your own project in Github - [Direct access](https://github.com/new) - Make sure that is **public!!**

    - Tell your local repository to push to the new project in Github:

    ```
    git remote rename origin old-origin
    ```

    ```
    git remote add origin git@github.com:username/your-repo-name.git
    ```

    - Do some edits and then add-commit-push (like this for the first commit):

    ```
    git add FILENAME
    ```

    ```
    git commit -m "My first commit"
    ```

    ```
    git push -u origin --all
    ```

    - For further changes, your workflow should be:

    ```
    git add FILENAME
    ```

    ```
    git commit -m "My other commit"
    ```

    ```    
    git push
    ```

!!! danger "About the commit message"
    This is a general point of failure for many many students (and instructors) that do not make a relevant commit message.

    Write a **meaningful commit message**. This should answer the question:
    > "If I apply this commit, I will... <commit message>".

    For example:
    > "uploading final project idea"

    This is not OK at all and will not help anyone to trace problems (and they will happen):

    ![](../../material/extras/week01/assets/bad-repo.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y5cm-__K3sM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
