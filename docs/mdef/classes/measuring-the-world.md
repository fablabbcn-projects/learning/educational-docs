# Measuring the world

Here you can find the [class documentation](https://hackmd.io/-6tpUjtTSpGx1vILGgJbUg?view). 

## Presentation

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTaQ-zItk9JuS2hvK6yjJT9_VD40yNBHH8aHkljvpnY_p3u7nVO4s28fAj0aktIp8mH827LGgqnhYUC/embed?start=false&loop=false&delayms=60000" frameborder="0" width="100%" height="480px" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>