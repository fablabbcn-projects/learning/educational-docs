# Design tools (2D,3D, parametric design)

## Presentation 2D MODEL

{{ get_snippet_rel('docs/content/design/cad.md', '## Presentation 2D MODEL Inspirational')}}


## Presentation 3D MODEL Inspirational trends

{{ get_snippet_rel('docs/content/design/cad.md', '## Presentation 3D MODEL Inspirational trends')}}


[ACCESS TO THE CAD MIRO BOARD](https://miro.com/app/board/uXjVPrnS_rQ=/?share_link_id=220271332338)

## CAD - Resources

{{ get_snippet_rel('docs/content/design/cad.md', '## CAD - Resources')}}

## GENERATIVE WITH Fusion360

<iframe width="560" height="315" src="https://www.youtube.com/embed/3smr5CEdksc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Parametric grasshopper ultraeasy

[Full guided manual](http://grasshopperprimer.com/en/index.html)

<iframe width="560" height="315" src="https://www.youtube.com/embed/zDDVeDldvaI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Blender with AI Render

<iframe width="560" height="315" src="https://www.youtube.com/embed/g0ZkUyiDkEU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Grasshopper Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Grasshopper Intro')}}

## Fusion360 Intro

{{ get_snippet_rel('docs/content/design/cad.md', '## Fusion360 Intro')}}
