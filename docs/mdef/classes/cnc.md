# CNC Manufacturing

{{ get_snippet_rel('docs/content/digifab/cnc.md', '## CNC MACHININIG PRESENTATION')}}



## CNC Raptor XL-S3100  Equipment Usage Videos

{{ get_snippet_rel('docs/content/digifab/cnc.md', '## Video tutorial CNC Raptor XL-S3100')}}

## CNC RHINOCAM

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT8nKZkfoTXtP4CQZjkJWBhSngq2gyNUHNjkO5nvyNDhBNp7h1_BYN_iIoktfa_sg25g6NNd884PaQY/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


## Video tutorial CNC Raptor XL-S3100

<iframe width="960" height="420" src="https://www.youtube.com/embed/wKqFPtVuM48" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## RHINOCAM VIDEO TUTORIAL

<iframe width="560" height="315" src="https://www.youtube.com/embed/bNNX4G2K0Ts" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you want to use the big CNC RAPTOR XLS milling machine of Pujades 102 you need to install the postprocessor for rhinocam. You can find it [here](https://github.com/EDUARDOCHAMORRO/CNC-StepPostprocessor)

## RHINOCAM EXAMPLE WORKFLOW

```
RhinoCam

1.Select Machine - 3 axis CNC_STEP_BCN


2.Box Stock:

    Stock Geometry: origin in the top left buttom corner
    Corner coordinates: 0, 0, 0
    Dimensions: L, W, H

3.Screw material and then around the area you want to cut

4.Draw points in Rhino where you have located your screws

5.Go to Machining operations - 2 axis
    Roughing
    Pocketing (emptying part of the material)
    Engraving (on cut) (choose it for making the "screws" spot and then select the points where you want to make the mark

6.Create/Select Tool

7.Fablab just have ball and flat mills.

    For the screws choose flat (next parameters should be according to the one you are going to use)

    Name it (this is an example of mill bit values, you have to choose your own)
    Holder Dia: 30
    Holder Len: 45
    Shank Dia: 6
    Tool Len: 105
    Shoulder Len: 60
    Flute Len: 45
    Tool Dia: 6
    Flute: 1
    Spindle Parameters: Speed: 18000 RPM, Direction: CW, Cut (calculate with cheapload equation): 4500, Retract: same as Cut, Departure: same as Cut. Plunge ("half of cut"): 2000, Approach: same as plunge and Engage: same as plunge.
        Clearance Plane: Clearance Plane Definition: Part Max Z + Dist: 20 (Never Automatic)
        Cut Parameters: we only care about "Cut Depth Control": 3mm
        Entry/Exit:
    Sorting: Minimum Distance Sort


    For pocketing:

    Feeds and Speeds: 18000 RPM
    Clearance Plane: Clearance Plane Definition: Part Max Z + Dist: 20 (Never Automatic)
    Global Parameters: Climb
    Cut Patter: Offset
    Do everything else!!!!!


For profiling/outside: use along path! when using profiling the machine doesn't recognize other pieces. So we'd like to use along path so it can slide and approach directly to the selected piece. Remember to add taps (bridges).


```

**RhinoCAM MILL Quick Start**

<iframe width="560" height="315" src="https://www.youtube.com/embed/LQdsXYhWWDk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## References and resources

{{ get_snippet_rel('docs/content/digifab/cnc.md', '## References and resources')}}