
## Grading Method

![](assets/qvG27nL.jpg)

-  **Only the documentation into their webpages will be taken in account for evaluation**
- The weekly standards and grading will be presented during the weekly master classes.
- Prototyping process understanding ,workflows and evolving best practices will seriously be taken in account.

[EVALUATION SPREADSHEET](https://docs.google.com/spreadsheets/d/1z5xFA6uRb3-6Brk7yHgMaaY0ozvM7ztBBrp6iHCQKNY/edit?usp=sharing)

[ATTENDANCE SPREADSHEET](https://docs.google.com/spreadsheets/d/1AZUtMXwiV0aSbmkwOag-3QhPChNm-UXFv3vwkmMEkK4/edit?usp=sharing)

#### Academic level - 40%

*Level of the project, artefacts or activities (quality and complexity of the solutions/ reflections/prototype/code/artifacts)*

- Initial idea / Concept of the Project ( aligned to research areas)
- Propose (What is supposed to do or not to do)
- Shown how your team planned and executed the project. (Explain personal contributions)
- Integrated Design (How you designed it - relation between elements)
- Honest Design (use of technology in a meaningful way, in relation to the topics)
- Explore design boundaries (based on your expertise)

#### Open Content- 35%

*Level of clarity and detail of the documentation material (photos, video, text, etc)*

- Add names and links to your individual pages
- Design process (How did you iterate)
- How did you fabricate it (fabrication processes)
- System diagram (illustration explaining function, parts, and relations)
- Design & Fabrication files (open source or open format)
- BOM (Build of Materials)
- Reflect about future development opportunity
- Described problems and how the team solved them
- Photograph’s of the end artefacts


#### Involvement - 20% (Selfevaluation)

*Attitude, Motivation level, proactive behaviours*

- Attendance to classes
- Proactive behaviours to find answers during the program
- Help others student’s projects
- Participation in feedbacks sessions

#### Explosion - 5 % 

Bonus if your artefact doesn´t explode and actually works as expected
