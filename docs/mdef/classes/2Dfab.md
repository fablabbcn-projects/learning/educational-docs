# 2D Fabrication

## Local Class

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Local Class')}}

### Miro board

[ACCESS TO THE MIRO BOARD](https://miro.com/app/board/uXjVN4mXNAI=/?share_link_id=83996765967)

## Computer-control Laser/Vinyl Equipment Usage Videos

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Computer-control Laser/Vinyl Equipment Usage Videos')}}

## Materials

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Materials')}}

## Trotec Speedy 400

{{ get_snippet_rel('docs/content/digifab/laser.md', '### Trotec Speedy 400')}}

## Vinyl Cutter

{{ get_snippet_rel('docs/content/digifab/laser.md', '### Vinyl Cutter')}}

## Softwares:

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Softwares:')}}

## Inspirational Ideas

{{ get_snippet_rel('docs/content/digifab/laser.md', '## Inspirational Ideas')}}
