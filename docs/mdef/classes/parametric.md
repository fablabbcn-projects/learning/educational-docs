# Parametric design

## Presentation

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTkOL4EnCyH5r8T7lAlNkRGrzGW0o7g-OFgmjrvU0G42mwIbhQQxwYYlajH16vOaCmzDQE05awHAGB_/embed?start=false&loop=false&delayms=15000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Class links
- [Miro Board](https://miro.com/app/board/uXjVN4mXNAI=/)
- [Grasshopper File](assets/ParametricLEGO-Grasshopper.gh)
- [Fusion360 File](assets/LEGOParametric-Fusion360.f3d)
- [Blender File](assets/BlenderParametricHelloWorld.blend)
- [Onsape Project](https://cad.onshape.com/documents/1efe30a33986b30e50c212d9/w/baccb25609c16b2c914b94ad/e/b328aaceefbd209e55024dbc?renderMode=0&uiState=65a8bd5d0a6e64533f821168)
- [Tinkercad Script](https://www.tinkercad.com/codeblocks/hs0MCPg7dvy)