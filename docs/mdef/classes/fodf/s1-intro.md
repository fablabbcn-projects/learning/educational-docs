# Session 01 - Intro

The Fundamentals of Digital Fabrication seminar is the first contact for new MDEF students with the technologies, tools, and fabrication processes available in the lab. Over the course of nine weeks, we will introduce the main 3D design software, digital fabrication machines, basic electronics concepts, and some design and production techniques that will undoubtedly be extremely useful for developing more elaborate and meaningful projects throughout the course.

## Intro to Fundamentals

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSQCMUZv4xC8uFBpTDxd0zBKs1sr9NI0fLdwx1zLwMMnD0KK9kgq8duVMXKEfzER9cd9FojY1vtkodM/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## People from MDEF

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSsmVBg5Uvuv8j2zjM7oUAQbKf2dLzp7tIssjR6lXZ2Wt0KzMR1-WgdmkXLZA8ZRLuX4do4pTR1N94E/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


## Lab Safety Measures & Rules

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vS0JCTjEaYq5zd7_piA9qbFurCX2HRGPjapWSzMXQc3vGqOGF9T4OcdpIsVQaZvyuuEtPJBr3qDBhwT/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


- [Super Lab Protocol](https://drive.google.com/file/d/15YUVZVWzKEkmhfqUpq6xkAOmEY9HWvFu/view?usp=drive_link)

