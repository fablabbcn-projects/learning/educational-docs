# Session 05 - Computer Numeric Control Milling 


## CNC

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSzSIA39xqFE6RZYb6JXt_bWjAzdYwRvrl_s9Og_ZujXJjGljWZ92aQmG9DIGMGnuenOoBOGkFTpIs5/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Resoruces

- [Raptor X-SL 3200/S20](https://docs.google.com/document/d/1MK71S9LVpypN15hveJr-5Jp3YeXtADaI8E4gu8TLui0/edit?usp=sharing)
- [Shopbot ATC PRS Alpha 3](https://docs.google.com/document/d/1WdcpQ6AInr0L8AbOVj3Hw4hejVq0XjTusNhqY6aSR4Y/edit?usp=sharing)
- [Tex S-1215](https://docs.google.com/document/d/1Ibcmk7MlnTLj0Bm3pdQbWo9_573JYZo7mGZEOhY01lE/edit?usp=sharing)
- [P59 millbits Tools Library](https://drive.google.com/file/d/1ZHkMwty5jNsB4Y5q4kyosF8aEZzJIvVn/view?usp=drive_link)
- [Post processors](https://drive.google.com/file/d/1WXn_t9WwI60p9JrlWFRXxq2aV5vVuKW2/view?usp=drive_link)
