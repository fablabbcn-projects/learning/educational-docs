# Tasks for Module 5

**In groups of two (pairs)** you should do:

- Create a visuals tha visualize your trained model. Outputs could be:

    1- Audio
    2- Text
    3- Video

![Alt text](../mdef/classes/assets/task5.png)



!!! warning "Submisson"
    Write a post in your personal mdef website, out of your weekly task experience including:

    1. Reflection: The four F's of active reviewing
        - Facts: An objective account of what happened
        - Feelings: The emotional reactions to the situation
        - Findings: The concrete learning that you can take away from the situation
        - Future: Structuring your learning such that you can use it in the future

    2. Pictures

    3. References

    4. Fabrication files

    5. Add the name of your teammate

