# Tasks for Module 2

**In groups of two or in pairs** you can choose from the following options:

1. Design and 3D print a mould to grow your own materials.
    - Mycelium
    - Kombucha

    ![Alt text](../mdef/classes/assets/task2_01.png)

2. Design and Bio3d print a small object with a paste printing machine.
    - Clay
    - Biomaterials

    ![Alt text](../mdef/classes/assets/biopasteprint.png)


!!! warning "Submisson"
    Write a post in your personal mdef website, out of your weekly task experience including:

    1. Reflection: The four F's of active reviewing
        - Facts: An objective account of what happened
        - Feelings: The emotional reactions to the situation
        - Findings: The concrete learning that you can take away from the situation
        - Future: Structuring your learning such that you can use it in the future

    2. Pictures

    3. References

    4. Fabrication files

    5. Add the name of your teammate

