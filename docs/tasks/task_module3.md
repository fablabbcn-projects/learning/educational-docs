# Tasks for Module 3

**In groups of two (pairs)** you have to do:

1- Fabricate (in pairs) **ONE** piece of furniture for your classroom

    *each student has a piece of 1200x1200x15mm plywood plan*
    As pairs - you have a full wood plank of 2440x1220x15mm


!!! warning "Submisson"
    Write a post in your personal mdef website, out of your weekly task experience including:

    1. Reflection: The four F's of active reviewing
        - Facts: An objective account of what happened
        - Feelings: The emotional reactions to the situation
        - Findings: The concrete learning that you can take away from the situation
        - Future: Structuring your learning such that you can use it in the future

    2. Pictures

    3. References

    4. Fabrication files

    5. Add the name of your teammate

