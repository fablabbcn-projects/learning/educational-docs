# Tasks for Module 1

**In groups of two or in pairs** you can choose from the following options:

1. Design and fabricate a small prototype with lasercut mold for bioplastics
    - [example](https://perfectly4med.co.uk/2020/08/07/laser-cut-pewter-moulds/) 
    - [example-2](https://softroboticstoolkit.com/soft-barrier/fabrication/casting)

    ![](https://projects.iq.harvard.edu/files/styles/os_files_xxlarge/public/srtdesigncomp/files/pouring_.jpg?m=1516134881&itok=qybB86pu)

2. Design and fabricate a pressfit prototype of your choice:
    [example](https://shiraczerninski.wixsite.com/thepastaproject)

   ![](https://static.wixstatic.com/media/ca3c2a_85b0f408ef354637b4fbea13922f8759~mv2.jpg/v1/crop/x_0,y_35,w_2250,h_2637/fill/w_648,h_746,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/IMG_4823_edited.jpg)


!!! warning "Submisson"
    Write a post in your personal mdef website, out of your weekly task experience including:

    1. Reflection: The four F's of active reviewing
        - Facts: An objective account of what happened
        - Feelings: The emotional reactions to the situation
        - Findings: The concrete learning that you can take away from the situation
        - Future: Structuring your learning such that you can use it in the future

    2. Pictures

    3. References

    4. Fabrication files

    5. Add the name of your teammate

## References

- [MAKING BIOMATERIAL USING A MOULD THAT CUT OUT USING LASER CUTTER](https://www.scopesdf.org/scopesdf_lesson/making-biomaterial-using-a-mould-that-cut-out-using-laser-cutter/)

 