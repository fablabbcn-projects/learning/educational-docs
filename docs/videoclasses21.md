This section is a compilation of the video classes given by the Fab Lab Barcelona team, for the Fabacademy / MDEF courses


<div class="iframe-container">

  <div class="iframe-item">
    <h2 class="video">Processing & P5</h2>
    <h3 class="class">[Interface and application programming]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/EZ5L9g-5mPg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>



</div>


<div class="iframe-container">

  <div class="iframe-item">
    <h2 class="video">A-Frame + App Inventor + NodeRed</h2>
    <h3 class="class">[Interface and application programming]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/2P-hK0hhNS4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>


  <div class="iframe-item">
      <h2 class="video">Grasshopper, Firefly II</h2>
      <h3 class="clubs">[MAKE CLUB]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/5H78MqUz_d4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>

</div>



<div class="iframe-container">

  <div class="iframe-item">
    <h2 class="video">Overview, I2C + Wifi + MQTT</h2>
    <h3 class="class">[Networking and communications]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/f4qqqbXpzp8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>


  <div class="iframe-item">
      <h2 class="video">Advanced Over The Air Class</h2>
      <h3 class="class">[Networking and communications]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/HAbDIvAyoNY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>

</div>




<div class="iframe-container">

  <div class="iframe-item">
    <h2 class="video">The resurrected website IV</h2>
    <h3 class="clubs">[CODE CLUB]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/O7tUkvcnu6g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>


  <div class="iframe-item">
      <h2 class="video">Advanced outputs setups</h2>
      <h3 class="class">[Output devices]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/KZal4s3uIzQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>

</div>


<div class="iframe-container">

  <div class="iframe-item">
      <h2 class="video">Overview & General concepts</h2>
      <h3 class="class">[Output devices]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/Ya3UNzbb09g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>


    <div class="iframe-item">

      <h2 class="video">Alternative mold making</h2>
      <h3 class="class">[Molding and Casting]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/hnyQecdz9VI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>




<div class="iframe-container">

    <div class="iframe-item">

      <h2 class="video">Overview & General concepts</h2>
      <h3 class="class">[Molding and Casting]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/lfd4OOILrk0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

      <h2 class="video">Grasshopper - Firefly I</h2>
      <h3 class="clubs">[MAKE CLUB]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/Je6S8UzVm5w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>


</div>








<div class="iframe-container">

    <div class="iframe-item">

      <h2 class="video">Data analysis and advanced sensor setups</h2>
      <h3 class="class">[Input devices]</h3>

    <iframe height="200" src="https://www.youtube.com/embed/rsSljD2N_1I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Input devices basics</h2>
        <h3 class="class">[Input devices]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/149sTP1kqh4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>


</div>


<div class="iframe-container">

  <div class="iframe-item">

    <h2 class="video">Unpacking Machines ( The CNC by the sea & SPML)</h2>
    <h3 class="class">[Mechanical & Machine design]</h3>

  <iframe height="200" src="https://www.youtube.com/embed/0OumJsD99cU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  </div>

    <div class="iframe-item">

        <h2 class="video">Mechatronics & Gcode</h2>
        <h3 class="class">[Mechanical & Machine design]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/8os-90MHxG4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>



<div class="iframe-container">


    <div class="iframe-item">

        <h2 class="video">Overview</h2>
        <h3 class="class">[Mechanical & Machine design]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/C2T7dJ0ZThg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Final Project review II </h2>
        <h3 class="class">[Fab Academy]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/kyMAwkEdJpE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>


<div class="iframe-container">


    <div class="iframe-item">

        <h2 class="video">Bacteria Dyes - Shemakes</h2>
          <h3 class="clubs">[MATERIAL CLUB]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/u2Tb5zJgRuE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Advanced programming</h2>
        <h3 class="class">[Embedded Programming]</h3>

      <iframe height="200" src="https://www.youtube.com/embed/RT02lEI7uYs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>


<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Intro to embedded programming</h2>
        <h3 class="class">[Embedded Programming]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/-B_gEZSYuC4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">How To Make A Computer, Digital + Analog</h2>
        <h3 class="class">[Embedded Programming]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/XTEiAhcHWSY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>





<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">How to debug your cut</h2>
        <h3 class="class">[Computer-Controlled Machining]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/GZZX0mQ4M0A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Overview & General Concepts</h2>
        <h3 class="class">[Computer-Controlled Machining]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/PUkr1pG_I6Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>



<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">The resurrected website I</h2>
        <h3 class="clubs">[CODE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/CVVhfbrHdIc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">KiCad Introduction</h2>
        <h3 class="class">[Electronics Design]</h3>

        <iframe height="200" height="315" src="https://www.youtube.com/embed/_o0-YN68n-U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>



<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Electronics components</h2>
        <h3 class="class">[Electronics Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/n0fFCHedCdA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>



<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">How to 3D print Stereolithography (SLA)</h2>
        <h3 class="class">[3D scanning and printing]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/98rXP5l8Qvo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Scanning MDX20</h2>
        <h3 class="class">[3D scanning and printing]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/9N-pWsPbaRg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>


<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Photogrammetry</h2>
        <h3 class="class">[3D scanning and printing]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/m63bd2vcX7k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Overview & General Concepts</h2>
        <h3 class="class">[3D scanning and printing]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/WZsbRJrs_LA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>


<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Understanding electronics</h2>
        <h3 class="class">[Electronics Production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/6jFTRnAvS94" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Soldering and debugging</h2>
        <h3 class="class">[Electronics Production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/Hg1qnJUibvo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Milling PCB</h2>
        <h3 class="class">[Electronic Production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/R0-t8p6OzeY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Basics Electronics</h2>
        <h3 class="class">[Electronic production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/YHKmyBvWcLI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Precious plastics</h2>
        <h3 class="clubs">[MATERIAL CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/2R-0_Bw40Y0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">How to create a Repository</h2>
        <h3 class="mdef">[Micro Challenge]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/0shUMzRCD-Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

</div>

<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Openscad Introduction</h2>
        <h3 class="class">[Computer-Control Cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/4I0zzdaJy1M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">Parametric design in Fusion 360</h2>
        <h3 class="class">[Computer-Control Cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/cAVAKePMP5c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>

<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Grasshopper, Parametric Press fit</h2>
        <h3 class="class">[Computer-Control Cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/jLiBliV4V2c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

    <div class="iframe-item">

        <h2 class="video">How to laser cut as a boss</h2>
        <h3 class="class">[Computer-Control Cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/2RDnWNbk95k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>

<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Design Tips for CCC</h2>
        <h3 class="class">[Computer-Control Cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/vDAfKZSLHYY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">CCC Overview & key Takeaways</h2>
        <h3 class="class">[Computer-Control Cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/gO5OQnBkU4s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>

<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Robotic arms I</h2>
        <h3 class="clubs">[MAKE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/2IAPHnAhvPc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Fusion 360</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/WBXeFk7IiT0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>

<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Rhinoceros</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/vcnHKkFD_Vo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Blender Introduction</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/QpUq0n9Z0JM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">2D/3D Design Concepts & Softwares</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/X4VSaTvKsPU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Fab Lab visit. Hand & power tools</h2>
        <h3 class="clubs">[MATERIAL CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/ssdQyD5zETE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Static Side Generators (SSGs)</h2>
        <h3 class="class">[Project management]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/IOLT-y2uMi8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <a href="material/extras/videoclasses/GMT20210128-084943_FABACADEMY.txt">Chat text</a>
    </div>

    <div class="iframe-item">

        <h2 class="video">Git + Web + Documentation</h2>
        <h3 class="class">[Project management]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/Fie9YdVgCeE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <a href="material/extras/videoclasses/GMT20210127-084239_-FABACADEM.txt">Chat text</a>
    </div>
</div>

<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Raspberry Pi Fundamentals</h2>
        <h3 class="clubs">[CODE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/t5OPHJQdCAM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">3D printing Paste</h2>
        <h3 class="clubs">[MAKE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/2hyebZufqcs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>


<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Python, web scrapping</h2>
        <h3 class="clubs">[CODE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/ZSDtmM5hZz4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <a href="material/extras/videoclasses/GMT20201109-105859_FABACADEMY.txt">Chat text</a>
    </div>

    <div class="iframe-item">

        <h2 class="video">Grasshopper II</h2>
        <h3 class="clubs">[MAKE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/w9-dETr2j8U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <a href="material/extras/videoclasses/GMT20201102-105932_FABACADEMY.txt">Chat text</a>

    </div>

</div>

<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Python</h2>
        <h3 class="clubs">[CODE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/YwmgyUXM_5g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <a href="material/extras/videoclasses/GMT20201026-105916_FABACADEMY.txt">Chat text</a>
    </div>

    <div class="iframe-item">

        <h2 class="video">Grasshopper Intro</h2>
        <h3 class="clubs">[MAKE CLUB]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/PhXpq-RB-yA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <a href="material/extras/videoclasses/GMT20201019-095719_FABACADEMY.txt">Chat text</a>
    </div>


</div>
