
# FabLab Barcelona Remote Students Schedule
Welcome: you will find information about the FabAcademy program, for Fab Lab Barcelona Remote students

All the information of the course and content is also provided in the main local content page, where all the classes are posted. [LINK TO THE PAGE](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/)


#### Class Types*


  - **Local Classes**: This classes are given every Wednesday and Thursday from 10 Am to 13.30 Pm (CET time)

  - **Global review + Class**: This classes are given by Neil Gershenfeld every Wednesday at 9 AM Boston Time (15pm CET)

  - **Remote Student Troubleshooting** :Every friday all the remote labs meet with one of the instructors to troubleshoot the weekly assignment, and prepare next week content and material.

  - **Expert Support** : timeframe open to book appointments on specific subject or expertises. To be request formerly by email.

![](../../assets/remoteschedule.png)

#### Weekly Schedule

  **Monday-Morning**: Students can participate in this extra session every Monday from 12pm to 14pm and book an appointment for especific expertise support from 10 am to 12pm.
  **Monday-Afternoon**: Students can participate in optional Recitations, complementing the week’s content. Broadcasted at 15:00 am – 18:00 pm CET ( Barcelona time)

  **Tuesday**: lab day(students, individual work day.)

  **Wednesday - Morning** : Students participate in *Local Lectures* broadcasted from Barcelona  at 10:00 am – 13:30 pm CET (Barcelona time); these classes are mandatory.
  **Wednesday - Afternoon** : Students participate in Global Lectures broadcasted from MIT  at 15:00 am – 18:00 pm CET ( Barcelona time); these classes are mandatory.

  **Thursday - Morning** : Students participate in *Extended Lectures* broadcasted from Barcelona  at 10:00 am – 13:30 pm CET (Barcelona time); these clases are a more in deep review of each weekly task.

  **Friday** :  All remoter students participate in *Remote StudentTroubleshooting*at 10:00 am – 13:30 pm CET (Barcelona time)

    *This schedule may change subject to COVID-19 restrictions or other force majeure circumstances


    *The lectures are recorded and available to students throughout the semester.*


Local Instructors must cover a minimum of 16 weekly hours on their duties during the Fab Academy program (January to June). In general, Fab Lab Barcelona Instructors organize themselves to give support throughout the whole week; but this does not mean they have to be there 24/7. Plan your work around the hours they are around, and if they are not around, rely on your peers.
