# Weekly Agenda and Class links

#### Class Types*

- **Local Classes**: This classes are given every Wednesday and Thursday from 10 Am to 13.30 Pm (CET time)(mandatory)

- **Global   Class**: This classes are given by Neil Gershenfeld every Wednesday at (15.30pm CET)(mandatory)

- **LAB LIFE**: Student can come to the fabacademy room and ask for support to instructors.

- **Regional review** :This openworkshop/ timeframe is to review together with other labs and students of Spain your weekly work so you have oportunity to present weekly and have feedback on your evaluation status (strongly recommended)

- **Recitations** : Open talks given by some experts of the network on the weekly topic of the assignment.

![](../../assets/fabacademy.jpg)

### Monday -  Recitations

**Every Monday  Recitation from 15h to 16.30h hours**
meeting ID: 894 1627 8965, password: 1234
can connect via browser: http://fab.academany.org/2021/video.html
or:  [https://zoom.us/j/89416278965](https://zoom.us/j/89416278965)


### Tuesday - Regional Reviews

**Every Tuesday from 13h to 14h hours**
- Zoom Link : [https://us02web.zoom.us/j/87451768492?pwd=bDFwb3h1YUNjdEg1dW9zQWljT21UQT09]( https://us02web.zoom.us/j/87451768492?pwd=bDFwb3h1YUNjdEg1dW9zQWljT21UQT09)
- Pass: 4321
Metting ID: 874 5176 8492

https://us02web.zoom.us/j/87451768492?pwd=bDFwb3h1YUNjdEg1dW9zQWljT21UQT09
Shared document:[https://docs.google.com/document/d/1u-Ff8A2TYpFUQ9ChMLqnfc3fuIZQf25Gv9EQ6G0fuQo/edit](https://docs.google.com/document/d/1u-Ff8A2TYpFUQ9ChMLqnfc3fuIZQf25Gv9EQ6G0fuQo/edit)


### Wednesday - Main Classes day

**Every Wednesday  Local CLASS from 10h to 13.30h hours**
- FABACADEMY_Wednesday [https://iaac.zoom.us/j/93987967422?pwd=Ty9oUnZBWTVza0Z1Y0RGSWl5cVhDdz09](https://iaac.zoom.us/j/93987967422?pwd=Ty9oUnZBWTVza0Z1Y0RGSWl5cVhDdz09)
- Meeting ID: 939 8796 7422
- Passcode: Wednesday

**Every Wednesday  GLOBAL CLASS & Review from 15h to 18h hours**
- Zoom Link : [https://zoom.us/j/89416278965](https://zoom.us/j/89416278965)
- Meeting ID: 894 1627 8965
- password: 1234


### Thursday -  Extra Classes day

**Every Thursday Local CLASS from 10h to 13.00h hours**
- Zoom Link : [https://iaac.zoom.us/j/602672552?pwd=Ym1MVExKemwrNlY1VEZNUStZN2ZmQT09](https://iaac.zoom.us/j/602672552?pwd=Ym1MVExKemwrNlY1VEZNUStZN2ZmQT09)
- Meeting ID: 602 672 552
- Passcode: Thursday
