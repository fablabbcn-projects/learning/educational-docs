# Weekly tasks

> The weekly tasks and feedback will be updated and presented weekly, during the local class

### Feedback

![](assets/weeklytasks-c790e2ce.png)

- [Click here to go to the spreadsheet](https://docs.google.com/spreadsheets/d/1enkh5j4DziBYb7unZ64a3gMM0T5-N5QJoVuvbTJzPFo/edit#gid=0)

----------------------

### WEEK 1 - Principles and practices, project management

- Update your webpage with: A new section for FabAcademy, a post/page for each topic and a post/page for each Challenge
- Send a profile picture from you or anything represents yourself
- Browse through the fabacademy web pages of last year MDEF students, select 3 examples of documentation (from weekly tasks) that you find most interesting. Explain why and try to identify the key points.

!!! Note "BONUS"
    Design and create a personal template structure to document weekly assignments. (e.g., reflection, images, references and learning outcomes)



### WEEK 2 - Computer aided design

- Design and create a 3d model with some relation to each person's research project. The result must be uploaded to the website in a rendered format.

!!! Info "INFO"
    Setting up context over the render of the 3D model using some 2D design software, the accounted context could be a poster that combines the render as collage or other 2D or 3D tools vectorial or raster compositions as you wish.
    There should be some vectorial and raster drawings on the render.
    The modeling tools of use are completely open to your skills but we encourage you test softwares that you never used.


!!! Note "BONUS"
    Design and create an animated gif that represents the idea of your project.  (Resolution 640x250 72ppi)
    ![](assets/weeklytasks-9792ea78.gif)


### WEEK 3 - Computer-controlled cutting

- Create a small object using parametric design tools. The object should be assembled in a press-fit way (no use of glue, screws etc).
- Complete the [Machine test](https://docs.google.com/forms/d/e/1FAIpQLScgRrkBb5JOtW_vhQUCHzT6of2vkIXgqccfY8HtWmpQAJoiZA/viewform) with the **100%** of the score

!!! Bonus "BONUS"
    - Make something small using the vinyl cutter (sticker,origami, etc)

!!! Warning "MATERIALS"
    - One MDF 3mm panel of 1000x600mm
    - One corrugated cardboard panel(white or black, based on availability) 4mm thick of 500x600mm ( it comes at 1000x600mm so you can use half sheet)
    - Vinyl cutter material in black/white/cyan/magenta ( to be used wisely, we have a 10meters roll of each for the whole class.


### WEEK 4 - Electronic Production [CHALLENGE WEEK]

#### Challenge
- **Individual post:** Write a post out of your learning experiences, explain how is linked to your research project and also you contribution to the challenge.
- **Project repository:** Complete the [Microchallenge I](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/challenge/c_1/)

#### Task
- Finish (by pairs) your LED BADGE soldering the smd components and test it(make the led light up with the power supply):
    - [Traces](assets/weeklytasks-246e7bb6.png)
    - [Interior](assets/weeklytasks-2f764ba6.png)
    - [Components](assets/weeklytasks-8461d4e3.png)

![](assets/weeklytasks-6c65500a.jpg)

!!! Warning "MATERIALS"
    - [Tutorial Reference link](https://fablabbrighton.github.io/digital-fabrication-module/guides/guide-soldering-practice-board.html)

### WEEK 5 - 3D Printing & Scanning

- 3D print an object **you have modelled**(not something you have downloaded) that could only be made with Additive manufacturing techniques
- Your 3d printed object **cannot be bigger than  70x70x70mm or take more than 3 hours**

![](assets/printing.jpg)

!!! Bonus "BONUS"
    - 3D scan something
    - 3D print something with the paste printer

### WEEK 6 - Electronic design

- Design a PCB board **also called breakout-board** for your ESP32 HUZZAH 32 microcontrollers ( or other board that you want to use rapberri pi ,etc) that allows you to connect an input (sensor) or an output (actuator) to your commercial board without cables.
- Include the design files in your webpage and explain well the process on how you did it.

![](assets/huzzah.jpg)

!!! Bonus "BONUS"
    - Fabricate the board you designed

### Week 7 - Computer Controlled Machining -CNC

- Design something big IN PAIRS! . You can use up to a maximun of half full board ( that means 1200x1200mm) of the available material per student. 15 mm thick pine plywood boards.
- **You will cut your design with your teammate, so you will cut the whole panel at once.**

![](assets/cnc.jpg)

!!! Bonus "BONUS"
    - no screws, no glue, only joinery,zipties or colourfull rope.


### WEEK 8 - Embedded Programming

- Use your programmer ([HUZZAH32, ESP32 Feather](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather)) to program a circuit with a "led and a bottom" to do something
- **The circuit board, could be done as a shield, breadboard or vinyl cutter**

!!! Bonus "BONUS"
    - Try other programming languages and development environments

### WEEK 9 - Molding and Casting [CHALLENGE WEEK]

#### Challenge
- **Individual post:** Write a post out of your learning experiences, explain how is linked to your research project and also you contribution to the challenge.
- **Project repository:** Complete the [Microchallenge II](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/challenge/c_1/)

#### Task
- Make a mold (can be 1 or two sides) HARD-SOFT-HARD . 3DPRINT- CAST SILICONE- CAST SOMETHING INSIDE
- You can use biomaterials to substitute the silicone or the final cast.

![](https://fabacademy.org/2021/labs/zoi/students/jeffery-naranjo/assets/images/w12/molding.jpg)

!!! Bonus "BONUS"
      - Try to mill mold in wax, you can use half wax block to mill. The dimensions of the wax piece you can use a block of 70x90x36´5mm.

!!! Warring "MATERIALS"
      - **MAXIMUM SILICONE EACH STUDENT CAN USE IS 150GR (that more or less is a 70x70x70mm)**


### WEEK 10-Output devices & 13-Input Devices

- **You remember when you had to DESIGN A PCB for week 6? "design a pcb for an input or an output"** now it is time to fabricate it ( finish the design - mill it - solder it)
- Make the PCB you designed real and make it work ( you have from these week until INPUT WEEK- included)
- **This task is a single task between INPUTS AND OUTPUTS WEEK**

![](http://eduardochamorro.github.io/fablabseoulacademy/images/pcbdesign/pic01.jpg)

!!! Info "INFO"
      - Use LAB LIFE days to get your *PCB designs doublechecked.*

### WEEK 11 - Machine design

Nothing this is machine week you already did *The Almost useless machines*

### WEEK 12 - Easter Week

This is easter break enjoy your holidays

### WEEK 14 - Networking and Communications

- Send a message between two microcontrollers (boards/ esp32 feathers or boards you made). The task could be done by pairs
- Communication can be done by cable or wireless

### WEEK 15   Interface & application Programming

- Code/program an interface with the tools provided during the  ( processing,P5JS, MitAppInventor,Aframe,NodeRed) or other alternatives software  that interfaces a user with an input and/or output device that you want to use.
- Interpret and implement design and programming protocols to create a Graphic User Interface (GUI).

### WEEK 16 - Wildcard Week

- Design and produce something with a digital fabrication process (incorporating computer-aided design and manufacturing) not covered in another tasks

### WEEK17 - Application and implications

- **Some of the questions you should reflect on it and answer in your website:**

- What will it do?
- Who’s done what beforehand?
- What will you design?
- What materials and components will be used?
- Where will it come from?
- How much will they cost?
- What parts and systems will be made?
- What processes will be used?
- What questions need to be answered?
- How will it be evaluated?

!!! Info "INFO"
      The main objective of this WEEK is to begin focusing on your final projects and interventions, defining the scope and developing a project plan for the rest of the course


### WEEK 18 - Invention, intelectual property and income

- Created a dissemination plan for your final project
- Outlined future possibilities and described how to make them probabilities
- Choose a licence (open/closed) of your choice for your final mdef intervention
