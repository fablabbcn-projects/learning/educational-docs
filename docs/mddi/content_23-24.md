# Content 23-24

This is where you can find all the presentations we used during the course.
It's still a WIP site, well keep you updated.

## Digital Tools I

**Digital tools I - Intro**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTKmqnwT2fAdwMBZ7GzcH_T-X3RzLgwTid8KOtbIqrTrldP3bTxAdsXBAAnqzNSjgrVgyNLhXbAEpsc/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W2 - Digitalizing realities - Computed aided modeling**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT8P7OkmszepMGUgghwAtO6RQqEHLmIkq5y9-JalVJh2PMam4_QfL9HBh6oHL6c8pbQI-9bI6sJnxdw/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W3 - Design for sustainability-Digital trends in advanced modelling**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTTWRicI2cS-R81tod6PC6Oe52iatv0m5D45QGVUmncktItXwpifOnLT17TvF3SHW4sUWxRdGz8jcoo/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W4 - Digital Sensing - Digitalizing the world**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQrS8hYuZrdwR5q-v_2AQ3fcEIvu7FCEJODO4daf74IycK64NSW6JoDvYx1A86YqsBH8L8TgNXt6fZN/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W5 - Sharing bits - Collective design workflows**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTz28FteuW4f-VagiRP7iINslUz63m2HnqQeLPsjNA6LikV0UAtuLy66S_GZY61nlt_bvsa1avPwdXk/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W6 - Interacting the world**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSCSJ-MFV4i1MXGqtIcJuF7Rodp-6b8R0NSjugLd9h_UJ4Texx53BLef3kwrKP53_ZK-NQhPlKXfmnl/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W7 - The world on data - Visualizing data**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTmCKIyQRMnfgwTwEFM6zyvloivD74UpMgDh1QfRBCIMQJzobiQpGlZ7ze95rV-9HThdjUj1Y8b6sh1/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Miro Board link

- (Miro Board Digital Tools I)[https://miro.com/app/board/uXjVNfBWCt8=/?share_link_id=831734935493]


## Digital Tools II

**Digital Tools - W8 - Bidimensional Fabrication**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR-JrtFkuTG6Pk2-XqEIKLtnX75NL7nZ7hXc-MpTOdipigmR-y-sLwQ8wnYDQtGvRqZqhwJgQtk17zB/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W10 - Additive manufacturing**



### Miro Board link
- (Miro Board Digital Tools II)[https://miro.com/app/board/uXjVN85OGzE=/?share_link_id=775155812857]
