# MDDI

The Master in Design for Distributed Innovation (MDDI) is a distributed learning program focused on the intersection of design, technology, ecosystems and communities to improve interspecies wellbeing. It connects a global community of changemakers with local innovators in order to address complex challenges. Combining online and hands-on learning, it supports the development of the social and technical skills needed to develop projects for positive impact in local communities that aim for global transformational change.


- [MDDI](https://iaac.net/educational-programmes/masters-programmes/mddi-master-in-distributed-design-innovation/)