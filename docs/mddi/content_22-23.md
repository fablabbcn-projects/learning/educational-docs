# Content 22-23

This is where you can find all the presentations we used during the course.
It's still a WIP site, well keep you updated.

## Digital Tools I

**Digital tools I - Intro**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQLizhOITeXkjwQCFELjpvgRJDD_fXxi855dwBiGjs9bI3PiAyrtErVhqLDAhQBfa7XDPbSVBAq0Oxh/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W2 - Digitalizing realities - Computed aided modeling**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRMcI_ZnM3cofCldW1KcRn1RWMrueHU8xCkKbpxuZGVZnzi0Ck7XL_LDRQyZZVppuZt_Cy9CVNFpOwZ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W3 - Design for sustainability-Digital trends in advanced modelling**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTyEALkUiHQzMwFWAPtOqGJMcrvvvue4bYw-PK-zukCRy-tC5m_d8AW2Ua7-BtlvrcQeRqpHyIOJNHd/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W4 - Sharing bits - Collective design workflows**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRUKYupXqwL1TQ9UsEWLk-9ce68d062FMzF-jfJ4ns_uO2NzhQ7BV2-C2mKW3D7nukNN8xK-U6gavFh/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W5 - Digital Sensing - Digitalizing the world**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRcX8keu1IWx1dOM9R75rNJRZgIhLul8JrFqtCEsXdXMXTyCpST0LWFtKg4-a22HmexwL8QJqsxJE1d/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W6 - Interacting the world**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSFpuEW_98ZQoM2dOVmxkgKPUnumr7UgS8GQTP3EfKgO-6ic5LBsSp0pwd0FiKHUyLobS_lRRh_sDgC/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W7 - The world on data - Visualizing data**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRbN1gMSZrE3VZx91x1gzn8ArjEXsLtFvJ6rOBXKsWxfCwA7Rfs5tW4FXHzYe0qFJ_dHESZuz8YzjtE/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Miro Board link

- (Miro Board Digital Tools I)[https://miro.com/app/board/uXjVPKpcgPk=/]


## Digital Tools II

**Digital Tools - W8 - Bidimensional Fabrication**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTLpB4jOdBTz7LZ_cPnu-TssfzTmogEd46T7MTbjVr8FKfhV_HXvjQLwjjpo6Z4_w71UsYZhSYNdmLN/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W9 - Intellectual property**

[Intellectual properties readings](https://hackmd.io/@vico/BkirIJiFO)
[Intellectual properties class](https://hackmd.io/@fablabbcn/HJZ9KGRoE)

**Digital Tools - W10 - Additive manufacturing**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSRlQZMqsLeWwwVCl6AAupz-ST8RG8Bd8ZcDZJauoMmNrY4KXM1sa7a_0_ySlH3M_I2AccraMCMENq2/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Digital Tools - W11 - Scanningtheworld**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQP54kAVdJJpxRoSEfKa04BcTd8Vlh2hEXpep-dSWbvYQXzxi_kdJNMm8vp_KY7UfDb3Qa1DiWpm6x5/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Miro Board link
- (Miro Board Digital Tools II)[https://miro.com/app/board/uXjVP0J-T80=/]
