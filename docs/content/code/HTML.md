# HTML 


[//]: # (Global FabAcademy CLass links)

## Fabacademy Global Class

- [Principles and Practices](http://academy.cba.mit.edu/classes/principles_practices/index.html)
- [Project Management](http://academy.cba.mit.edu/classes/project_management/index.html)



[//]: # (Presentation used for Fabacademy, explaining the basics of HTML and GIT)

## Fabacademy Presentation

**Introduction to web design / git-gitlab**

<!-- https://docs.google.com/presentation/d/1b3YbeB8zFUMmHigqtQPV4foq_33r-vST4KqQ-P8QktE/edit?usp=sharing -->

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTlR_gYYka8IYPYpUigS_Ji5jBFsbe2UxNxlnDpEQpYC8SaC5M6Pb4Orh9dsuJU0N3RDmNM-i7gi-ab/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


[//]: # (Old presentation for basics of HTML and GIT)

## Presentation How to make your web page in 10 steps

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSEWikJkQBGpCkUwxrVq9s84cYB_VK_V-BC1PKFPaQ5JdrhyxDYehdiyaQXkRXIt4SxEdLHORAz6DbP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>



[//]: # (HTML Exercise to lear HTML CSS and JavaScript)

## HTML exercise

[//]: # (Here are the files to explain the basics of HTML, CSS and JavaScript)

In order to go through the basics of a webpage, let's go thought some examples:
!!! Note
    - [Here you can download all the files needed for this guide](/learning/educational-docs/content/code/assets/HTML_Excercise/html-class.zip)

First of all, let's try to understand the basic structure of a website:

   - [Basic Page](/learning/educational-docs/content/code/assets/HTML_Excercise/01-BasicStructure.html)

The second example is about some of the different items that we can use in HTML.

   - [HTML elements](/learning/educational-docs/content/code/assets/HTML_Excercise/02-BasicItems.html)

Let's give it some style!

   - [Styling HTML](/learning/educational-docs/content/code/assets/HTML_Excercise/03-Style.html)

What is CSS?

   - [CSS file](/learning/educational-docs/content/code/assets/HTML_Excercise/04-StyleInCSS.html)

And what about JavaScript?

   - [Making it interacive with JavaScript](/learning/educational-docs/content/code/assets/HTML_Excercise/05-JS.html)




[//]: # (HTML Resorces)

## HTML Resources

- How the web works: [https://github.com/vasanthk/how-web-works](https://github.com/vasanthk/how-web-works)




[//]: # (HTML Tools)
## HTML Tools

- [Online tool for Bootstrap template generator](https://www.layoutit.com/es)





