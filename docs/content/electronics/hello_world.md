# Hello World!

## Our challenge

Let's make a small _intervention_, _device_, _installation_, however you want to call it, that uses the inputs and outputs below.

## Inputs

Inputs today will be either switches or capacitive sensors.

### Switches

Simple switches can have many many shapes!

![](http://rebeccaapeters.com/images/installation/morethanwords/exhibition003.jpg)

You can use a simple push button to do really fun stuff!  
Remember to use the [digitalRead()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/) function on your sketch.

!!! Note "Some ideas"
    - [Fabacademy Project 2020](https://fabacademy.org/2020/labs/barcelona/students/david-prieto/projects/final-project-hardware/#assembly-of-the-button-sensor-with-copper)
    - [Dance dance revolution](https://medium.com/@melhuang_/building-a-diy-dance-dance-revolution-e136265bbbfc)
    - [Art installation](https://oscgonfer.gitlab.io/portfolio//2018/more-than-words/)

### Capacitive Sensor

_...capacitive sensing (sometimes capacitance sensing) is a technology, based on capacitive coupling, that can detect and measure anything that is conductive or has a dielectric different from air._

![](https://i.pinimg.com/originals/3a/85/f2/3a85f2ce96f760f14e67ab359b56169c.gif)

![](https://hackster.imgix.net/uploads/attachments/865224/2z2js2_3edJ2wegi7.gif)

#### ESP32

ESP32 based boards already have support for capacitive sensing through the [arduino-esp32](https://github.com/espressif/arduino-esp32) core. An example can be found [here](https://github.com/espressif/arduino-esp32/blob/a59eafbc9dfa3ce818c110f996eebf68d755be24/libraries/ESP32/examples/Touch/TouchRead/TouchRead.ino)

!!! warning "Prep it"
    You will need to install the core via **Boards Manager** - Use the **esp32 by espressif** for this to work.

**Example**

First run it with as below and check the serial monitor to check the range of values that it reports when you touch the tin foil or the pin...:

```
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(touchRead(T0));
}
```

![](assets/esp32_touch.png)

**Example**

Now you can put a conditional with a threshold in the middle between your min (touched) and your max (not touched). Remember to comment the line where you print the sensor value so you can see the `Touching!!!` message. 

```
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
    // put your main code here, to run repeatedly:
    int sensor = touchRead(T0);

    // Serial.println(sensor);

    if (sensor < 30) { // Note that when we touch, the value is lower!
        Serial.println("Touching!!!");
    } else {
        Serial.println("Nothing there!!!");
    }
}
```

#### For other Boards

You just need a resistor (around 1Mohm) and a piece of conductive material like tin foil.

![](assets/arduino_foil.png)

**Setup**

![](assets/capacitive.gif)

_Courtesy of: [arduino.cc](https://playground.arduino.cc/Main/CapacitiveSensor/)_

**Installing the library**

!!! Note "Installing the library"
    - [Capacitive Sensor](https://playground.arduino.cc/Main/CapacitiveSensor/)

The simplest way to make a capacitive sensor work is using a library.  
If you don't know how to install a Library on Arduino IDE please follow this [guide](https://docs.arduino.cc/learn/starting-guide/software-libraries)

Search for a [library](https://playground.arduino.cc/Main/CapacitiveSensor/) called _Capacitive Sensor_ by _Paul Badge_ and _Paul Stoffregen_ and click install.

![](assets/capacitive_install.png)

**Reading the sensor**

Try this code example and adapt it tou your needs.  

```
#include <CapacitiveSensor.h>

CapacitiveSensor   cs_4_2 = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired

void setup() {
   Serial.begin(115200);
}

void loop() {
    
    long sensor =  cs_4_2.capacitiveSensor(30);
    Serial.println(sensor);
	
//    if (sensor > 2000) {
//        Serial.println("Touching!!!");
//    } else {
//          Serial.println("Nothing there!!!");
//    }

    delay(10);
}
```

First run it with the conditional (if...) commented and check the [serial monitor](https://docs.arduino.cc/software/ide-v2/tutorials/ide-v2-serial-monitor) to check the range of values that it reports when you touch the tin foil:

![](assets/capacitive_serial.png)

Now you can put a conditional with a threshold in the middle between your max (touched) and your min (not touched) values, ej. 2250 for the values in this image. Remember to comment the line where you print the sensor value so you can see the _Touching!!!_ message.  


```
#include <CapacitiveSensor.h>

CapacitiveSensor   cs_4_2 = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired

void setup() {
   Serial.begin(115200);
}

void loop() {
    
    long sensor =  cs_4_2.capacitiveSensor(30);
//     Serial.println(sensor);
	
    if (sensor > 2250) {
        Serial.println("Touching!!!");
    } else {
        Serial.println("Nothing there!!!");
    }

    delay(10);
}
```

## Outputs

Your led strip is composed of several **WS2812** or **WS2813** led units, it acts like a networked mesh where the signal is passed to all the leds one by one and you talk to each of them by an address that goes from 0 (first led) to N (the last led).

![](https://cdn-learn.adafruit.com/assets/assets/000/010/668/medium800/leds_neo-closeup.jpg?1377729545)

![](assets/led_strip_demo.gif)


The wiring:

![](assets/led_strip_colors.jpg)

![](assets/sw2813_conexions.png)
_...Place a **300 to 500 Ohm** resistor between the Arduino data output pin and the input to the first NeoPixel._

To control this type of leds, the easiest way is using the Adafruit [Neopixel library](https://github.com/adafruit/Adafruit_NeoPixel) if you feel this is too simple you can start with the more advanced [Fast led](https://github.com/FastLED/FastLED) library.  

Install the library with the normal procedure using Arduino library manager.

![](assets/neopixel.png)

To test your leds, copy this simple example and change the _LED_PIN_ acording to your wiring. 

```
#include <Adafruit_NeoPixel.h>

#define LED_PIN    12
#define LED_COUNT 20

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.show();
}

void loop() {

  for(int i=0; i<strip.numPixels(); i++) {
    strip.clear();
    strip.setPixelColor(i, 0, 50, 255);
    strip.show();
    delay(20);
  }
}
```

!!! Warning
	Dont try to light more than 20 leds if you are using the Feather as a power source, keep in mind that the leds are very power hungry and you will need a bigger power supply to light more. You can find more info on this subject [here](https://learn.adafruit.com/adafruit-neopixel-uberguide/powering-neopixels#estimating-power-requirements-2894486).

If you want to see a lot of colors try loading the Adafruit Neopixel library example calle **strandtest**

!!! Notes "References"
    - [Adafruit Uberguide](https://learn.adafruit.com/adafruit-neopixel-uberguide)
    - [Forum post](https://forum.arduino.cc/index.php?topic=563687.0)
	- Adafruit [neopixel](https://github.com/adafruit/Adafruit_NeoPixel) library
	- [Fast led](https://github.com/FastLED/FastLED) library
