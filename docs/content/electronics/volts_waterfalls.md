# Volts, Watts and waterfalls

## Electricity basics

In this section, we will understand the basics of electricity and how it can help us represent information. We will start with the simplest of the circuits, and infer from it how the information can be stored, managed and sent over to other components.

![image](https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQ9kYxL_ca6PUdTXhiiKa64H-Vm5ClMysp3i1qs6aW-JhydyFLM)

**Charge**

It's all about **electric charge**. Objects can hold an **electric charge** (either positive or negative), or not (meaning they are neutral). In subatomic particles, **protons hold positive charge**, **electrons hold negative charge** and **neutrons are neutral**. We find these particles in atoms and they define the world as we see it, being the underlying structure of what we understand as **elements** (Hydrogen, Helium, Uranium...). We all know that particles charge go like:

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/r/400-400/assets/a/6/2/4/4/519fb817ce395fff0a000000.png" width=300></div>

In an atom, the core holds protons and neutrons together, while electrons are orbitting around them:

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/3/4/1/a/3/51a65d7bce395f156c000000.png" width=400></div>

**Voltage**

When we **force** electrons to group in a certain area, leaving another area without electrons, we create a **difference in voltage**. This voltage is the relationship between the energy we applied and the electric charge: 
<div style="text-align:center">
_E = V x Charge_
</div><br>

When **two objects have a difference in voltage**, we can say that their electrons will try to jump from one another creating a **current flow**, to balance out the situation and become stable. Voltage is expressed in Volts (V). This voltage can be constant with time, or alternating:

<div style="text-align:center">
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYYAAACBCAMAAADzLO3bAAABNVBMVEX///8AAAD8/Pz19fWysrL6///7//v5AIP///v9xuD8abTv7++ioqLr6+usrKz//v/h4eHU1NTZ2dnHx8e8vLzf399YWFi4uLiPj49DQ0PNzc2mpqaDg4PDw8Pz//9zc3MjIyMuLi6Xl5dubm6bm5tiYmJHR0c9PT1VVVV8fHz0AIeJiYk2NjYTExMbGxtycnLwAIP51/D88fvyAIr7fbj8AH//AI314PH4L5YoKCjm6+bkZqf1Xab6vN7hAIH3AHL7nM3l//7qus30yt353/D1qNX7yur0RZv4jb/mAI72YaLsjL3p9frzUaXzfbL80e/5sNL3NZ/4Xq+eraj1mcT9ea/qsdj3LIzsSqX/6u+aYnyyKGWrAFetZ4WpSHWzOHycTHe4rLuwAFTqzdv6TZPw6vv9lM917IsEAAASRElEQVR4nO1diWPaRpefecNIxEKMxH3KmBthHIwMxo3dddYHTus4zhe3/rrb79jtpv//n7BvuDGHbxtc/doQEJJ4b37zrtHMhBAXLly4cOHChQsXLly4cOHChQsXLl4RIP+H15binrhVXoBVU2kFWZCtvLidV00nIKsnM5EULKQB7mAwywIImCYjxMhpXZGhR0fvvexsetCcUAeG+g8PAzE1wzR1+Z4FEyysv4DYwTAhoQobE2sgJPQ7FLB1U2ozlBSkdNoNRfoXQP8DvhgGIaa8kuhpE0ztJbgEtkEpzZNARO9L1JMHoMzk7ydpjfpGp7P+n0H7J/14BzCplsK7FBgJ4l+5BGXTP/SUMuN/HrpBSL7Gwut9eYdf9j96fIQVzJ7Q/U5FTJSu2FdycA2QIR14L4ROg2BQqqNyeLovX3wJN4E0VImPJsIR5sunciSWKWiQjm+mq7ToIaDTGNGZp8ACRc2frxYS68k4Se5FDJbzlyK6ubUXwZtEYiRVQjJikMkB00nG/8xCA4mv0wDJ7+llWjT0yJ6PRLPZcmI9tpElemGvoAdq+JJLBHJZ1E8vZAqoFtksM6YbJQPKaSMXjZvr0U09UdpMk5QvUkrgvcpoYj5sd18x7iNaV3eNmi9jDTHCaD5I9SwtR31biULBoKm8GaP+ACFRGkad8zUtSLU8LUY9NJ4M0XRqU69temjKqOWi2FdoglQ3CRTKAZpGY4Fq4ZmFxi4biFdJPqOXNzx6aT1Igz7sNihRFVvNl9ishDOFqE5DQbqepYlKPEyTBPu4BxgJUJOgbnQzG6C1aoAm/VQrUH9xj5U3PchVOUvIhi9ZQ90NaQjl2PMq09NI0gDUl6B6rEZIgca34h6KDhTbHYWIYp9DGrY0/D5PAUVjpErje7VAJk/K6ySTRZPX8CS0BpIrm0gD0pbceF6ZAZLUX8iQbAZSRWLQzRLN+qn0VJpBzXBug+ZICbuypEFnNBiLB2kInT5F+4YADTDqMbDrGHgwQTc3aaJQJr4awXuh8KUkCdNUjBpIg3Rn65VnVaavkqQhSRMobyxDSC4eDoSD1CAsjd4RsOtgLwK0Bmz/bK1HQ3bLDAT0rTwprpM97DpE0lCNo+etsloKQCO+0jMLTTYz8SI1fRmoxLGT5w0TbZVIGsLYpnEWeU9KVZQrhL1Ho8HA1nuPjCiZAgAL0KBGPUgGtraJNKQDJtoxWjypFGVgKSZJlhaLNGtI3RnJpV4mNmAgipE0NapoDQlaLlbZRi2TD1B0uEBSNEOjJo1nkKZef0O1S4UC+jFSfE/KdFM6JQ+RITrCiJ/u4c0Kz9yBQDo/Qtd9NZakG2aV5jZMX086dDkpWqjl0K5LBo1i79JpIkQLEek8o7RWWyd0qySdkonWECRQpu9rpFwkaOs+WsI4sP6e1N6jEVBSQWWipPTMga6nEQSDQQNjcQjCKBQYnpBONPmS8BgyYzU9aQYJvx5kRhp9cgj7ixaNGgwvSgRA9+BBki2SQDoYkCYd8Ic0hgb/rDITPYruwgwaQYBQlKGoQZQeD6eBpTWIpnVTSsbSuhZlLGSUq8HKloaXhD0hg+j+QMBgIdY9F1/8mJWih+rdC0iwpqd1+ROA900zk75A+j0YxoBh9tyrFgaHYFgnkMEp/Yywf5Z8x5jMMHr3kamqP/XcMt+Qsl9QQ0/gYRUweMO23ueLFTIUnAzOHZ4+KhzwpRIdL42qvsU14jPoNVJo8GnwFRmqBgMKYFz/0fANyLrieeuGnlizQW7qIGH48qGRTpPnjzE40BOzjv4ndmulvjyAm+MgSzaiBuMl2p1Oh4kPqzLKc1PD5WKBjIY37nbyBA1Py8KyNcxfD0BCOe3205YZwcLzB5vnh0+OU60yknTF+xGR1vAGaFhxBbrwvUwF8nzwvwFrcGlYErg0LAVcGpYCLg1LAZeGpcAq09AdWHBpeG2sPg1AwpXuuNRq0iDH1PR1Oea86jQEKFtdGiQPYTnjabVpAL1QppECwGrSICcMFiK0IEf1VpoGLRmjfv9y0CAEEfe8BFCBLPX5YcVpkE6p+wwDaRAg5j02fAkoXPCHDM6Fe9MwV5kGBAt1n0f66A+KorwmDaAo4r7WQKRBR9nqZ0qDp/lJ+gPxcuUVYX1kwnqQDmTlaRg+rPfRD2uvi/0D60EPDAAm6wZBuBDdl7veoXeq4He3Rbw9no/XoSO9p7iL4aM/qrb6mviPw4/wCJ0GNHBgTBoX+rg7sioYcFCIwtmdf5+LBl4BliIe4kcXwEf/890rYxcaT0ADNKzG8dHRsbAU5W5Xcu5V4Pjd0TZ479qoAkm2jk/fHXPraaeYYGzgrwvg1j3cwhT6NIB1ftJx7Jazf2rBnZoIuICDTl21f+yc3rUbCEX82nFUtd6+hDuSfTdgwor3874mQJBHqCRpQHcNjX10rrbadOpnlsJubVd07g3rFzy75TRbzoWwbjUIvAI7zJltq05L/aR+QI/2dAaxBOXb4yBpOG9Y7CdV3dlmjdOO2vz59gbyYq1ifVDVD6fn7OizXb8Qt16CJqvAz2q9c9o4/7LWtPct5enC9JugQSj8ou78Ct3894Na/1WxbunbvKHww2brUHhBAXHh2CfktkuE5eWXTXXH8n60QDmx7bOH5dkz8RZoAKYcqPaf2KAMBDQ+O2rjtsJcCO+pau98VCzeEBbfsZvHt5mDUJTten3fanC8hIuLprrtfTIt3gQNwK6dtXMsP9DBnyvf684HfkuYFsK6dtpWA2Sl0eCsY3+2ZMxedI3SWGtdNxhWDYpXWNZnuy0veBoq3gQNyqHtfLG67h1dPj9R69u3DVOJg3rzVOlnysAPHPUdXxwfhHKq1k+swXJp/kW1LxVFuDR0gTQozGmdWdD11NhXLd52flpMAwaTtrPPoV8LA1g/tTrSHBZcA3zf/nw+uC/ysOO0mfJEVdxboIGfNJ1dgG7HBDngcKDWjxdeBNaBqu4KC/qtCPzIUU+HH2eCH6k/vrMaw1O8v9n2yVNVcW+BBvFVvSCDroxZJBe2c2EtGPXhXt6RxjBqdoCO8zdlfurDkd2dVgd7/2jLAWtH3W+4NPTgpz98qbe2vYPm8BIB1qFdR7c/321jzqMejDegwi+d+m/zmQNQGj86l+NrQRQ0D4xBjxS/j5Wn4RtlO+rnUSjo0vDdVi/nmoNXCL5Tb1njpbsA5qiHc2MDRm/r0nH0cWLRmDrq2V3HEW/BytOQpEbLvhy1D9KAWf0H9DlzHT3WFm31ioxHccGVHfvr3AqOC0vsO79MfI/F4lW93Xik+H2sPA1++qtqNyZX/AB/pzq7855wYwg4cpq7E0UCcOvIrh/Pc2NewY9V+2gihAvOj+utd5wsDOx3xBug4XNdpp5jhzCONlr1y3l9WwF+4XQmh/KwHhPt+tm8EK0ILEY6WD9PHhb79o4M9I/nYeVpSNK6czCZvitYO2AWc7PRhoDGNfqkCacunf9Z63reFYpgn1sX027uRG03FJcGIq2h7jS8E+3nFQpWvPbxnOgJyruWuuvlkx4GvH+01O15v8KPbedoigY82jrid6BhMPthtJfCaG07wFLN2oM5w0Aj2WG0FcRoObOf/rg/XTJ7Gxi351TSwM+cTmP6AThcO1dYUcyKD+iT7PaMS6yOfcaVQS2+YGnzGAVEZ/2l7T0+ulctDQ0wjwfobfAgv5R7GI1PrZFf+6kz3d4C+Jo9b0ADzjHR5NMP9a1fnDZXYGaY5vv1HT4Vi4VyqHYGAxqLt24AUkmTbFK+LYc0c+KrpbKGhDlbja4C+YECgd6h0aJ6pOG3GRfBpTMvmYRt9D6WmKJBOVLV78rMakNp2Oq76elUQtlV1WPWTcnkrjULlvoD8eVILRAsZ1GLwkY0XUixdCQXTURispP5lmaaj4dWtVlbanQV2DOlvMVQLuMJRlIQjOT8ZqG7w5qf/n3m7X5rtY5m/xC/an2d7c0/2Sd8dpp7aTszSRWd1tWAtxCtLOrSxl5iQ6PhUjSSXl/XwumN0GYgY26FMumuNSQ9y4EKpbUkm0EDGBuJTZ2apWg5XVlnBipQMjfMvehmtDuj+7+iM+727dt///6Pb/7p4/5v/n/+/k/P9Deeb55//f7v/8G/puD3/OP3f80U2v/v//3n4H1KKrDgOUcxEgtTUvEVgrEsKcZK0Wpx3aCxrNxgykeXCv5ps0a9ypGYIRWIpLMxEo+VQtVixajFYtLFJl9b5En45vklub+ZSYpFasTTSeqLF2jofbliRAoRo2sNBlsKgJ/Sgj7Lu0oFAqQcp3opJBXI0dB6uWLmIoWw3O11tgIa+6NZ/2PmLx3U69/PZ36zW6+fKNqML07V+h/WrF85t5j2g9bofvD0FJgHYAG5LS0DgxFT0wIsUAxXUiQQWK5lJv5MYmayJHeQCuNxUyNhqQAzmVkKVyok0N22zE9/mBVUudL42rqajsNcYEK0NjMr5aB8bV1YMyppfqV2uHdG0PBiSjb8DU8tuGg3nkGRMNgti7HKVk6DQcL6GiG6ny+P5c4A+oK8+8ZOXyyVyQ3C+ZypxNimO3YHpnJMUBpt7PKz4jAH61B1poQATFTbzsXkpBvoNasYbq0mV4Hda3elfofrX/FK1sB0piXMPg2sz8X90G+JOTR4uSykv0+NK3H+TlV3Z9MglO2m/eXm1DXgynFT3VbGaQDQNB0VIAzYqHvcU/5BMf16TgmK6EejIckARoXuH7jP/lpjOs+lQT5BOJkaf1C4fDoxe/hBKPza2bnpxrycH6rX55MDsgQVKKc9SIcUnmn3JmHgEV7XGkg1RmL+bHwrsrFJUvH4PfUY63xzaFCwTdfszzeDgFdpXNuH3plDeN5z6/wMC+kbhznnHedisrTED9kUyfqSpUx5c5PF4iXtcfu9vZI1kFSVpPLZiplhNEGzteiDlZi7zAT4QfPT8Q0XI6wvtroNcydUbKv17fFZ80LayHe1dXDDrBiJVUg168uFM9oWKpCJPlD8Pl7LGiQNvlg1EGfUpNGo/uBH6/NX+0DDVk9uHuQXakfMn43N7dafFowckJA514nanloCAV0a8qnwprZlUk/okXtrvZY15H0k7/H7jBxEdH+x+PQ0CMySdup/u9nt5eNPPn9CknVmt8XYU2whZ2C05TwPZdy9ybWbeZL3e7J6jhVQgbixkjRM7Gb6KK86hwZ0LMJ7oNrHE8/sBTlS7V2YT4PYlfOVhk5JYMLr3W3Wv0yN+N1U4JEzA5anfHsgFi1BxHD889iELmx9/guG7QWWx+HvrR1r6JSEIrzeM/v6iR78z8dbpgEUbEE26vloBcfq4pl2YJ00ncYwgivSQamfrh60cPs+eMs0CO+2ox5YQ3eCZTImpI1FjyzBatj24XBqt1zhc4JV4BPNRpqPt0wD94o1+//Oh83OxPd262zuTAEJAOWi2W5Av8ATXBHXrTXuWsNtWBQbFH5qN4cpv7CUQ7u1u3BFM8gJSc6V1XdlgE7KaW5bixc+PAHeMg0CXcpPn9qsO2dYDuHs2vaZtbBng5xb03J2FW83PnitRr21dusSrsfjLdMgVw0ef+rN6JI0WB3nGv3Nwp7NhdW4dvahV65x8aGlfvc+xby8xXjrNFh/qvalTFUxyl7Y9QMLFq/P4YIply37z4/onpCJK1U9ecqFt/PwpmkgMvXZb9WvLK4o3y/q6tkdYi0o4sJWL87l3I0r1Vl7kX9y4q3TwBX22Wm1Dw9+dmwszG5bjgCycrYaP9Wd9tnB1XVL/fxEi9tuwVunwWKWWKs7qtpy1J+tO+X/MoqcNfES22nu8I/PHhck3joNmPoo1pe1a7tztoue6S5tKoiiWLsXnfrXs23+VGsMb8Fbp0ECLMviXPHe3cfLVJejB3uBqNDDX4EGLoBhIXyfGkwuslYetbPN/fBXoEFu6SgHvu9Bg5CD4bdvZ/I0WKp5Sg8B3HGvPTlC5L1PyuPtzht+7iRpOL1mNWkYPGWBFd8cuj9TaVVpGH/atdI0sGg0GoqSpdiV+AEAwkISd3VKywo9Ho+XiqtLA+jFUjxeXPU9uvuzFlfVKZHRhMVVpmH4TzyvKA2D3XRW/R8O6K+vXFUaxtayrjINI6woDSO4NCwFXBqWAi4NSwGXhqWAS8NSwKVhKeDSsBR4CzT0xpRe6OnG8yC56v2ISBrCafZyT1yfA0aarbT8XTxgPfKSYeUVILdtxbQSWHX5Xbhw4cKFCxcuXLhw4cKFCxcuXLgYx/8DGHmmVggGQO4AAAAASUVORK5CYII=" width=400></div>

<div style="text-align:center">
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.Aa-X7gCjIsLGmqi3M-oVvAHaGP%26pid%3DApi&f=1" width=400></div>

!!! Note "The units"
    Energy is measured in Jules (J), voltage in Volts (V) and Charge in Coulombs (C). Note that all the units that come from the discoverer's name are capitalised!

**Current**

When two objects are subject to a difference voltage, electrons will try to come back to their position. When doing so, we say there is an _electric current_ or just _current_. This movement of electrons inside a material is measured in Amperes (A) or just Amps. If we have alternating voltage, we will have also alternating current (AC), and the same with constant voltage, in which case we will have DC.

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/9/5/6/1/4/519fcd42ce395f804c000000.gif" width=400></div>

**Ohm's law**

For electrons to go from one point to another, when subject to voltage, they will have it more or less difficult to go through. How difficult it is, is called **resistance**, and it's measured in Ohms (Ω). 

<div style="text-align:center">
<p><a href="https://commons.wikimedia.org/wiki/File:Georg_Simon_Ohm3.jpg#/media/File:Georg_Simon_Ohm3.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/2/2a/Georg_Simon_Ohm3.jpg" alt="Georg Simon Ohm3.jpg"></a><br></p></div>
<p><i>By <a href="https://commons.wikimedia.org/w/index.php?curid=1507336">Wikipedia</a></i></p>

[Georg Ohm](https://en.wikipedia.org/wiki/Georg_Ohm) discovered that voltage (V), resistance (R) and current (I) go with the following formula: 

![ohm](/learning/educational-docs/content/electronics/assets/ohm.png)

Meaning that when the resistance is two high, there is almost no current (and when the current is 0, it means we have an _open circuit_).  When the resistance is almost 0, the current can be very big, leading to what we call a _short_.

![vcr](/learning/educational-docs/content/electronics/assets/vcr.jpg)

The nice thing about it, is that we can **control this flow** (or lack of it), and then we can make very cool things!

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/a/0/9/4/0/51a52b62ce395f2f25000001.gif" width=400></div><br>

!!! Note "Some reference tutorials"
    - [Electricity basics](https://learn.sparkfun.com/tutorials/what-is-electricity)
    - [Voltage, current and resistance](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)


## Our new best friends

### The multimeter

A device that combines several measurement functions in one single unit. At it's minimum can measure **Voltage**, **Resistance** and **Current**. It also serves as a debugging tool to check for **[continuity between points](/learning/educational-docs/content/electronics/assets/continuity_mode.jpg)** in the circuit.

<div style="text-align:center">
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.sparkfun.com%2Fassets%2Flearn_tutorials%2F1%2F01_Multimeter_Tutorial-04.jpg&f=1" width=400></div><br>

!!! Note "References"
    Here are some resources for better understanding the multimeter and learning how to use it:

    - [Sparkfun tutorial](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all)
    - [How to use a multimeter](https://www.sciencebuddies.org/science-fair-projects/references/how-to-use-a-multimeter): this page also contains a summary of the symbols and different forms in which they are represented in diferent multimeters

### The breadboard

It is a prototyping tool we use to **connect electronic components with ease**, so we can make circuits fast and prototype easily:

![breadboard](/learning/educational-docs/content/electronics/assets/breadboard.png)

!!! Note "References"
	- Another sparkfun [tutorial](https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard), now about breadboards.

## Hands on!

Let's make these first easy circuits and compare:

- Voltages in +/- lines with a multimeter (change USB - Battery) and resistor size
- How bright the LED is

![circuit 1](/learning/educational-docs/content/electronics/assets/circuit_01_batt.png)

![circuit 1](/learning/educational-docs/content/electronics/assets/circuit_01_usb.png)

Now let's integrate a **push button** in our circuit so we can control the led manually.

![circuit 2](/learning/educational-docs/content/electronics/assets/circuit_02_usb.png)

!!! Note "References"
	[Resistor calculator](https://www.calculator.net/resistor-calculator.html?bandnum=5&band1=red&band2=red&band3=black&multiplier=black&tolerance=brown&temperatureCoefficient=brown&type=c&x=82&y=26) calculate your resistor values.
