# DIY Sensors- Input devices

This is a collection/instructions of sensors that you can build at home with materials.
*Still you will need an arduino or similar board*

**This are the sensors you can build:**
- Force sensitve resistors
- Shock Sensor with a speaker
- Plant Moisture Sensor
- Capacitive Sensing
- Flex/bend Sensor
- Proximity Sensor
-
## DIY Force Sensitive Resistor (FSR) #1

![](assets/sensor.jpg)

#### materials
- Soldering iron
- HotGlue/glue
- Knife
- Aluminium foil/ copper tape/copper pcb
- Wire
- Conductive Foam/sponge material
      ```
      Conductive foam is what microcontrollers generally come packaged in. If you've received little ATmega microcontrollers or PICs, sometimes they'll be surrounded by conductive foam inside a little case or box. Not all conductive foam is created equal: some of it bounces back into shape faster than others. If you use PIC foam to make your FSR, it will respond quickly, but if you use ATmega foam will take a second to release. The fact that this FSR has a visible deformation is the primary difference from other FSRs.
      ```
#### Steps-fabrication
Cut your foam into the same shape as the plate or you aluminium cooking sheet

![](assets/copper.jpg)

Solder one wire to each plate.
Glue the three pieces together. Only glue along the outline of the FSR, otherwise it will not conduct well. For mine, I just glued the top and bottom of both plates to the foam.
![](assets/foam.jpg)

Grab a multimeter and measure the resistance across your FSR. Your values will vary, but I got about 200 kiloohms at rest and 9 kiloohms when almost completely depressed. If your plates have a larger surface area, or the foam in between is thinner, these values will be smaller.
![](assets/multimeter.jpg)

Now connect to you board like any other [resistive sensor!](http://arduinolearning.com/code/force-sensitive-resistor-example.php)

![](assets/resistive.png)

*You will need to try diferent values of resistor to get good readings

## DIY Shock Sensor with a speaker #2

![](assets/speaker.jpg)

You will need to find a speaker that you are willing to sacrifice for this project. I used a small speaker from an old pair of headphones, but you can find one almost anywhere - like a musical greeting card or an old alarm clock.

#### Materials

- Resistors 220 ohms or similar
- cable jumpers
- speaker
- arduino
- led

#### Code

```
int shockMin = 996;  //you might need to change these
int shockMax = 1010;  //you might need to change these

void setup() {
  pinMode(11, OUTPUT);
  // Serial.begin(9600); //uncomment this to help with calibration
}

void loop() {
  int shock = analogRead(A0);
  int lightval = map(shock, shockMin, shockMax, 0, 255);
  if (lightval > 0) {
    analogWrite(11, lightval);
  }
  else {
    analogWrite(11, 0);
  }
  // Serial.println(shock); //uncomment this to help with calibration
}
```

#### Wiring

![](assets/scircuit.jpg)

Press on the center of the speaker with your finger and it should make the led blink. If not, you will need to calibrate it in the next step. Otherwise, you can try attaching the speaker to something. Maybe you could make a drum by taping it to a paper plate? - Try using pencils as drumsticks.

#### Calibrate
If your led is already blinking satisfactorily, you can skip this step. Otherwise, follow these steps:

- Delete the "//" on the lines that say "//uncomment this to help with calibration"
- Upload the code and open the serial monitor
- Press on the center of the speaker and watch as the values change
- Change the shockMin and shockMax variables to the low and high values in the serial monitor

```
int shockMin = 996;
int shockMax = 1010;
```

For example, if the serial monitor reads 700 as the non-pushed state of your sensor (when it's just sitting there),

and when you push it it goes up to 860, change the shockMax to somewhere around 900 (just a little above the sensor reading) and the shockMin to about 680. Next:

- Close the serial monitor
- Upload the new code
- Press on the center of the speaker some more

If it all goes right, the led should turn on only when you press the sensor.

## DIY Plant Moisture Sensor W/ Arduino #3

![](assets/moisture.jpg)

This project will calculate the water content of soil around a plant by measuring the dielectric constant (the soil's ability to transmit electricity) and will alert you with a red LED when the plant needs more water or a blue one when it has too much.

#### Materials

![](assets/mcircuirt.jpg)

- Arduino UNO or equivalent (x1
- 220Ω* resistors (for LED) (x3)
- 10kΩ resistor (x1) - brown black orange
- RGB LED (x1) or 3 different colored LEDs
- Jumper Cables (x6)
- Breadboard (x1)
- Bolts of any size (x2)Nuts same diameter as bolts above (x2) or any metal bar

#### Sensor

![](assets/screws.jpg)

- Begin to tighten the nut around the bolt
- Slide the end of the long jumper cable between the nut and the head of the bolt.
- Finish tightening the nut until you are unable to pull out the jumper cable

#### Code

```
int moistPin = 0;
int moistVal = 0;
int tooDry = 150; //set low parameter for plant
int tooWet = 400; //set high parameter for plant
void setup()
{
  Serial.begin(9600);
}
void loop()
{
  moistVal = analogRead(moistPin);
  Serial.println(moistVal);
  int percent = 2.718282 * 2.718282 * (.008985 * moistVal + 0.207762); //calculate percent for probes about 1 - 1.5 inches apart
  Serial.print(percent);
  Serial.println("% Moisture ");
  if (moistVal <= tooDry) {
    digitalWrite(4, HIGH); //Red LED
    digitalWrite(3, LOW);
    digitalWrite(2, LOW);
  }
  else if (moistVal >= tooWet) {
    digitalWrite(4, LOW);
    digitalWrite(3, HIGH); //Blue LED
    digitalWrite(2, LOW);
  }
  else {
    digitalWrite(4, LOW);
    digitalWrite(3, LOW);
    digitalWrite(2, HIGH); //Green LED
  }
  delay(250);
}
```

#### Test

- Insert the prongs you made about 1" to 1.5" apart in soil close to the plant you want to monitor.
- Give the plant a healthy amount of water and open the serial monitor
- It should give you readings around 25 - 30% if you gave it the right amount of water
- If not, try moving the prongs around to get it right (or you just added too much water)

## DIY SPONGE SENSOR #4

Link for the [tutorial](https://www.instructables.com/id/Sponge-Sensor-for-Scratch/)
The sponge sensor works like a variable resister- electricity passes through a wet sponge.

![](assets/sponge.jpg)

As the sponge is squished more or less water lets more or less electricity pass through.

## DIY Proximity Sensor #5

How to make a proximity sensor using a infrared led and infrared phototransistor.

![](assets/infra.jpg)

#### materials

- Infrared LED
- Infrared Phototransistor
- 33 ohm Resistor
- 10 kohm Resistor
- Black Heat Shrink Tubing (the same diameter as the phototransistor)
- Perf Board

![](assets/i2.jpg)

#### How it works

To make a simple proximity sensor, all you need is a light emitter and a light detector. The light emitter is constantly on. Whenever that light hits a nearby object, some of the light is reflected back to the detector. The closer the object is, the more light will be reflected. By measuring the output of the light detector, you can get a rough approximation of how close the object it.

![](assets/work.jpg)

This isn't exact because the amount of reflected light also depends on the physical properties of the reflecting surface (sometimes called its reflectivity).
I chose to use infrared light for this sensor because it is invisible to the human eye. So your guests won't notice it. Also it won't experience as much interference from the lighting in the room.

#### Sensor how it works

The IR LED is connected to the 5V supply with a 33 ohm series resistor. To find the appropriate resistor value for the LED use this formula:

Resistor Value = (Supply Voltage - LED Voltage) / LED Current.

In my circuit, the supply voltage is 5V. The LED is rated for 1.3V at 150 mA. This gave a resistor value of 24.7 ohm. The closest resistor that I had above this value was a 33 ohm resistor. So I used that. Be careful to not overload the resistor. A 33 ohm resistor will experience about 1/2 watt if power. If you don't have a resistor rated for that much. You can use two 68 ohm resistors in parallel. Or you can use multiple LEDs in series to bring down the voltage across the resistor.

![](assets/diagram.jpg)
The emitter of the phototransistor is connected to ground. The collector is wired to a 10 kohm resistor that is connected to 5V. An additional wire is connected to the collector to act as an output pin for the sensor. I soldered the sensor components together on a small piece of perf board. I added three pins to connect the sensor to the signal processor.
![](assets/board.jpg)
To ensure that only reflected light is detected by the phototransistor, it is important to add a light barrier between the LED and the phototransistor. The most effective way to do this is to put a black piece of heat shrink tubing around the phototransistor. This also helps to make the sensor more directional.

#### Code

In most cases you will use a microcontroller such as an Arduino to monitor the signal from the sensor. To do this, connect the ground terminal from the sensor to the GND pin on the Arduino. Connect the 5V wire from the sensor to either the 5V pin or a digital output pin set to HIGH. Then connect the signal wire from the sensor to an analog input pin on the Arduino.

```
// Example Code
int InputPin = 0;      // analog pin 0 is the input pin
int OutputPin = 1;   // digital pin 1 is the output pin
int val = 0;              // variable to store the value read

void setup()
{
pinMode(OutputPin, OUTPUT);      // sets the digital pin as output
}

void loop()
{
  digitalWrite(OutputPin, LOW);    // sets the output pin initially to LOW
  val = analogRead(InputPin);        // read the input pin 0 to 1023
  if (val < 800)                              // if sensor value is below threshold set output HIGH
{
  digitalWrite(OutputPin, HIGH);    // sets output pin HIGH to activate special effects
delay(1000);                  // waits for a second
}
}
```

The AnalogRead function measures the voltage of the signal coming from the sensor. When the voltage drops below a set threshold, the Arduino activates the your special effects. Here is an quick example of the code that you could use.

## DIY Flex/bend Sensor #6

Build a flex/bend sensor that is compatible with an arduino board and does not require static bags or anything unusual, you can make this right now from things you have around the house.

[LINK FOR THE TUTORIAL](https://www.instructables.com/id/Diy-Cheap-Arduino-Compatible-Flexbend-Sensor-Made-/)
