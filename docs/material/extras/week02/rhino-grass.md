# Computer-aided design
### Rhinoceros / Grasshopper Class

* [Computer-aided design](http://academy.cba.mit.edu/classes/computer_design/index.html)
----

### Rhino / Grasshopper Tutorials References
* **Modelling with parametric tools**
     * [Video tutorials]() *(Free + Step by Step)*
        * [Official grasshopper introduction](https://vimeopro.com/rhino/grasshopper-getting-started-by-david-rutten) *(by-david-rutten)*
        * [Visual Programming in Rhino3D with Grasshopper](http://shop.oreilly.com/product/0636920051985.do#) *(6 hours video)*
        * [Noumena Grasshopper - Lecture](https://www.youtube.com/watch?v=mktp0ibtcTw&t=6815s)

     * [Book tutorials]() *(Overall & concepts)*
        * [Grasshopper fundations- Third edition](https://aae280.files.wordpress.com/2014/10/mode-lab-grasshopper-primer-third-edition.pdf)   
        * [Introduction to Grasshopper for Rhinoceros](https://bit.ly/2WnYVoh)*(McGill School)*
        * [Grasshopper Premier](https://drive.google.com/file/d/1Zgc4NzQ-arbs9zwjG98oJClRNbMWds-M/view)-[Backup link](https://github.com/EDUARDOCHAMORRO/grasshopper-primer/blob/master/en/SUMMARY.md)

* **Rhino 3D Design Tools**
    * [3D CAD: 3D Modeling Tools for Beginners](http://archive.fabacademy.org/archives/2016/doc/3D_CAD.html)

    * [Rhinoceros 3D](http://www.rhino3d.com/en/) *(Commercial 3D Cad software)*
        * [ Rhinoceros 3D - Nurbs](https://www.youtube.com/watch?v=SXReLvfCA_Y)

**PLUGINS TO INSTALL**

* [BASIC PLUGIN SET](assets/scripts/plugins.rar)

* [ADVANCED PLUGIN SET](assets/scripts/plugins-advanced.rar)

**CLASS EXAMPLE SCRIPTS**

* [SIMPLE GEOMETRY SCRIPT](assets/scripts/01-Basic.gh)- [RHINO FILE](assets/scripts/01-Basic.3dm)

* [LISTS SCRIPT](assets/scripts/02-Listas.gh)

* [GEOMETRY LISTS SCRIPT](assets/scripts/03-Geometria-Lists.gh)

* [SPIRAL BASE SCRIPT](assets/scripts/04-Grow-Scale-RotateSurface.gh)

* [DISPLACE GEOMETRY SCRIPT](assets/scripts/05-DisplaceGeometryConect.gh)

* [EXTRUSION DISTANCE SCRIPT](assets/scripts/06-ExtrusionDistance.gh)

* [LIVING HINGE SCRIPT](assets/scripts/07-ParametricHinge.gh)

* [PRESSFIT SCRIPT](assets/scripts/08-PressFit.gh)

### GRASSHOPPER CLASS

<script async class="speakerdeck-embed" data-id="b7fdf6c9b06e44499f69c414dfb17485" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

[Download PDF presentation](https://speakerd.s3.amazonaws.com/presentations/b7fdf6c9b06e44499f69c414dfb17485/Grasshopper.pdf)


#### EXAMPLES SCRIPTS

- [Simply geometry](assets/scripts/01-Basic.gh)
- [Lists](assets/scripts/02-Listas.gh)
- [Geometry lists](assets/scripts/03-Geometria-Lists.gh)
- [Spiral Base](assets/scripts/04-Grow-Scale-RotateSurface.gh)
- [Display geometry](assets/scripts/05-DisplaceGeometryConect.gh)
- [Extrusion Distance](assets/scripts/06-ExtrusionDistance.gh)
