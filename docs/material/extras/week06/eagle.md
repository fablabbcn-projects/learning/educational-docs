---
title: Eagle class
description:
Autors: Santi Fuentemilla, Rutvij Pathak, Eduardo Chamorro, Xavi Dominguez, Óscar González, Esteban Gimenez
---

# EAGLE, Electronics design

[TOC]

![](https://i.imgur.com/IjIPoZQ.jpg)


## Tutorials

- [General tutorials site](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/)
- [Introduction to Eagle](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week06_electronic_design/eagle_english.html)


## References

+ [Webs](http://electronicsforu.com/)
+ [Concise electronics for geeks](http://lcamtuf.coredump.cx/electronics/)

#### Books:

- [“Make: Electronics Learning by Discovery Charles Platt” (e-book)](https://www.amazon.es/Make-Electronics-Learning-Through-Discovery/dp/0596153740)

- [“Make: Encyclopedia of Electronic Components Volume 1: Resistors, Capacitors, Inductors, Switches, Encoders, Relays, Transistors” (pdf)](https://www.amazon.es/Make-Encyclopedia-Electronic-Components-Transistors/dp/1449333893)

#### Components:

- [Pull up](https://learn.sparkfun.com/tutorials/pull-up-resistors)
- [Resistor SMD code](http://www.resistorguide.com/resistor-smd-code/)

#### How current flows in a circuit:

- [What is electric current](https://youtu.be/kYwNj9uauJ4)
- [Voltage, Current, Resistance, and Ohm's Law](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)
- [Conventional Versus Electron Flow](https://www.allaboutcircuits.com/textbook/direct-current/chpt-1/conventional-versus-electron-flow/)
- [Electric current](https://en.wikipedia.org/wiki/Electric_current)

#### Eagle Youtube Resources :
-[Getting Started with EAGLE Sept 2018](https://www.youtube.com/watch?v=SkxbnIypGwY)
-[Getting Started with CadSoft EAGLE](https://www.youtube.com/watch?v=R4DYztYB6d4)

#### Simulation Software :
-[Proteus 8](https://www.labcenter.com/)
-[Tutorial about basic proteus 8](https://www.youtube.com/watch?v=Oo_2J2traQc)

#### Fusion + Eagle Intergration :
-[Autodesk EAGLE and Fusion360 Workflow - July 2018
](https://www.youtube.com/watch?v=Daks_P1JSOU)
-[Fusion 360 Tutorial – Using Library IO & Eagle](https://www.youtube.com/watch?v=S8U09Pj1m6M)



# Eagle WorkFlow:

![](https://i.imgur.com/axxbuyW.png)

Opening Eagle. Create a New Project and Create a new schematic:

![](https://i.imgur.com/TNOpwUW.png)

Schematic ("Connection Plan/Drawing ")

![](https://i.imgur.com/bkmQBfA.png)

Open Library Manager

![](https://i.imgur.com/l5TU18S.png)

Make sure fab.lbr is added and "in use"

![](https://i.imgur.com/pTgw7fc.png)

Use the "ADD" tool to add components

![](https://i.imgur.com/VLdRaYV.png)

Library View

![](https://i.imgur.com/FOsmG2G.png)

Notice the Schematic Represenation and the footprint indicated

![](https://i.imgur.com/WtgmNG9.png)

Adding the VCC and GND Symbols

![](https://i.imgur.com/FSJOcnK.png)

Using the "Route Tool"

![](https://i.imgur.com/bgg8HM2.png)

After adding the AVR ISP we use the "NAME" tool to rename the "NET" to the desired names

![](https://i.imgur.com/kIfmqPE.png)

The Pop-Up for the naming tool

![](https://i.imgur.com/YEOVAZX.png)

The correct dialog box that opens up after the connection

![](https://i.imgur.com/OzRPQ6x.png)

Placing Labels on the NET's helps in design and debugging.

![](https://i.imgur.com/4M2slNe.png)

Completing the Schematic

![](https://i.imgur.com/pJSBeO1.png)

Using the SCR to BRD button to create the actual PCB

![](https://i.imgur.com/9nT37Yo.png)

First instance of the Board Environment

![](https://i.imgur.com/MKyW918.png)

Standard way that components are arranged

![](https://i.imgur.com/npEsypl.png)

Select the "MOVE" tool to move the components and/or group of components

![](https://i.imgur.com/7JetpEH.png)

> THE FIRST PLACEMENT OF THE COMPONENT SHOULD ALWAYS BE THE **IC** .( Simplifies a lot of things afterwards )

![](https://i.imgur.com/BYAMLh3.png)

Temporary Placement

![](https://i.imgur.com/Xz1YJFk.png)

Select the "RatsNest" tool to optimise the airwires . This tool optimises the lenght of the airwire making
shorter pcb traces

![](https://i.imgur.com/qzX9KGV.png)

Notice the slight change of airwires

![](https://i.imgur.com/WWwLCAR.png)

Optimised Placement of Components

![](https://i.imgur.com/u2jeQ1c.png)

Select the "Route Tool"

![](https://i.imgur.com/n0X9n8u.png)

Select the width to "16 mil  " ( 1 mil = A thousandth of an inch )

IDK why it came to be used either ?

![](https://i.imgur.com/QBWyiV8.png)

Shown here is the two traces are two traces that are two close to be used . In this case use the "RIPUP" tool

![](https://i.imgur.com/sUWUFNZ.png)

This converts the trace to airwire

![](https://i.imgur.com/SgUoOLs.png)

Sometimes you can also change the grid size of the board to allow for finer control

![](https://i.imgur.com/6cSh2KQ.png)

There is no option but to use a few "Jumpers on the board " . So i tried adding just adding pads to the board.

![](https://i.imgur.com/ZyNxnHV.png)

Eagle doesn't allow me to do that there needs to be a component in the schematic to represent

![](https://i.imgur.com/HIga6F8.png)

So i first added 3 random resistors in the schematic

![](https://i.imgur.com/cEBRcZ5.png)

The resistors appear in the board environment

![](https://i.imgur.com/wizhdM7.png)

We first place the components at the right place . but Eagle does not allow me to connect to that pad .

![](https://i.imgur.com/0NfOhJe.png)

I switch back to the schematic to reconnect this resistor to the right pin

![](https://i.imgur.com/hdIvhur.png)

Now we connect the resistors

![](https://i.imgur.com/xPWDOvm.png)

Completed Layout of the board

![](https://i.imgur.com/F4tumgT.png)

I go to layer settings to export the image for production

![](https://i.imgur.com/WCFx0lt.png)

List of available layers in Eagle .

![](https://i.imgur.com/SfqQDON.png)

For generation of "traces" select TOP and DIMENSION

![](https://i.imgur.com/dscqCIQ.png)

Shown here

![](https://i.imgur.com/BVBXaJM.png)

Dialog box shows here the options

IMPORTANT THINGS TO BE KEPT IN MIND

DPI : 1000 and MONOCHROME to be selected

![](https://i.imgur.com/qEJ37vX.png)

To export the cutout file select only the "DIMENSION " layer of the Board .

![](https://i.imgur.com/pNSvYe3.png)

Voila file ready to be milled

![](https://i.imgur.com/1y3e0Lm.png)

Outline file
