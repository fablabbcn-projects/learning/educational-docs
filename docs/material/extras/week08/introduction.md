# How To Make A Computer

**An intoduction to digital electronics**

<script async class="speakerdeck-embed" data-id="85a73ebd554c45a2808a6768fb1c2914" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

## From Ideas to Code

![](https://miro.medium.com/max/1200/1*8j2PmhExz4q87OoddaH7ag.png)

!!! info "More on programming languages and geek stuff"
    - [REPL.it](https://repl.it/languages)
    - [Instructions for the Apollo Guidance Computer](https://en.wikipedia.org/wiki/Apollo_Guidance_Computer#Instruction_set)

### Algorithms and flow charts

!!! info "Definition"
    [...] an algorithm is a finite sequence of well-defined, computer-implementable instructions, typically to solve a class of problems or to perform a computation. Algorithms are always **unambiguous** [...]

![](https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/examples/flowchart-templates/simple-flowchart.svg)

!!! Note "Class example"
    How can we control the temperature of room?

### I/O Programming

The microcontroller will interface with the outter world with its PINs. Pins can serve to receive information, i.e. _inputs_, or actuate, i.e. _outputs_:

![](https://areacom.altervista.org/wp-content/uploads/arduino_atmega328_pinout.jpg)

We can use this pins to process inputs and generate outputs:

![](assets/ipo.png)

For example:

![](assets/input_outputs_balancer.png)

### Coding control flow statements

We can use control flow statements to manage how our program reacts to different inputs and actuates on the outputs.

```
while (condition) {

    // Do stuff
}
```

```
if (condition) {

    // Do stuff
} else {

    // Do other stuff
} 
```

```
for (# iterations) {
    //Do stuff
}
```

### Arduino programming

#### Installation

You will need to install the [Arduino IDE](https://www.arduino.cc/en/Main/Software). After that, we need to define the board that we are going to use.

#### Using Libraries

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Duke_Humfrey%27s_Library_Interior_6%2C_Bodleian_Library%2C_Oxford%2C_UK_-_Diliff.jpg/1200px-Duke_Humfrey%27s_Library_Interior_6%2C_Bodleian_Library%2C_Oxford%2C_UK_-_Diliff.jpg)

**Libraries** are sets of tools that someone has built to make certain tasks easier, for instance, reading pins in a microcontroller, or controlling the LEDs. For using a LED strip, we will need to install one library.

#### Class Example

Let's make an example for a temperature controller of IAAC's main hall!

![](assets/temperature_controller.png)

!!! Note "Key things to remember"
    1. Comming in or going out? [pinMode()](https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/)
    2. Pin on/off with [digitalWrite()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/)
    3. Read button state with [digitalRead()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/)
    4. Functions have arguments and might return things or not. The type of what they return is defined before the function name (`void` if it doesn't return anything) and the arguments in parenthesis

```
int t_setpoint = 23;// temperature I want to be at
float t; // temperature from the sensor
bool is_there_people_in_the_room;

int PIN_HEATERS = 5;
int PIN_TEMP = 14;
int PIN_PEOPLE = 8;

void setup() {
  // INIT SENSORS

  // HEATERS RELAY
  pinMode(PIN_HEATERS, OUTPUT);

  // TEMPERATURE SENSOR (ANALOG SENSOR)
  pinMode(PIN_TEMP, INPUT);

  // PEOPLE SENSOR
  pinMode(PIN_PEOPLE, INPUT);
  
}

void loop() {
  // SENSE PEOPLE
  is_there_people_in_the_room = sense_people();
  if (is_there_people_in_the_room) {
    // IF THERE IS SOMEONE, THEN...
    
    t = measure_temperature();

    if (t>t_setpoint) {
      switch_heater(false);
    } else {
      if (t<t_setpoint-2) {
        switch_heater(true);
      }
    }
    delay(60000);
  }
}

void switch_heater(bool desired_heater_status){
  if (desired_heater_status == true){
    digitalWrite(PIN_HEATERS, HIGH);
  } else {
    // RELAY OFF FOR HEATERS
    digitalWrite(PIN_HEATERS, LOW);
  }
}

bool sense_people() {
  bool people_or_not;
  // INTERACT WITH MY PEOPLE MEASUREMENT SENSOR
  people_or_not = digitalRead(PIN_PEOPLE);
  return people_or_not;
}

float measure_temperature(){
  int sensor_reading;
  // HERE WE INTERACT WITH THE TEMPERATURE SENSOR
  sensor_reading = analogRead(PIN_TEMP);
  // HERE WE CORRECT THE ANALOG SCALE
  float cor_reading = sensor_reading/4096;
  return cor_reading;
}
```

If we hit `Verify` and compile it we get our `.hex` file:

```
cffa edfe 0700 0001 0300 0080 0200 0000
0f00 0000 e006 0000 8500 0000 0000 0000
1900 0000 4800 0000 5f5f 5041 4745 5a45
524f 0000 0000 0000 0000 0000 0000 0000
0000 0000 0100 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 1900 0000 7802 0000
5f5f 5445 5854 0000 0000 0000 0000 0000
0000 0000 0100 0000 0070 0a00 0000 0000
0000 0000 0000 0000 0070 0a00 0000 0000
0700 0000 0500 0000 0700 0000 0000 0000
...
```

## References

[![](https://i.imgur.com/wujwOaw.png)](http://linkedbyair.net/bin/Ted%20Nelson%20Computer%20Lib%20Dream%20Machines%20-%201st%20edition%201974.pdf)
_Click on the image to download the PDF_

### A brief history of information and computers

!!! warning ""
    Do not try to watch them all! Simply choose a topic you like and jump over the videos. Maybe you find something interesting and suddenly want to learn more about it.

* [Information Theory](https://www.youtube.com/watch?v=p0ASFxKS9sg&list=PLP6PHJ8SLR6D4ytpHhZBdylPNcazU5m7o)
* [More on information Theory and Coding (for those who enjoyed the videos above)](https://www.youtube.com/playlist?list=PLzH6n4zXuckpKAj1_88VS-8Z6yn9zX_P6)
* [How computers think](https://www.youtube.com/watch?v=dNRDvLACg5Q)
* [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
* [How computers work](https://www.youtube.com/watch?v=nN9wNvEnn-Q)
* [How computer memory works the insides](https://www.youtube.com/watch?v=XETZoRYdtkw&t=6s)
* [Colossus the first electronic computer](https://www.youtube.com/watch?v=knXWMjIA59c)
* [How they design the computers they landed astronauts to the moon](https://www.youtube.com/watch?v=xQ1O0XR_cA0)
* [UNIX or what an operative system does](https://www.youtube.com/watch?v=tc4ROCJYbm0)
* [Looking at how things are built and work to learn how to design](https://www.youtube.com/playlist?list=PLvOlSehNtuHsy89OdSxBajult8e5srVLA)
* [An introduction to Arduino towards future assignments](https://www.youtube.com/watch?v=_h1m6R9YW8c)
