## Introduction to mechanics

![](http://media.boingboing.net/wp-content/uploads/2015/11/tumblr_nylxu1CAvV1qzt7d8o1_500.gif)

### Concept of degrees of freedom (DOF)

In physics, the degree of freedom (DOF) of a mechanical system is the number of independent parameters that define its configuration. It is the number of parameters that determine the state of a physical system and is important to the analysis of systems of bodies in mechanical engineering, aeronautical engineering, robotics, and structural engineering. The degrees of freedom can be calculated depending on the type of joints and support that a mechanism has. As a summary:

- If DOF = 1 -> Deterministic mechanism
- If DOF = 0 -> Structure
- If DOF > 1 -> Non-deterministic mechanism

![](https://i.imgur.com/PRnTJrb.png)

Things can get more difficult to analyse:

![](https://i.imgur.com/342RcJY.png)

And structures can get complex:

![](https://constructalia.arcelormittal.com/files/styles/carousel_large/public/33_picture_6--0d4b7dc26fe755c4c4037792c73c04a5.jpg)

### Mechanisms

- Linear movement:

    - Linear bearings / guides
    
    ![](https://i.imgur.com/f9SziAf.png)
        
    ![](https://cdn.shopify.com/s/files/1/0615/2193/files/slide_rails.gif)

    - Frictive mechanisms (piston)
    
    ![](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/18a53f301ea44510f3e82bb9bada31b9/medium.gif)
    
    ![](https://www.daz3d.com/forums/uploads/thumbnails/FileUpload/a2/7facfe48a5183a8c14ef9175e4767d.gif)

    - Screw
    
    ![](http://12vactuators.com/wp-content/uploads/2017/10/Ball-Screw-1.gif)
     
- Rotational movement:

When things need to have a coupled rotation, we can use gears, belts or chains to transfer the movement to one another. In any of them, we define transfer ratio as the ratio between the rotational speeds. If we consider the number of teeth of a gear, or pinion as r, and its speed as w:


i = w2/w1 = r1/r2


#### Gears

When things are close to each other: **we use gears**: 

![](https://upload.wikimedia.org/wikipedia/commons/5/53/Engrenages_-_85.488_-.jpg)

- **Straight gear**: simple, but noisy
    
![](https://wonderfulengineering.com/wp-content/uploads/2014/06/gear-wallpaper-30.jpg)

- **Helix gear**: more complex, but silent:

![](https://i.imgur.com/6YbJ9xQ.jpg)
    
- **Straight Bevel gear**: when we have angles

![](https://i.imgur.com/2NxDXvH.png)
    
- **Helix Bevel gear**: all of the above

![](https://www.carid.com/images/motive-gear/products/t529f29-2.jpg)

- **Pinion-rack**: from linear to rotation
    
![](https://i.imgur.com/ExMCVnT.png)

- Planetary gears!

![](https://orig00.deviantart.net/6bc6/f/2014/360/d/e/gear_fa_by_mcsoftware-d8bcjck.gif)

#### Belts and chains

When distances are longer, we don't use gears due to it's size, we use either belts or chains. Chains can transmit more power, but they are more noisy and they don't absorbe as much sudden vibrations as belts:

- Chain

![](http://3.bp.blogspot.com/-87AWI9KxdpI/TyQdySrmytI/AAAAAAAAKbI/t6E-DM1I1gE/s1600/Bike+Chain+Rotation.gif)

- Belt

![](https://i.imgur.com/57nUORe.gif)

An continuosly variable transmission (CVT):

![](http://www.rdg.co.za/wp-content/uploads/2018/03/CVT.gif)

#### Cams

Cams can come in different flavours:

![](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/ab590cc97d02562cfd3717b1ff0d19fc/medium.gif)

![](https://i.imgur.com/IfB9qR6.gif)

### Keeping things in place

- Bearings
    - Linear

    ![](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/a5ccf675ee12dba495dabd5524764c3c/large.gif)

    - Ball

    ![](http://4.bp.blogspot.com/-QQyRhpm_itM/TuXrG_xxYAI/AAAAAAAAAMc/M2SqxsVcCGY/s1600/roller_bearings.jpg)

    - Flat/needle

    ![](http://1.bp.blogspot.com/-dhSOeVlo1eU/Tuhz4ZWVf4I/AAAAAAAAAQ8/rqEWJiw661w/s1600/Needle_Roller_Bearing.jpg)

    - Conical

    ![](https://i.imgur.com/A2o2K47.png)

- Rings

![](https://images.homedepot-static.com/productImages/65c9dab0-2d20-4055-9d80-7989fb65010f/svn/blacks-the-hillman-group-pins-rings-clips-881384-64_1000.jpg)

### Mechanisms examples
- Bars mechanisms: 
    - 4-bar mechanism: 

    ![](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/b5a7467884b62dffdf1fd4b03da673b0/large.gif)

        - Locking plyers

    ![](https://i.imgur.com/B1jtVUB.png)

        - Crane

    ![](https://i.imgur.com/f3CX4sw.png)

        - Excavator

    ![](https://www.jcbcea.com.au/wp-content/uploads/2015/08/A_JS_300_2-1-meg.jpg)

    - Ratchet

    ![](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/73b09aa57298b621f5a6fb73a6d6c29d/medium.gif)

    - Geneva wheel

    ![](http://3.bp.blogspot.com/-t1YoDoDiynk/WADD47HBVNI/AAAAAAAACWs/PkHq2p4IQGEguSA9znAMT4ybd0g30QCvACK4B/s1600/thumb.gif)

    - Universal joint

    ![](https://i.imgur.com/x3cr0aV.gif)
    
    - Core X/Y

    ![](http://thingiverse-production-new.s3.amazonaws.com/renders/97/af/7e/11/40/c_preview_featured.jpg)

## Some links:

[Mechanism Examples](https://www.cs.cmu.edu/~rapidproto/mechanisms/examples.html)
[CoreXY](https://corexy.com/)
[Reprap CoreXY](https://www.reprap.org/wiki/CoreXY)
[Machines that make](http://mtm.cba.mit.edu/)