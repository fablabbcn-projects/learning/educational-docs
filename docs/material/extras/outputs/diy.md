# DIY Actuators - Outputs devices

This is a collection/instructions of outputs/actuators that you can build at home with materials.
*Still you will need an arduino or similar board*

**These are the sensors you can build:**
- DIY DC MOTOR
- Make your own light Bulb


## DIY Mini Acoustic Levitation

<iframe width="560" height="315" src="https://www.youtube.com/embed/DD8XXZAPaMg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This project uses an ultrasonic distance sensor to create a pocket of wave interference where a small object can be levitated.

Acoustic levitation is made possible through the fact that sound behaves as a wave.  When two sound waves intersect each other, they can either constructively or destructively interfere with each other.  (This is how noise - cancelling headphones work.)

This project uses an ultrasonic distance sensor to create a levitation effect. This works by creating "pockets" where two opposing sound waves interfere with each other. When an object is placed in the pocket it will stay there, seemingly hovering in place.

#### Materials

- arduino
- ultrasonic sensors
- wires
- breadboard

![](assets/u.jpg)

#### Steps-fabrication

You will need to sacrifice a distance sensor for this step (don't worry, they're relatively cheap):

![](assets/u2.jpg)

- De-solder and remove both transmitters from the board
- Remove and save the mesh screen from one
- Solder wires to both transmitters
![](assets/u3.jpg)


#### Code
Create the above circuit and note the following:

- You may not necessarily need to include the two 100nF capacitors.  (only if your board is for some reason not able to handle the circuit and it keeps shutting itself down)
- The 9v battery is a stand-in for any DC power supply - mine worked fine with a 7.5v LiPo battery
-
![](assets/uc.jpg)

```
//original code from: https://makezine.com/projects/micro-ultrasonic-levitator/
byte TP = 0b10101010; // Every other port receives the inverted signal
void setup() {
  DDRC = 0b11111111; // Set all analog ports to be outputs

  // Initialize Timer1
  noInterrupts(); // Disable interrupts

  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 200; // Set compare register (16MHz / 200 = 80kHz square wave -> 40kHz full wave)

  TCCR1B |= (1 << WGM12); // CTC mode
  TCCR1B |= (1 << CS10); // Set prescaler to 1 ==> no prescaling
  TIMSK1 |= (1 << OCIE1A); // Enable compare timer interrupt

  interrupts(); // Enable interrupts
}
ISR(TIMER1_COMPA_vect) {
  PORTC = TP; // Send the value of TP to the outputs
  TP = ~TP; // Invert TP for the next run
}
void loop() {
  // Nothing left to do here :)
}
```
#### Mount and calibration

![](assets/c1.jpg)
Start by positioning the transmitters about 3/4" apart from each other
Get a small piece of Styrofoam about half the size of a pea (it doesn't need to be round)

![](assets/c2.jpg)
Place the Styrofoam on the mesh screen from step 1
Using tweezers or pliers, position it in between the two transmitters (it should start to wiggle when you get close)
Move the transmitters around (closer and farther apart) until the Styrofoam stays still
![](assets/c3.jpg)

#### Troubleshooting

It took me about fifteen minutes to get it working the first time, but after that it was pretty easy to get it going again.  Here are some things you can try if it doesn't work at first:
- Make sure you wired everything correctly
- Increase the voltage to the H-bridge (different battery)
- Get a smaller piece of Styrofoam
- Try a different position for the transmitters
- Try adding the capacitors (if you didn't already)
- If it still doesn't work, maybe something is broken: try a different set of transmitters or a new battery.

![](assets/u.gif)


## DIY Motor #1

<iframe width="560" height="315" src="https://www.youtube.com/embed/2XsQQrUxLKs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](assets/motor.jpg)

#### materials
- arduino
- HotGlue/glue
- battery
- copper wire
- metalic clips
- transistor
- magnets

#### code

![](assets/dc.png)

```
int motorPin = 3;

void setup()
{
 pinMode(motorPin, OUTPUT);
 Serial.begin(9600);
 while (! Serial);
 Serial.println("Speed 0 to 255");
}


void loop()
{
 if (Serial.available())
 {
   int speed = Serial.parseInt();
   if (speed >= 0 && speed <= 255)
   {
     analogWrite(motorPin, speed);
   }
 }
}
```

The transistor acts like a switch, controlling the power to the motor, Arduino pin 3 is used to turn the transistor on and off and is given the name 'motorPin' in the sketch.

When the sketch starts, it prompts you, to remind you that to control the speed of the motor you need to enter a value between 0 and 255 in the Serial Monitor.
learn_arduino_serial_monitor.jpg

In the 'loop' function, the command 'Serial.parseInt' is used to read the number entered as text in the Serial Monitor and convert it into an 'int'.

You could type any number here, so the 'if' statement on the next line only does an analog write with this number if the number is between 0 and 255.


#### Steps-fabrication
Start by taking the wire, and leaving some slack...like 3 inches and about 6-7 cm and coiling it.

Messiness counts! Randomly and speedily coil it. Overlapping is fine and actually, if you do it messier, the coil performs better.
![](assets/dc1.jpg)

To hold it securely together, hold it and wrap it around the coil a few times.
Now, at the end of the coil, use a knife (and being very careful not to kill yourself) to strip the insulation off of the bottoms of the ends.
Next, bend one end of the wire up and the other down so that the coil doesn't go flying.
![](assets/dc0.jpg)


Now you need to assemble the rest. The coil is the biggest part. Now, take the safety pins and rubber band them together.
![](assets/dc2.jpg)

Now, take the AA battery and put it in the C battery socket. Then hold the safety pins together like in the first picture.  Then, add the magnet like in the second picture. Now, put the coil in the loops. It may be hard with the bent ends but keep trying until you get it in.

![](assets/dc3.jpg)

Now give the coil a abrupt push and it should spin. If it doesn't well, don't worry. Push it the other way abruptly.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dG74qCg3ViE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If it still doesn't work, try leaning the coil to one side. I have found that the coil tends to lean to one side of the battery. It probably is influenced by the magnet. Try putting the magnet directly under the coil and give it another spin. If it still doesn't spin, it might be a deadish battery. If it doesn't work then, see if you made a lousy connection. By now, if it still doesn't work, see if you had striped all the insulation off the ends. If so, then take a marker and color in half of the ends or, just cut the wire and unwind a turn or two.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dbEH3t2DxCs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You can try driving it directly from the battery or try the arduino circuit directly driving it from an arduino pin and if not use a transistor to git it more power.


## DIY Homemade Buzzer#2

![](assets/piezo1.jpg)

The alternating voltage induced in the coil generates a varying magnetic field in accordance to the applied voltage, which in turn vibrates an iron plate to produce the required audible buzzing sound.

#### Materials

- Resistors 150k ohms and 4k7 ohm
- cable jumpers
- thin metal plate x2
- arduino
- rubber ring
- transistor

#### Code

![](assets/buzzer.png)

```
const int buzzer = 13; //buzzer to arduino pin 13


void setup(){

 pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output

}

void loop(){

 tone(buzzer, 1000); // Send 1KHz sound signal...
 delay(1000);        // ...for 1 sec
 noTone(buzzer);     // Stop sound...
 delay(1000);        // ...for 1sec

}
```

#### Circuit

![](assets/buzzer.png)

As can be seen in the circuit diagram, the circuit uses just one transistor, two resistors, a buzzer coil and a piezo- electric transducer. Here no external oscillator is required to drive the piezo, rather the circuit is a self oscillatory one. To maintain oscillations in an electronic circuit a feedback voltage is always required and here it is received through the centre tap of the piezo (thanks to these simple low cost 3-terminal transducers). The buzzer coil here performs the function of amplifying the low voltage oscillations into high voltage oscillations required by the piezo to create a strong audible tone. The resistor R1 is used just for providing the required biasing to the transistor, where as R2 has been included so that the oscillations do not stall due to overloading of the circuit.


## Easiest motor of the world #3

![](assets/simplemotor02.jpg)

This project make a simple unipolar motor with a battery,cable,magnet and an screw.
<iframe width="560" height="315" src="https://www.youtube.com/embed/e90P6Pk4V0s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](assets/simplemotor03.jpg)
![](assets/simplemotor05.jpg)


From: Arvindguptatoys

## DIY Arduino Controlled Magnetic Stirrer #4


How to make a proximity sensor using a infrared led and infrared phototransistor.

![](assets/mat.jpg)

#### materials

- Fan
- transistor
- Arduino
- Neodymium Magnets
- Battery power supply
- button(or two wires)
- glue or something that sticks

![](assets/wiring.jpg)
*Note: this wiring uses a 3 switch systems,but you can reduce to one on/off.

### code
```
void setup() {
 pinMode(7, INPUT_PULLUP);
 pinMode(3,OUTPUT);
}

void loop() {
 int val = digitalRead(7);
 if (val == LOW) {
     analogWrite(3, 255);
 delay(5000);
 }
 else
 {
   analogWrite(3,0);
 }

}
```

#### Assembly

Hot glue is perfect to hold all components in place and also adds a layer of electrical insulation to all connections.

![](assets/magnets.jpg)

Once you have this done, take your fan and hot glue your magnets to the fan blades about 25mm apart, keeping them as centrally balanced as you can. Place your fan into the centre space and screw down tightly.

![](assets/pill.jpg)

You can use any magnetic material you wish. Luckily I already had some 8mm threaded rod so I used that, but I was also considering cutting an old screwdriver or drill bit to use for this. Like I say, anything magnetic will do... (chopped Nails?)

#### Use

![](assets/tea.jpg)
1.) Now, go into your kitchen and plug in your new device.

2.) Heat up 1 cup of milk in a microwave or over the hob until hot.

3.) Put 3 teaspoons of drinking chocolate in a Mug.

4.) Add 2 teaspoons of sugar.

5.) Add a tablespoon of milk and mix together.

6.) Pour half the heated milk into your Mug and turn your mixer onto "High"

7.) Add some pieces of chocolate.

8.) Slowly add the rest of your milk, turning down the speeds as you go.

9.) Add marshmellows, and top with a sprinkling of chocolate sprinkles, chocolate powder and squirty cream!

## Converting Micro-Servos for Continuous Rotation

![](assets/servo.jpg)
Micro-servos are great devices perfect for adding motion to your projects. Unlike steppers, they can be controlled directly from your Arduino or other favorite microcontroller. Most servos are limited to 180 degrees of motion. This allows microcontrollers to know the exact position of the servo's shaft. You can find continuous rotation servos, but if you have access to lots of cheap micro-servos of the 180 degree variety, it might be helpful to learn this quick hack to make them continuous.

<iframe width="560" height="315" src="https://www.youtube.com/embed/zV_5wUo7Kxs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### materials

![](assets/mat2.jpg)
- A 9g micro-servo;
- Needle nose pliers;
- Screw drivers matching screws on your servo;
- A 1.4mm flat head screw driver;
- Box cutter or hobby knife;
- Superglue;
- Arduino or other microcontroller to test your servo (including USB cable to attach to your computer) and;
- Jumper wires to hook your servo up to your Arduino for testing.

### Modification

![](assets/open.jpg)

This is simple. Just unscrew the screws usually found on the bottom of the servo case. Next, very carefully open the case and try not to knock out any of the gears. Sometimes they are just barely hanging in there and the pressure of the case is all that is keeping them in place.

Next, take a picture of how everything looks before going any further. You will thank me later when it is time to put it all together. I know what you're thinking, "I can remember how it goes back together." Trust me, take a picture! I've included one of the 9g Tower Pro we have here at Maker Zoo. Yours might be identical, or maybe a little different.

![](assets/cut.jpg)

The topmost gear needs to be removed. Take a look underneath and notice what should be a tab. This tab is part of what limits the servo's motion to 180 degrees. You're going to need to shave it off with your box cutters or hobby knife.

Be very careful with your hands and fingers because the gear is small and difficult to hold on to while cutting. Take your time, and don't rush through this step. Take a little material off at a time to reduce the amount of force needed.

Next, look closely at the hole that this topmost gear fits onto the shaft with. It is a semicircle. To make sure it spins freely on the shaft 360 degrees, we need to auger out material to make it a full circle. Using an approximately 1.4mm jeweler's screwdriver, slowly twist it until the hole becomes circular.


![](assets/glue.jpg)
Remove the rest of the gears. Keep the servo sitting up and support it with something if necessary for this next step. Get your pliers (though you could use your fingers if you are very dexterous), and twist the shaft of the potentiometer. You will notice its 180 degrees of rotation to the left and right. Twist it back and forth a few times to get an idea of where center is, then leave it as close to center as you can.

Next, you need to carefully apply some superglue around the base (not on the shaft) of the potentiometer. Give it some time to dry. Gently blow on it and if you see the glue moving, it isn’t dry yet. If you were careless when gluing and assembling the servo while it is still wet, you might glue everything together! Disaster!

![](assets/assembly.jpg)

Once the superglue is fully dried, using the picture we took at the beginning after removing the case, we can reassemble the gears carefully. Place the top cover of the casing back on, making sure it is securely and properly fitted in place. Then screw the case closed with the four screws removed in Step 1.

### Code

![](assets/ard.jpg)

Now that you have everything put back together, it’s time to test the servo and make sure the conversion was successful. An Arduino is perfect for this and all you need to test it is your Arduino (we’re using an UNO board), 3 jumper wires, and a USB cable to connect the UNO to our computer. On our servos, the brown cable is ground, red is positive, and orange is for the signal. Connect ground to GND on your Arduino, postive to 5V on your Arduino, and the signal wire to pin 9 on your Arduino. (Note: A servo under any kind of load should not be hooked up to your Arduino like this, it could overdraw amps and cause your Arduino to reset or burn it out. But here we are just testing rotation, and just for a short time.)
```
//Test continuous servo conversion

#include Servo myservo;

void setup() {

myservo.attach(9);

}

void loop() {

myservo.write(180); //clockwise rotation

delay(2000); //rotation duration in ms

myservo.detach(); //detach servo to prevent “creeping” effect

delay(500); //short pause

myservo.attach(9); //reattach servo to pin 9

myservo.write(0); //counterclockwise rotation

delay(2000); //rotation duration in ms

myservo.detach(); //detach servo to prevent “creeping” effect

delay(500); //short pause myservo.attach(9); //reattach servo to pin 9 before looping

}
```
Another adafruit tutorial [link](https://learn.adafruit.com/modifying-servos-for-continuous-rotation/overview)

## Control your ROOMBA CLEANER WITH ARDUINO!


![](assets/roomba.jpg)

Link for the tutorial[link](https://www.instructables.com/id/Controlling-a-Roomba-Robot-With-Arduino-and-Androi/)

Try also with a camera and image recognition![link](https://www.instructables.com/id/Drive-a-Roomba-robot-from-Grasshopper-using-comput/)

## Electric generator

![](assets/Spoolgenerator14.jpg)

Tutorial [link](http://www.arvindguptatoys.com/toys/Spoolgenerator.html)
