What We Learned From Onions
===========================

!!! Note
    This is a copy of Guillem's notes: [https://hackmd.io/S_zReHFdTcGiBd2o7bnhQQ](https://hackmd.io/S_zReHFdTcGiBd2o7bnhQQ)
    
**or what happens when you type google.com into your browser and press enter?**

**an introduction to Networks, the Internet and the OSI Model**

<script async class="speakerdeck-embed" data-id="f44315c9f98446e583de317cdebc5395" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

## Some readings

[**How the web works?**](https://github.com/vasanthk/how-web-works/blob/master/README.md)

[**What happens when you type google.com into your browser and press enter?**](https://github.com/alex/what-happens-when)

[**How the Internet works?**](https://thesquareplanet.com/blog/how-the-internet-works/)

[**Submarin Cables and more...**](https://arstechnica.com/information-technology/2016/05/how-the-internet-works-submarine-cables-data-centres-last-mile/)

[**Full networks course**](http://intronetworks.cs.luc.edu/current/html/)

[**Distributed community networks**](https://www.lowtechmagazine.com/2015/10/how-to-build-a-low-tech-internet.html)

<iframe width="560" height="315" src="https://www.youtube.com/embed/L6bDA5FK6gs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>