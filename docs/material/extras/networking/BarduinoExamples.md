# Examples Code for communications between Barduino's


## Serial communication using SoftwareSerial library

!!! Danger "Install ESPSoftwareSerialLibarary"
	You have to install the ESPSoftwareSerialLibarary trougth ArduinoIDE

Simple connection between 2 ESP32 (for example the ESP32 of the Barduino), you just need to connect Pin 12 to Pin 13 from one board to the other, as Pin12 is the RX pin and pin13 is TX pin. You can use any digital pins. Check the code here (in this case is the same for both boards as is just translating from the Software Serial to the Serial):

```C++
#include <SoftwareSerial.h>
EspSoftwareSerial::UART myPort;

void setup() {
    Serial.begin(115200);
    myPort.begin(9600, SWSERIAL_8N1, 12, 13, false);
}

void loop() {
    // Get something on SoftSerial
    if (myPort.available()){
        // Read it and send it to Serial
        Serial.write(myPort.read());
    }
    // Get something on Serial
    if (Serial.available()) {
        // Read it and send it to SoftSerial
        myPort.write(Serial.read());
    }
}
```

You can try to implement te same with the ESP32 and a SAMD for example, trought Software Serial or just Serial like the Serial bridge code we use to upload code to esp32. For using SoftwareSerial with the SAMD an extra library may be installed.

## Bluetooth Serial Bridge between ESP32

!!! Danger "ESP32 core version"
	To make sure this example work, use the ESP32 boards from Arduino version 2.4.0, follow the instructions [here](https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/installing.html)

Here is how to make the same but trought Bluetooth! In this case you don't need any wire between them, it is wireless ;).

Here is the code.

Code for the Master Bluetooth: 

```C++
#include "BluetoothSerial.h"

BluetoothSerial SerialBT;
int i = 0;

String name = "ESP32test"; //                  <------- set this to be the name of the other ESP32!!!!!!!!!
char *pin = "1234"; //<- standard pin would be provided by default
bool connected;

void setup() {
  Serial.begin(115200);
  //SerialBT.setPin(pin);
  SerialBT.begin("ESP32testm", true); 
  //SerialBT.setPin(pin);
  Serial.println("The device started in master mode, make sure remote BT device is on!");
  
  // connect(address) is fast (upto 10 secs max), connect(name) is slow (upto 30 secs max) as it needs
  // to resolve name to address first, but it allows to connect to different devices with the same name.
  // Set CoreDebugLevel to Info to view devices bluetooth address and device names
  connected = SerialBT.connect(name);
  //connected = SerialBT.connect(address);
  
  if(connected) {
    Serial.println("Connected Succesfully!");
  } else {
    while(!SerialBT.connected(10000)) {
      Serial.println("Failed to connect. Make sure remote device is available and in range, then restart app."); 
    }
  }
  // disconnect() may take upto 10 secs max
  if (SerialBT.disconnect()) {
    Serial.println("Disconnected Succesfully!");
  }
  // this would reconnect to the name(will use address, if resolved) or address used with connect(name/address).
  SerialBT.connect();
}

void loop() {
  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }
  delay(20);
}

```

And here the one for the slave:

```C++
#include "BluetoothSerial.h"

BluetoothSerial SerialBT;

void setup() {
  Serial.begin(115200);
 SerialBT.begin("ESP32test"); //Bluetooth device name // <------- set this to be the same as the name you chose above!!!!!
 Serial.println("The device started, now you can pair it with bluetooth!");
}

void loop() {
  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }
  delay(20);
}
```

In this case you can only do it with the ESP, if you want to do it with a SAMD you will need an external BT module.


## I2C between the ESP32 and the SAMD11

!!! Danger "ESP32 core version"
	To make sure this example work, use the ESP32 boards from Arduino version 2.8.0, follow the instructions [here](https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/installing.html)

Communicating into the same board! We are going to program the SAMD as a master and the ESP32 as a slave for I2C communication. 

For the connections:

ESP32        SAMD11
21 ----------- 14   SDA
22 ----------- 15   SCL

Remember to put PullUp resistors (10K ohm) connecting both lines (SDA & SCL) to 3.3V.

Then, for this code, the SAMD11 will request one Byte to te ESP32, that will answer with a 1 or a 0 depending on the state of pin12, where I seted up a button. Then the SAMD11 lights up the LED in pin2 acordingly.

Here the codes:

Master (SAMD11)

```C++
#include <Wire.h>

void setup() {
    Wire.begin();        // join i2c bus (address optional for master)
    Serial.begin(9600);  // start serial for output
    pinMode(2, OUTPUT);
}

void loop() {
    Wire.requestFrom(8, 1);    // request 6 bytes from slave device #8

    while (Wire.available()) { // slave may send less than requested
        char c = Wire.read();    // receive a byte as character
        Serial.println(c);         // print the character
        if (c == '1'){
          digitalWrite(2, HIGH);
        }
        else{
          digitalWrite(2, LOW);
        }
    }
    delay(10);
}
```

Slave (ESP32)
```C++
//Code for a slave ESP32
#include <Wire.h>

void setup() {
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  pinMode(12, INPUT);
}

void loop() {
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  if (digitalRead(12)) {
    Wire.write("1"); // respond with message of 6 bytes
    // as expected by master
  }
  else {
    Wire.write("0");
  }
}
```



