Networking and Communications
=============================

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Internet_map_1024.jpg/1200px-Internet_map_1024.jpg)

We use networks and communication protocols to distribute and connect systems due to:

- **location**: not all operations in a system are possible on the same hardware
- **paralelism**: we want to do several things at the same time
- **modularity**: to develop modules, making more easily scalable and repairable
- **interference**: avoid interference between systems

![](https://live.staticflickr.com/65535/50812574093_2e9493d105_k.jpg)

!!! warning
	**Protocol vs Network**

	A **network** is a group of computers connected among themselves through communication lines.

	A **protocol** is the set of rules that specify message formatsand procedures that allow machines and programs to exchange information.


## Types

Networks can be of various shapes and types ([wiki](https://en.wikipedia.org/wiki/Computer_network)):

- **transmission media**: cable, optic, radio frequency

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/World_map_of_submarine_cables.png/1200px-World_map_of_submarine_cables.png" alt="World map of submarine cables.png"><a href="https://commons.wikimedia.org/w/index.php?curid=5542154">Wikipedia</a></p>

- **topology**

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/NetworkTopologies.svg/1200px-NetworkTopologies.svg.png" alt="NetworkTopologies.svg"><a href="https://commons.wikimedia.org/w/index.php?curid=15006915">Wikipedia</a></p>

- **range**:

<div style="text-align:center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Data_Networks_classification_by_spatial_scope.svg/1200px-Data_Networks_classification_by_spatial_scope.svg.png" width="400px">
</div>
<br>

!!! info
	**To keep in mind**

	Choose a network type depending on your needs, and checking the items below:

	- Power consumption
	- Range
	- Bandwidth
	- Existing networks? or adhoc?
	- Mobility
