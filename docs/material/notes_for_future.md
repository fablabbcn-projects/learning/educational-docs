## Oscar

Mention or include FPGAs:
- [FPGAs](https://github.com/Obijuan/digital-electronics-with-open-FPGAs-tutorial/wiki)
- [Alhambra FPGA](https://alhambrabits.com/alhambra/)
- [Biorobots FPGA](https://github.com/jcarolinares/fpga-biorobots)

Include more DSP in inputs
http://protolab.pbworks.com/w/page/19403657/TutorialSensors

Frameworks:
https://github.com/espressif/esp-idf

More micropython

Less content, more support
