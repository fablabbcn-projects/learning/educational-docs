# Digital Bodies

### Local Class

**LASER CUTTER (CAM)**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0BWKXFiqYcHg4tUMDiM3hMuyBG2y06cQ3ZkR2KtF7eSboRzuctbTIlKNvAoafyd2tVD9JgNqZ04Ut/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


#### Computer-control Laser/Vinyl Equipment Usage Videos

<iframe width="560" height="315" src="https://www.youtube.com/embed/5GTeIffOlzg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/8oCOzy_Zx2o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Materials

[Laser_Cutter_Materials](http://atxhackerspace.org/wiki/Laser_Cutter_Materials)

**NEVER CUT THESE MATERIALS**
![](../../material/extras/week03/assets/week03-82b9f3a8.png)

**Safe Materials**
![](../../material/extras/week03/assets/week03-1d852fd4.png)

 - [WHY YOU SHOULD TRY TO AVOID MDF](https://www.hse.gov.uk/woodworking/faq-mdf.htm)
 
**Biomaterials Cut**

[Cutting Pasta on lasercutter](https://shiraczerninski.wixsite.com/thepastaproject)

![](../../material/extras/week03/assets/pasta.jpg)
