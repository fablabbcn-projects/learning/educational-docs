# Textile as Scaffold

[Tetile as Scaffold site](https://class.textile-academy.org/classes/2024-25/week10/)

## CNC Milling

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQJE9tzVZWpD9L58zvJ1dIqpL5c5F1KCtN29D5M7Sp2Cf6OAUtRAQOkj6fJTigoO9-7eCz9pqtYn9NQ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Process

  1. Measure material
  2. Design something BIG
  3. CAM -> Gcode generation
  4. Gcode is checked by someone
  5. Design milled
  6. Design assembled
  7. Design finished

**Love Letter to Plywood**

<iframe src="https://player.vimeo.com/video/44947985?title=0&byline=0&portrait=0&badge=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

### 1961 introduction to CNC for Boeing engineers.
![](/learning/educational-docs/material/extras/week07/assets/intro0.png)
![](/learning/educational-docs/material/extras/week07/assets/intro1.png)
![](/learning/educational-docs/material/extras/week07/assets/intro2.png)
![](/learning/educational-docs/material/extras/week07/assets/intro3.png)

**RHINOCAM VIDEO TUTORIAL**

<iframe width="560" height="315" src="https://www.youtube.com/embed/bNNX4G2K0Ts" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you want to use the big CNC RAPTOR XLS milling machine of Pujades 102 you need to install the postprocessor for rhinocam. You can find it [here](https://github.com/EDUARDOCHAMORRO/CNC-StepPostprocessor)

## References and resources

### CNC concepts
- [Guerrilla Guide to CNC machining](http://lcamtuf.coredump.cx/gcnc/)
- [Opendesk](https://drive.google.com/open?id=0B3wpVpnNyxI0ZmdLUDRIYk9mNk02UUVTUDA0R2tLSGFBVXJz)

### Rhino

- [Rhinocam](https://mecsoft.com/rhinocam-mill/)
- [Rhinocam Getting Started Guide (PDF)](https://drive.google.com/open?id=0B3wpVpnNyxI0SGJ0RlBzNl9fQ3c)
- [Rhinocam Tutorial ( PDF ) ( Basics )](https://drive.google.com/file/d/0B3wpVpnNyxI0Z3VmOWFZUkt6a0k/view)
- [Rhinocam Tutorial ( PDF ) ( Software Guide and a list of tutorials)](https://drive.google.com/file/d/0B3wpVpnNyxI0UDZmX29YblQ4VXM/view)
