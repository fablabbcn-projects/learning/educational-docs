# From physics to information

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/p0ASFxKS9sg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Getting inspired by the past

[![](assets/computer.png)](http://linkedbyair.net/bin/Ted%20Nelson%20Computer%20Lib%20Dream%20Machines%20-%201st%20edition%201974.pdf)  
_Click on the image to download the PDF_  

!!! Warning "Some references"
    Do not try to watch them all! Simply choose a topic you like and jump over the videos. Maybe you find something interesting and suddenly want to learn more about it.

    * [Information Theory](https://www.youtube.com/watch?v=p0ASFxKS9sg&list=PLP6PHJ8SLR6D4ytpHhZBdylPNcazU5m7o)
    * [More on information Theory and Coding (for those who enjoyed the videos above)](https://www.youtube.com/playlist?list=PLzH6n4zXuckpKAj1_88VS-8Z6yn9zX_P6)
    * [How computers think](https://www.youtube.com/watch?v=dNRDvLACg5Q)
    * [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
    * [How computers work](https://www.youtube.com/watch?v=nN9wNvEnn-Q)
    * [How computer memory works the insides](https://www.youtube.com/watch?v=XETZoRYdtkw&t=6s)
    * [Colossus the first electronic computer](https://www.youtube.com/watch?v=knXWMjIA59c)
    * [How they design the computers they landed astronauts to the moon](https://www.youtube.com/watch?v=xQ1O0XR_cA0)
    * [UNIX or what an operative system does](https://www.youtube.com/watch?v=tc4ROCJYbm0)
    * [Looking at how things are built and work to learn how to design](https://www.youtube.com/playlist?list=PLvOlSehNtuHsy89OdSxBajult8e5srVLA)
    * [An introduction to Arduino towards future assignments](https://www.youtube.com/watch?v=_h1m6R9YW8c)
    * [The Secret Life of Machines](https://www.youtube.com/watch?v=KDpNQQqdSh8&list=PLByTa5duIolYRtq45Cz_GmtzfWJyA4bik)

## Representing information

Now, let's start representing information with our very basic circuit.

![](assets/multimeter.png)

!!! Note "A **couple of questions**"
    - How much information are we able to represent with this circuit?
    - How many possibilities do we have? And what if we combine all the circuits we just built?

### Digital Logic in an Analog World

As we saw before:

> A single switch can be on or off, enabling the storage of 1 bit of information. Switches can be grouped together to store larger numbers. This is the key reason why binary is used in digital systems.

<div style="text-align:center">
<img src="https://usercontent1.hubstatic.com/13809446.gif" width="400"></div><br>

With a bank of eight switches we can _store_ 2^8^ = 256 possible numbers. Each of those switches is called **1 bit**, and 8 of them are a **byte**:

<div style="text-align:center">
<img src="https://usercontent2.hubstatic.com/8735065_f520.jpg" width="600"></div><br>

This implies then, that to represent information with electricity, we have one very important limitation: information becomes discrete:

<div style="text-align:center">
<img src="https://i.imgur.com/9D2GKW6.png" width="600"></div><br>

Of course, depending on the amount of bits we use we have a more precise curve:

<div style="text-align:center">
<img src="http://pediaa.com/wp-content/uploads/2015/08/Difference-Between-Analog-and-Digital-Signals-A2D_2_bit_vs_3_bit.jpg" width="600"></div><br>

And if we put enough of them, we can represent more complex things like this:

<div style="text-align:center">
<img src="https://i.imgur.com/4MAPJzp.png" width="600"></div><br>

**Logic Levels**

Now that we have talked about representing information with single bit, we have to agree on how things are going to talk to one another. For this, we standardise what we consider as a **1** (high voltage) and **0** or low voltage. We call this **Logic levels**:

<div style="text-align:center">
<img src="https://i.imgur.com/zG4NPwb.png"></div><br>

### The core component: Transistor

Wouldn't it be ideal if we had a tiny mini switch we could control by ourselves? The transistor is nothing else than an electronic switch.

<div style="text-align:center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/BJT_symbol_NPN.svg/1200px-BJT_symbol_NPN.svg.png" alt="BJT symbol NPN.svg" width=200></div><br>

And it basically works like this:

<div style="text-align:center">
<img src="https://www.build-electronic-circuits.com/wp-content/uploads/2014/05/transistor-current-explanation.png" width=400></div><br>

We will see how this transitor helps us build more complex things, by controlling how they are connected together.

!!! Note "Reference Tutorials"
    - [Binary](https://learn.sparkfun.com/tutorials/binary)
    - [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
    - [Logic Levels](https://learn.sparkfun.com/tutorials/logic-levels/all)
    - [Guerrilla electronics](http://lcamtuf.coredump.cx/electronics/)

<div style="text-align:center">
<iframe width="600" height="400" src="https://www.youtube.com/embed/Fxv3JoS1uY8" frameborder="0" allow="autoplay; encrypted-media; picture-in-picture" allowfullscreen></iframe></div>

## The Arduino project

<div style="text-align:center">
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fyt3.ggpht.com%2F-XW1x_JT1g24%2FAAAAAAAAAAI%2FAAAAAAAAAAA%2FlMWjeVh_HEw%2Fs900-c-k-no%2Fphoto.jpg&f=1" width=200>
</div>

<div style="text-align:center">
<img src="https://i.imgur.com/sEQcWuF.png1">
</div>

![](https://hackster.imgix.net/uploads/attachments/907564/dsc01880_aCYwk124ES.JPG?auto=compress&w=900&h=675&fit=min&fm=jpg)
> The importance of the freedom to use, understand, modify and share your tools.

* Hardware - Programming IDE and libs - Community
![](assets/ardcom.png)

!!! Note "Resources"
    - [Arduino](https://arduino.cc)
    - [Fritzing](http://fritzing.org/home/)
    - [Arduino reference](https://www.arduino.cc/reference/en/)
    - [An introduction to Arduino](https://www.youtube.com/watch?v=_h1m6R9YW8c)

## Coding languajes
> A **high-level** programming language is a programming language with strong abstraction from the details of the computer.  
> A **low-level** programming language is a programming language that provides little or no abstraction from a computer's instruction set architecture.

_From_ [_wikipedia_](https://en.wikipedia.org/wiki/High-level_programming_language)

<div style="text-align:center">
<img src="https://miro.medium.com/max/1200/1*8j2PmhExz4q87OoddaH7ag.png" width=400>
</div><br>

## Algorithms and flow charts

> [...] an algorithm is a finite sequence of well-defined, computer-implementable instructions, typically to solve a class of problems or to perform a computation. Algorithms are always **unambiguous** [...]  


To represent algorithms we can use a for of diagram called **flow chart**.

> A flowchart is a type of diagram that represents a workflow or process. A flowchart can also be defined as a diagrammatic representation of an algorithm, a step-by-step approach to solving a task.  

> The flowchart shows the steps as boxes of various kinds, and their order by connecting the boxes with arrows. This diagrammatic representation illustrates a solution model to a given problem. Flowcharts are used in analyzing, designing, documenting or managing a process or program in various fields.

_From_ [_wikipedia_](https://en.wikipedia.org/wiki/Flowchart)

<div style="text-align:center">
<img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/examples/flowchart-templates/simple-flowchart.svg" width=400>
</div><br>

There are a lot of different **symbols** you can use on your flowcharts, these is the basic set and its function:

<div style="text-align:center">
<img src="../assets/fc_symbols.jpg">
</div><br>

!!! Note "References"
	- You can draw your flow charts online with [draw.io](https://draw.io)

### I/O Programming

Our code will interface with the outer world, receiving information, i.e. **_inputs_**, and actuating in some way, i.e. **_outputs_**. After receiving information from the environment, normally through some kind of sensor, we **_process_** the collected data to take decisions and actuate.

![](assets/ipo.png)

A balancing robot is a good example of this technic.

![](assets/input_outputs_balancer.png)


## Practical coding

### Variables

Think of variables as virtual drawers where you can store your stuff!

<div style="text-align:center">
<img src="../assets/drawers.jpg" width=300>
</div><br>

You can even label them with names, so you don't forget whats inside. But remember to tell the computer what the content will be, so she can build the drawer big enough to fit it.

```
  bool hello;
  byte my;
  int friend;
  float how;
  long are;
  char you;
  String today; // Only arduino!
```

### Coding control flow statements

We can use control flow statements to manage how our program reacts to different inputs and actuates on the outputs.

Conditionals help us take decitions, remember that your question can only be answered with _true_ or _false_.

```
  if (condition) {

      // Do stuff

  }
```

We can chain many conditions one after the other:

```
  if (condition) {

  	// Do stuff

  else if (condition2) {

  	// Do stuff 2

  } else {

  	// Do other stuff

  }

```

Loops can be used to repeat operations based on diferent parameters:

```
  while (condition) {

      // Do stuff

  }
  ```

  ```
  for (# iterations) {

  	// Do stuff

  }
```

### Comments

It is easy to forget what a piece of code was supposed to do if we read it after a couple of days, to write reminders or to explain what the code does, or simply to make a joke, we can use **_comments_**. In C language, simply starting a line with _//_ will make the compiler ignore all the content on it.

```
  // This line will be ignored by the computer!!

  if (fail) {

  	// Failure is the way to learn...
  	print("ERROR!!!");

  }

```

### Functions

When our code starts growing, it can get very messy and difficult to understand. Functions are a way to organize our code by grouping instructions that we use often and then calling them. We can assign any name we want to functions, the idea is making it descriptive so we remember what the function does.

![](https://docs.arduino.cc/static/f98f8f8864dbc20a93f66665e128aecf/6c1e7/FuncAnatomy.png)

```
  // We define a simple function to temporary light a led
  void blink() {
  	digitalWrite(LED_PIN, HIGH);
  	delay(500);
  	digitalWrite(LED_PIN, LOW);
  }

  // And we can use it whenever we want!
  void loop() {
  	if (ERROR) {
  			blink();
  	}
  }
```

### Libraries

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Duke_Humfrey%27s_Library_Interior_6%2C_Bodleian_Library%2C_Oxford%2C_UK_-_Diliff.jpg/1200px-Duke_Humfrey%27s_Library_Interior_6%2C_Bodleian_Library%2C_Oxford%2C_UK_-_Diliff.jpg)

**Libraries** are sets of tools that someone has built to make certain tasks easier, for instance, reading pins in a microcontroller, or controlling the LEDs. For using our LED strip, we will need to install one library.

!!! Note "References"
	* [Using Variables in Sketches](https://docs.arduino.cc/learn/programming/variables)
	* Arduino [article](https://docs.arduino.cc/learn/programming/functions) on functions.
	* [Get to know Arduino Libraries](https://docs.arduino.cc/learn/starting-guide/software-libraries)
	* You can find alot more info on the arduino [learn](https://docs.arduino.cc/learn) site.

## Hands on

!!! Note "Three things to remember"
    1. Comming in or going out? [pinMode()](https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/)
    2. Pin on/off with [digitalWrite()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/)
    3. Read button state with [digitalRead()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/)

### Reading a button

Digital input with [DigitalRead()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/)

![](https://i.imgur.com/vT2JVQu.png)

```
  int buttonPin = 2;
  int value = 0;

  void setup() {
    pinMode(buttonPin, INPUT);
    Serial.begin(115200);
  }

  void loop() {
    value = digitalRead(buttonPin);
    Serial.println(value);
  }
```

### Blinking led

Digital output with [digitalWrite()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/) using [delay](https://www.arduino.cc/reference/en/language/functions/time/delay/) function.

![](assets/circuit_04_led_blink.png)

```
  void setup() {
      // Set pin 12 as output
    pinMode(12, OUTPUT);
  }

  // Loop runs forever
  void loop() {
      // Voltage up in pin 12
      digitalWrite(12, HIGH);
      // Wait 200ms with pin 12 HIGH
      delay(200);
      // Voltage down in pin 12
      digitalWrite(12, LOW);
      // Wait 200ms with pin 12 LOW
      delay(200);    
  }
```

### I/O
**If you are feeling adventurous, try this**

![](assets/circuit_04_led_blink_smart.png)

~~~c++ 

      // Our variable for checking if it's pressed or not

      bool pressed = false;

      // the setup function runs once when you press reset or power the board
      void setup() {

        pinMode(12, OUTPUT);
        pinMode(27, INPUT);
        // Turn the LED off
        digitalWrite(12, LOW);
      }

      // Loop runs forever
      void loop() {
        // Read the pin
        if (digitalRead(27)) {
          // pressed!
          pressed = false;
        } else {
          // not pressed!
          pressed = true;
        }

        if (pressed) {
          // Turn it on
          digitalWrite(12, HIGH);
        } else {
          // Turn it off
          digitalWrite(12, LOW);
        }
      }
~~~     


!!! Note "General resources"
  - [Lisa's Soft Switches](http://thesoftcircuiteer.net/soft-switches/) Collection of soft-switches by Lisa Stark
  - [Lisa's Soft Sensors](http://thesoftcircuiteer.net/soft-switches/) Collection of soft-sensors by Lisa Stark
	- [hackaday.com](https://hackaday.com/) is one of the best blogs on DIY inventions and hardware hacking
	- [lowtechmagazine.com](https://www.lowtechmagazine.com/) many technology choices are political and economic, looking at past forgotten technologies helps us see the future
	- [archive.fabacademy.org](http://archive.fabacademy.org/) 10 years of project from Fab Labs around the world. Sometimes hard to browse but inspiring!
	- [learn.adafruit.com](https://learn.adafruit.com/) a really good site for electronics and programming tutorials, especially for beginners
	- [instructables](https://learn.adafruit.com/) more and more DIY tutorials, sometimes aren't good but there's a lot
  - [REPL.it](https://repl.it/languages)
  - [Instructions for the Apollo Guidance Computer](https://en.wikipedia.org/wiki/Apollo_Guidance_Computer#Instruction_set)
