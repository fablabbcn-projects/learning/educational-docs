# Computational Couture

## Local Class

### 3D Printing overview

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRZdm2USdKBOddVOxVnBDj3zDpFyHMN0GLhFJsvr7JT4kT5PqDLInrno9ci_hLSwSzr4_Aq2OmBjB7w/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Machines

- Creality Ender 3 Pro

!!! note
    This machine have to be manualy calibrated before printing.

    Here is a [video tutorial](https://www.youtube.com/watch?v=2htZ1Habb38) on ow to use it.

- Prusa MK2S
- Creality CR-10

!!! note
    The nozzle size is 1.2mm, use the computer in te 3D printing room to slice your files.

- Bambu labs A1
- Bambu labs X1-Carbon

!!! note
    Use [BambuStudio](https://bambulab.com/en/download/studio) to slice your models forthe Bambu printers. 

## References

- [Florencia final project](https://class.textile-academy.org/2022/florencia-moyano/finalproject/final/)
- [Dinesh final project](https://class.textile-academy.org/2023/dineshkumar-gunasekaran/project/)

### 3D Printing resources

- [A list of interesting links and videos](https://docs.google.com/document/d/15BHV1B9hmCRTgyB5Kp-fognhDLSsoO2vc9sJRsG70ng/edit?usp=sharing)


#### 3D Printable Things

- [Thingiverse](https://www.thingiverse.com)
- [YouMagine](https://www.youmagine.com/)
- [GrabCAD](https://grabcad.com/)
- [Cults](https://cults3d.com/en)
- [PinShape](https://pinshape.com/)

#### G-Code generators for 3D Printers

- [PrusaSlicer](https://www.prusa3d.com/page/prusaslicer_424/)
- [Cura](https://ultimaker.com/software/ultimaker-cura/)
- [BambuStudio](https://bambulab.com/en/download/studio)

### 3D modelling

- Visual Programming [Beetle Blocks](http://beetleblocks.com/)
