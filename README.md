# Fablab Barcelona Local Documentation

This repository contains the main source of documentation for Fablab Barcelona local Fabacademy program. This is an iterative documentation, so it's updated every year. 

View this documentation:
https://fablabbcn-projects.gitlab.io/learning/educational-docs/


Old documentation repository:
https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/
