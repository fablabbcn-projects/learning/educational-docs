# code
	## GIT
	## HTML
	## Terminal
	## Python?
	## Interfaces
		### Processing p5.js
		### Blender
		### MIT App Inventor
		### Blynk?

# digifab
	## Laser
	## CNC
	## CAM Softwares
		### RhinoCAM
		### BlenderCAM
		### Aspire
		### Easel
		### ModelPlayer4
	## Vinyl
	## 3D Printing
	## 3D Scanning
	## Precious Plastics?
	## Molding & Casting
	## Composites
	## Machine Design

# design
	## Presentation CAD
	## Rhino
	## Grasshopper
	## Fusion 360
	## Blender
	## OpenScad
	## Inkscape
	## Gimp?
	## CAM

# electronics
	## Electronics Intro
	## Embedded Programing
		### Arduino Programming
		### PlatformIO
	## Architectures 
	## Arduino Setup
	## Inputs
	## Outputs
	## Networking
		### wireless?
		
		### wired?
	## Electronics Production
	## Electronics Design
		### Kicad
		### Eagle
		### SVG PCB?
		### Easy EDA?

# biomaterials
	## 
